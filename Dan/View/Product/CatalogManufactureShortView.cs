﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using Dan.Models.Catalog;
using Dan.View.Image;

namespace Dan.View.Product
{
    public class CatalogManufactureShortView
    {
        [Display(Name = "#")]
        public long EntityId { get; set; }


        [Required, StringLength(256)]
        [Display(Name = "Наименование")]
        public string Name { get; set; }

        [AllowHtml]
        [Required, StringLength(8192)]
        [Display(Name = "Описание товара")]
        public string Description { get; set; }


        [Display(Name = "Изображение")]
        public CatalogImageView Image { get; set; }
    }

    public static partial class ViewHelper
    {
        public static List<CatalogManufactureShortView> AsShortView(this List<ManufactureEntity> entity)
        {
            return entity.Select(e => e.AsShortView()).ToList();
        }

        public static CatalogManufactureShortView AsShortView(this ManufactureEntity entity)
        {
            Mapper.Initialize
            (
                c => c.CreateMap<ManufactureEntity, CatalogManufactureShortView>()
                .ForMember(p => p.Image, p => p.MapFrom(m => m.ImagEntity.AsShortView()))
            );
            return Mapper.Map<CatalogManufactureShortView>(entity);
        }

    }
}