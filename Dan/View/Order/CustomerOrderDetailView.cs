﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using Dan.Models.Order;
using Dan.View.Cart;

namespace Dan.View.Order
{
    public class CustomerOrderDetailView : CustomerOrderPreview
    {
        public List<ProductCartView> Products { get; set; } = new List<ProductCartView>();
    }

    public static partial class ViewHelper
    {
        public static List<CustomerOrderDetailView> AsDetailView(this ICollection<OrderEntity> entity)
        {
            return entity.Select(p => p.AsDetailView()).ToList();
        }

        public static CustomerOrderDetailView AsDetailView(this OrderEntity entity)
        {
            Mapper.Initialize
            (
                c => c.CreateMap<OrderEntity, CustomerOrderDetailView>()
                    //.ForMember(p => p.Product, m => m.MapFrom(r => r.ProductEntity.AsTradeCartView()))
                    //.ForMember(p => p.Category, m => m.MapFrom(r => r.ProductEntity.CategoryEntity.AsShortView()))
                    .ForMember(p => p.Cost, m => m.MapFrom(r => r.ItemEntities.Sum(p => p.Price)))
                    .ForMember(p => p.Weight, m => m.MapFrom(r => r.ItemEntities.Sum(p => p.ProductEntity.Dimensions.Weight)))
            );
            return Mapper.Map<CustomerOrderDetailView>(entity);
        }
    }
}