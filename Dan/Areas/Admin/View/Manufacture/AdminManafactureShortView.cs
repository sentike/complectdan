﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using Dan.Models.Catalog;
using Dan.View.Image;
using Dan.View.Product;

namespace Dan.Areas.Admin.View.Manufacture
{
    public class AdminManafactureShortView : CatalogManufactureShortView
    {
        public long Products { get; set; }
    }

    public static partial class ViewHelper
    {
        public static List<AdminManafactureShortView> AsShortAdminView(this List<ManufactureEntity> entity)
        {
            return entity.Select(e => e.AsShortAdminView()).ToList();
        }

        public static AdminManafactureShortView AsShortAdminView(this ManufactureEntity entity)
        {
            Mapper.Initialize
            (
                c => c.CreateMap<ManufactureEntity, AdminManafactureShortView>()
                .ForMember(p => p.Image, p => p.MapFrom(m => m.ImagEntity.AsShortView()))
            );
            return Mapper.Map<AdminManafactureShortView>(entity);
        }

    }
}