﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Dan.Controllers;

namespace Dan.Infrastructures.Razor
{
    public static class CurrencyHelper
    {
        public static string ActionQuery(this UrlHelper html, string param, object value)
        {
            var query = HttpUtility.ParseQueryString(HttpContext.Current.Request.QueryString.ToString());
            query[param] = value.ToString();
            return $"?{query}";
        }

        public static MvcHtmlString AsRuble<T>(this HtmlHelper html, T price, string style = null)
           where T : IComparable<T>, IEquatable<T>, IComparable, IFormattable, IConvertible
        {
            //<span class="glyphicon glyphicon-ruble" style="font-size: 13px; font-weight: 100;"></span>
            TagBuilder text = new TagBuilder("span");

            TagBuilder icon = new TagBuilder("span");
            icon.AddCssClass("glyphicon");
            icon.AddCssClass("glyphicon-ruble");

            if (string.IsNullOrWhiteSpace(style) == false)
            {
                icon.Attributes.Add("style", style);
            }
            
            text.InnerHtml += price.ToString("F0", CartController.CurrencyFormat);
            text.InnerHtml += icon.ToString();

            return new MvcHtmlString(text.ToString());
        }
    }
}