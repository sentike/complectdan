﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using AutoMapper;
using Dan.Models.Image;

namespace Dan.Areas.Admin.View.Image
{
    public class AdminImageCategoryLargeView : AdminImageCategoryShortView
    {
        public List<AdminCatalogImageShortView> Images { get; set; } = new List<AdminCatalogImageShortView>();
    }

    public static partial class ViewHelper
    {
        public static List<AdminImageCategoryLargeView> AsLargeAdminView(this List<ImageCategoryEntity> entity)
        {
            return entity.Select(e => e.AsLargeAdminView()).ToList();
        }

        public static AdminImageCategoryLargeView AsLargeAdminView(this ImageCategoryEntity entity)
        {
            Mapper.Initialize
            (
                c => c.CreateMap<ImageCategoryEntity, AdminImageCategoryLargeView>()
                .ForMember(p => p.Images, p => p.MapFrom(m => m.ImageEntities.AsShortAdminView()))
                .ForMember(p => p.Count, p => p.MapFrom(m => m.ImageEntities.Count))
            );
            return Mapper.Map<AdminImageCategoryLargeView>(entity);
        }

    }
}