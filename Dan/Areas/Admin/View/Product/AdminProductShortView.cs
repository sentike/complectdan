﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using AutoMapper;
using Dan.Areas.Admin.View.Image;
using Dan.Models.Catalog;
using Dan.Models.Interface;
using Dan.View.Image;
using Dan.View.Product;

namespace Dan.Areas.Admin.View.Product
{
    public class AdminProductShortView 
        : CatalogProductLargeView
        , ICategoryPropertyInterface
    {
        public long CategoryId { get; set; }

        [Display(Name = "Категория")]
        public string CategoryName { get; set; }

        [Display(Name = "Комментарий")]
        public string AdminComment { get; set; }


        [Display(Name = "Опубликовано")]
        public bool Published { get; set; }

        [Display(Name = "Удалено")]
        public bool Deleted { get; set; }

        [Display(Name = "Новое")]
        public bool MarkAsNew { get; set; }

        [Display(Name = "Дата создания")]
        public DateTime CreatedOnUtc { get; set; }
    }

    public static partial class ViewHelper
    {
        public static List<AdminProductShortView> AsShortAdminView(this List<ProductEntity> entity)
        {
            return entity.Select(e => e.AsShortAdminView()).ToList();
        }

        public static AdminProductShortView AsShortAdminView(this ProductEntity entity)
        {
            Mapper.Initialize
            (
                c => c.CreateMap<ProductEntity, AdminProductShortView>()
                .ForMember(p => p.CategoryName, p => p.MapFrom(m => m.CategoryEntity.Name))
                .ForMember(p => p.Image, p => p.MapFrom(m => m.ImagEntity.AsShortView()))
            );
            return Mapper.Map<AdminProductShortView>(entity);
        }

    }
}