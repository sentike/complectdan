﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Dan.Areas.Admin.Infrastuctures;
using Dan.Areas.Admin.View;
using Dan.Areas.Admin.View.Manufacture;
using Dan.Infrastructures.Repository;
using Dan.Models.Catalog;
using Dan.View.Product;

namespace Dan.Areas.Admin.Controllers
{
    public class ProductManufacturerController : AbstractController
    {
        // GET: Admin/Manufacturer
        public ActionResult Index()
        {
            using (var r1 = new ImageRepository())
            {
                using (var r2 = new ManufactureRepository(r1.Context))
                {
                    var manufactures = r2.TakeEntities();
                    var view = manufactures.ToList().AsShortAdminView();
                    return View(view);
                }
            }
        }

        // GET: Admin/Manufacturer/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Admin/Manufacturer/Create
        public ActionResult Create()
        {
            using (var r1 = new ImageRepository())
            {
                using (var r3 = new ImageCategoryRepository(r1.Context))
                {
                    var view = new AdminManafactureCreateView
                    {
                        ImageSelectList = r1.TakeEntityList()
                    };
                    return View(view);
                }
            }
        }

        // POST: Admin/Manufacturer/Create
        [HttpPost]
        public ActionResult Create(AdminManafactureCreateView view)
        {
            using (var r1 = new ImageRepository())
            {
                using (var r2 = new ManufactureRepository(r1.Context))
                {
                    using (var r3 = new ImageCategoryRepository(r1.Context))
                    {
                        if (ModelState.IsValid == false)
                        {
                            view.ImageSelectList = r1.TakeEntityList();
                            return View(view);
                        }

                        if (view.ImageId < 0 || (view.ImageId > 0 && r1.IsExistEntity(view.ImageId) == false))
                        {
                            ModelState.AddModelError(nameof(view.ImageId), "Изображение не найдено");
                            view.ImageSelectList = r1.TakeEntityList()
                                .WithDefaultElem("Выберите изображение или загрузите новое");
                            view.ImageCategorySelectList = r3.TakeEntityList(view.Image.CategoryId);
                            return View(view);
                        }

                        try
                        {
                            if (view.ImageId == 0)
                            {
                                var status = UploadImage(r3.Context, view.Image);
                                if (status == UploadImageResponse.Successfully)
                                {
                                    view.ImageId = view.Image.EntityId;
                                }
                                else
                                {
                                    view.ImageSelectList = r1.TakeEntityList().WithDefaultElem("Выберите изображение или загрузите новое");
                                    view.ImageCategorySelectList = r3.TakeEntityList(view.Image.CategoryId);
                                    return View(view);
                                }
                            }

                            var entity = r2.Context.ManufactureEntities.Add(view.AsEntity());
                            r2.Context.SaveChanges();
                            return RedirectToAction("Index");

                        }
                        catch (Exception e)
                        {
                            ModelState.AddModelError(nameof(view.Name), e);
                            view.ImageSelectList = r1.TakeEntityList();
                            return View(view);
                        }
                    }
                }
            }
        }

        // GET: Admin/Manufacturer/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Admin/Manufacturer/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Admin/Manufacturer/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Admin/Manufacturer/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public override DeleteProcessResponse OnDeleteProcess(AdminUniversalDeleteView view)
        {
            throw new NotImplementedException();
        }
    }
}
