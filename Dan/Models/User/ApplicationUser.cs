﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Dan.Models.Common;
using Dan.Models.Order;
using Dan.Models.Tradecart;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Dan.Models.User
{
    public enum GenderTypeId : byte
    {
        None,
        Male,
        Famele,
    }

    // Чтобы добавить данные профиля для пользователя, можно добавить дополнительные свойства в класс ApplicationUser. Дополнительные сведения см. по адресу: http://go.microsoft.com/fwlink/?LinkID=317594.
    public class ApplicationUser : IdentityUser<Guid, ApplicationUserLogin, ApplicationUserRole, ApplicationUserClaim>
    {
        public string SecretAnswer { get; set; }
        public string EmailReserveValue { get; set; }
        public string EmailSafeValue { get; set; }
        public DateTime EmailChangeDate { get; set; }
        public DateTime PhoneChangeDate { get; set; }

        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }

        public GenderTypeId Gender { get; set; }
        public DateTime RegistrationDate { get; set; }
        public DateTime LastActivityDate { get; set; }
        public DateTime? BirthDate { get; set; }

        public virtual List<TradecartEntity> TradecartEntities { get; set; } = new List<TradecartEntity>();
        public virtual List<WishlistEntity> WishlistEntities { get; set; } = new List<WishlistEntity>();
        public virtual List<OrderEntity> OrderEntities { get; set; } = new List<OrderEntity>();

        public virtual CustomerAddressEntity Address { get; set; }

        public override string ToString()
        {
            return $"{Id} | {UserName}";
        }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser, Guid> manager)
        {
            // Обратите внимание, что authenticationType должен совпадать с типом, определенным в CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Здесь добавьте утверждения пользователя
            return userIdentity;
        }
    }
}