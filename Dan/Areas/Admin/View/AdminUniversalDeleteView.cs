﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Dan.Areas.Admin.View
{
    public class AdminUniversalDeleteView
    {
        [Display(Name = "Название")]
        public string Name { get; set; }

        [Display(Name = "Описание")]
        public string Description { get; set; }

        [Display(Name = "#")]
        public long EntityId { get; set; }

        [Display(Name = "Код")]
        public Guid PublicKey { get; set; }

        [Display(Name = "Код подтверждения")]
        public Guid PrivateKey { get; set; }
    }
}