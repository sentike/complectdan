﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;
using Dan.Models.Image;
using Dan.Models.Interface;
using Dan.Models.Types;

namespace Dan.Models.Catalog
{
    public class ProductEntity 
        : IAbstractEntityInterface
        , IEntityWithImageInterface
    {
        public long EntityId { get; set; }

        //------------------------------------------------------------

        public long ManufactureId { get; set; }
        public virtual ManufactureEntity ManufactureEntity { get; set; }

        #region Category
        public long CategoryId { get; set; }
        public virtual CategoryEntity CategoryEntity { get; set; }
        #endregion

        public string Sku { get; set; }


        public long ImageId { get; set; }
        public virtual ImageEntity ImagEntity { get; set; }

        //------------------------------------------------------------

        #region Description
        /// <summary>
        /// Gets or sets the name
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Gets or sets the short description
        /// </summary>
        public string ShortDescription { get; set; }
        /// <summary>
        /// Gets or sets the full description
        /// </summary>
        public string FullDescription { get; set; }
        #endregion
        //------------------------------------------------------------

        /// <summary>
        /// Gets or sets the admin comment
        /// </summary>
        public string AdminComment { get; set; }

        //------------------------------------------------------------

        public SeoEntityType Seo { get; set; } = new SeoEntityType();
        public PriceType Price { get; set; } = new PriceType();
        public DimensionsType Dimensions { get; set; } = new DimensionsType();
        public ShippingType Shipping { get; set; } = new ShippingType();
        public MarketingType Marketing { get; set; } = new MarketingType();



        public bool Published { get; set; }
        public bool Deleted { get; set; }


        public DateTime CreatedOnUtc { get; set; }
        public DateTime UpdatedOnUtc { get; set; }




        public virtual List<ProductParamEntity> ParamEntities { get; set; } = new List<ProductParamEntity>();
        public virtual List<ProductImageEntity> ImageEntities { get; set; } = new List<ProductImageEntity>(1);
        public virtual List<ProductVisitEntity> VisitEntities { get; set; } = new List<ProductVisitEntity>(1);
        public virtual List<ProductRecomendEntity> RecomendEntities { get; set; } = new List<ProductRecomendEntity>();

        public class Configuration : EntityTypeConfiguration<ProductEntity>
        {
            public Configuration()
            {
                HasKey(i => i.EntityId);
                ToTable("catalog.ProductEntity");

                //=================================================================
                HasMany(p => p.ParamEntities).WithRequired(p => p.ProductEntity).HasForeignKey(p => p.ProductId).WillCascadeOnDelete(true);
                HasMany(p => p.ImageEntities).WithRequired(p => p.ProductEntity).HasForeignKey(p => p.ProductId).WillCascadeOnDelete(true);
                HasMany(p => p.VisitEntities).WithRequired(p => p.ProductEntity).HasForeignKey(p => p.ProductId).WillCascadeOnDelete(true);
                HasMany(p => p.RecomendEntities).WithRequired(p => p.ProductEntity).HasForeignKey(p => p.ProductId).WillCascadeOnDelete(true);

                //=================================================================
                Property(p => p.Name).IsRequired().HasMaxLength(128);
                Property(p => p.AdminComment).IsOptional().HasMaxLength(196);
                Property(p => p.ShortDescription).IsRequired().HasMaxLength(4096);
                Property(p => p.FullDescription).IsRequired().HasMaxLength(4096 * 4);

                //=================================================================
                Property(p => p.Sku).IsOptional().HasMaxLength(128);
            }
        }

    }
}