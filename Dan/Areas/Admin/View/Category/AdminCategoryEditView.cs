﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Dan.Areas.Admin.View.Image;
using Dan.View.Image;

namespace Dan.Areas.Admin.View.Category
{
    public class AdminCategoryEditView : AdminCategoryCreateView
    {
        [Display(Name = "Изображение")]
        public new CatalogImageView Image { get; set; }

        [Display(Name = "Категория видна в каталоге")]
        public bool Published { get; set; }

        [Display(Name = "Категория удалена из каталога")]
        public bool Deleted { get; set; }

    }
}