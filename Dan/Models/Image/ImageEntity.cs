﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;
using Dan.Models.Blog;
using Dan.Models.Catalog;
using Dan.Models.Interface;

namespace Dan.Models.Image
{
    public enum FileImageFormat
    {
        None,
        Png,
        Jpg,
        Bmp,
    }

    public class ImageEntity 
        : IAbstractEntityInterface
    {
        public long EntityId { get; set; }

        public long CategoryId { get; set; }
        public virtual ImageCategoryEntity CategoryEntity { get; set; }

        //------------------------------------------------------------
        public string FileName => $"{EntityId}_original.{ImageFormat}";
        public string SmallFileName => $"{EntityId}_small.{ImageFormat}";
        public string LargeFileName => $"{EntityId}_large.{ImageFormat}";
        public const string RootDirectory = "/Content/Public";

        public FileImageFormat ImageFormat { get; set; }

        //------------------------------------------------------------

        #region Description
        /// <summary>
        /// Gets or sets the name
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Gets or sets the short description
        /// </summary>
        public string ShortDescription { get; set; }
        /// <summary>
        /// Gets or sets the full description
        /// </summary>
        public string FullDescription { get; set; }
        #endregion


        /// <summary>
        /// Gets or sets the "alt" attribute for "img" HTML element. If empty, then a default rule will be used (e.g. product name)
        /// </summary>
        public string AltAttribute
        {
            get { return ShortDescription; }
            set { ShortDescription = value; }
        }

        /// <summary>
        /// Gets or sets the "title" attribute for "img" HTML element. If empty, then a default rule will be used (e.g. product name)
        /// </summary>
        public string TitleAttribute
        {
            get { return FullDescription; }
            set { FullDescription = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the picture is new
        /// </summary>
        public bool IsNew { get; set; }

        public virtual List<ProductEntity> ProductEntities { get; set; } = new List<ProductEntity>(32);
        public virtual List<CategoryEntity> CategoryEntities { get; set; } = new List<CategoryEntity>(32);
        public virtual List<ManufactureEntity> ManufactureEntities { get; set; } = new List<ManufactureEntity>(1);
        public virtual List<BlogArticleEntity> ArticleEntities { get; set; } = new List<BlogArticleEntity>(1);

        public class Configuration : EntityTypeConfiguration<ImageEntity>
        {
            public Configuration()
            {
                HasKey(i => i.EntityId);
                ToTable("image.ImageEntity");

                Ignore(p => p.AltAttribute);
                Ignore(p => p.TitleAttribute);
                //=================================================================
                HasMany(p => p.ProductEntities).WithRequired(p => p.ImagEntity).HasForeignKey(p => p.ImageId);
                HasMany(p => p.CategoryEntities).WithRequired(p => p.ImagEntity).HasForeignKey(p => p.ImageId);
                HasMany(p => p.ManufactureEntities).WithRequired(p => p.ImagEntity).HasForeignKey(p => p.ImageId);
                HasMany(p => p.ArticleEntities).WithRequired(p => p.ImagEntity).HasForeignKey(p => p.ImageId);


                //=================================================================
                Property(p => p.Name).IsOptional().HasMaxLength(256);
                Property(p => p.ShortDescription).IsOptional().HasMaxLength(256);
                Property(p => p.FullDescription).IsOptional().HasMaxLength(256);
            }
        }

    }
}