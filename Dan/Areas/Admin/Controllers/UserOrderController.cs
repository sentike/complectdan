﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Dan.Areas.Admin.Infrastuctures;
using Dan.Areas.Admin.View;
using Dan.Areas.Admin.View.Order;
using Dan.Infrastructures.Repository;
using Dan.Models;
using Dan.Models.Order;

namespace Dan.Areas.Admin.Controllers
{
    public class UserOrderController : AbstractController
    {
        // GET: Admin/Order
        public ActionResult Index()
        {
            using (var db = new ApplicationDbContext())
            {
                var entity = db.OrderEntities.ToList();
                var view = entity.AsView();
                return View(view);
            }
        }

        // GET: Admin/Order/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }

            var entity = OrderRepository.TakeEntity(id.Value);
            if (entity == null)
            {
                return RedirectToAction("Index");
            }

            var view = entity.AsDetailView();
            return View(view);
        }

        #region ConfirmCreate

        // GET: Admin/Order/Edit/5
        public ActionResult ConfirmCreate(Guid? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }

            var entity = OrderRepository.TakeEntity(id.Value);
            if (entity == null)
            {
                return RedirectToAction("Index");
            }

            var view = entity.AsEditView();
            if (entity.OrderStatus != CustomerOrderStatus.Processing)
            {
                ModelState.AddModelError(nameof(view.OrderStatus), $"Заказ уже подтвержден! Статус заказа: {entity.OrderStatus.Description()}");
            }
            return View(view);
        }

        // POST: Admin/Order/Edit/5
        [HttpPost]
        public ActionResult ConfirmCreate(Guid id, AdminOrderConfirmView view)
        {
            var entity = OrderRepository.TakeEntity(id);
            if (entity == null)
            {
                ModelState.AddModelError(nameof(view.EntityId), "Заказ не найден!");
                return View(view);
            }

            if (entity.OrderStatus != CustomerOrderStatus.Processing)
            {
                ModelState.AddModelError(nameof(view.OrderStatus), $"Заказ уже подтвержден! Статус заказа: {entity.OrderStatus.Description()}");
                return View(view);
            }

            entity.Comment += view.Comment;
            entity.ConfirmOrderDate = DateTime.Now;
            entity.ConfirmPhoneDate = DateTime.Now;
            entity.OrderStatus = CustomerOrderStatus.Payment;
            OrderRepository.SendConfirmCreateMessage(entity);
            OrderRepository.SaveChanges();
            return RedirectToAction("Details", new {id, status = entity.OrderStatus });
        }

        #endregion



        #region ConfirmPayment

        public ActionResult ConfirmPayment(Guid? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }

            var entity = OrderRepository.TakeEntity(id.Value);
            if (entity == null)
            {
                return RedirectToAction("Index");
            }

            var view = entity.AsEditView();
            return View(view);
        }

        [HttpPost]
        public ActionResult ConfirmPayment(Guid id, AdminOrderConfirmView view)
        {
            var entity = OrderRepository.TakeEntity(id);
            if (entity == null)
            {
                ModelState.AddModelError(nameof(view.EntityId), "Заказ не найден!");
                return View(view);
            }

            if (entity.OrderStatus != CustomerOrderStatus.Payment)
            {
                ModelState.AddModelError(nameof(view.OrderStatus), $"Заказ уже оплачен! Статус заказа: {entity.OrderStatus.Description()}");
                return View(view);
            }

            entity.OrderStatus = CustomerOrderStatus.Preparing;
            OrderRepository.SendConfirmPaymentMessage(entity);
            OrderRepository.SaveChanges();
            return RedirectToAction("Details", new { id, status = entity.OrderStatus });
        }

        #endregion

        #region ConfirmCreate

        public ActionResult ConfirmDelivery(Guid? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }

            var entity = OrderRepository.TakeEntity(id.Value);
            if (entity == null)
            {
                return RedirectToAction("Index");
            }

            var view = entity.AsEditView();
            return View(view);
        }

        [HttpPost]
        public ActionResult ConfirmDelivery(Guid id, AdminOrderConfirmView view)
        {
            var entity = OrderRepository.TakeEntity(id);
            if (entity == null)
            {
                ModelState.AddModelError(nameof(view.EntityId), "Заказ не найден!");
                return View(view);
            }

            if (entity.OrderStatus != CustomerOrderStatus.Preparing)
            {
                ModelState.AddModelError(nameof(view.OrderStatus), $"Заказ уже отправлен! Статус заказа: {entity.OrderStatus.Description()}");
                return View(view);
            }

            entity.DeliveryToken = view.DeliveryToken;
            entity.DeliverySendDate = DateTime.Now;
            entity.OrderStatus = CustomerOrderStatus.Delivery;
            OrderRepository.SendConfirmDeliveryMessage(entity);
            OrderRepository.SaveChanges();
            return RedirectToAction("Details", new { id, status = entity.OrderStatus });
        }


        #endregion

        #region CancelOrder

        public ActionResult CancelOrder(Guid? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }

            var entity = OrderRepository.TakeEntity(id.Value);
            if (entity == null)
            {
                return RedirectToAction("Index");
            }

            var view = entity.AsEditView();
            return View(view);
        }

        [HttpPost]
        public ActionResult CancelOrder(Guid id, AdminOrderConfirmView view)
        {
            var entity = OrderRepository.TakeEntity(id);
            if (entity == null)
            {
                ModelState.AddModelError(nameof(view.EntityId), "Заказ не найден!");
                return RedirectToAction("Index");
            }

            if (entity.OrderStatus == CustomerOrderStatus.Delivery)
            {
                ModelState.AddModelError(nameof(view.OrderStatus), $"Заказ уже отправлен!");
                return View(view);
            }
            else if (entity.OrderStatus == CustomerOrderStatus.Canceled)
            {
                ModelState.AddModelError(nameof(view.OrderStatus), $"Заказ уже отменен!");
                return View(view);
            }

            entity.AdminComment = view.AdminComment;
            entity.DeleteDate = DateTime.Now;
            entity.OrderStatus = CustomerOrderStatus.Canceled;
            OrderRepository.SendCancelOrderMessage(entity);
            OrderRepository.SaveChanges();
            return RedirectToAction("Details", new { id, status = entity.OrderStatus });
        }


        #endregion

        public override DeleteProcessResponse OnDeleteProcess(AdminUniversalDeleteView view)
        {
            throw new NotImplementedException();
        }
    }
}
