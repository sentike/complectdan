﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using Dan.Models.Catalog;
using Dan.View.Filter;
using Dan.View.Product;
using Dan.View.Shared.Navigation;
using Dan.Models.Image;

namespace Dan.View.Category
{
    public class CatalogSliderFilter
    {
        public decimal? Min { get; private set; }
        public decimal? Max { get; private set; }

        public CatalogSliderFilter() { }
        public CatalogSliderFilter(decimal? min, decimal? max)
        {
            Min = min;
            Max = max;
        }

    }

    public class CatalogCategoryLargeView
    {
        public bool IsCurrent { get; set; }
        public long EntityId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public string Image { get; set; }
        public CatalogDisplayMethod DisplayMethod { get; set; }


        public CatalogSliderFilter WeightFilter { get; set; } = new CatalogSliderFilter();
        public CatalogSliderFilter WeightSeed { get; set; } = new CatalogSliderFilter();

        public CatalogSliderFilter CostFilter { get; set; } = new CatalogSliderFilter();
        public CatalogSliderFilter CostSeed { get; set; } = new CatalogSliderFilter();
        public bool IsSearchView { get; set; }

        public IEnumerable<FilterParamView> Filters { get; set; } = new FilterParamView[0];
        public List<CatalogCategoryLargeView> CategoryViews { get; set; } = new List<CatalogCategoryLargeView>();
        public PageNavigationView<CatalogProductShortView> ProductViews { get; set; }
    }

    public static class CatalogCategoryLargeViewHelper
    {
        public static List<CatalogCategoryLargeView> AsLargeView(this List<CategoryEntity> entity, CategoryEntity from = null, bool remap = false, PageNavigationView<CatalogProductShortView> navigation = null )
        {
            return entity.Select(e => e.AsLargeView(from, remap, navigation)).ToList();
        }

        public static CatalogCategoryLargeView AsLargeView(this CategoryEntity entity, CategoryEntity from = null, bool remap = false, PageNavigationView<CatalogProductShortView> navigation = null)
        {
            Mapper.Initialize
            (
                c => c.CreateMap<CategoryEntity, CatalogCategoryLargeView>()
                .ForMember(p => p.CategoryViews, m => m.MapFrom(r => r.ChildCategoryEntities.AsLargeView(from, false, null)))
                .ForMember(p => p.IsCurrent, m => m.MapFrom(r => r == from))
                .ForMember(p => p.ProductViews, m => m.UseValue(navigation))
                .ForMember(p => p.Image, m => m.MapFrom(r => $"{ImageEntity.RootDirectory}/{r.ImagEntity.FileName}"))
                .ForMember(p => p.Description, m => m.MapFrom(r => r.ShortDescription))
            );

            if (remap == false || entity.ChildCategoryEntities.Any() || entity.ParentCategoryEntity == null)
            {
                return Mapper.Map<CatalogCategoryLargeView>(entity);
            }

            return Mapper.Map<CatalogCategoryLargeView>(entity.ParentCategoryEntity);
        }

    }
}