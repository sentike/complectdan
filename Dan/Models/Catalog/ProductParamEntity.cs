﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace Dan.Models.Catalog
{
    public class ProductParamEntity
    {
        public long EntityId { get; set; }

        public long ProductId { get; set; }
        public virtual ProductEntity ProductEntity { get; set; }

        public long ParamId { get; set; }
        public virtual TemplateParamEnity ParamEnity { get; set; }


        public string Value { get; set; }


        public class Configuration : EntityTypeConfiguration<ProductParamEntity>
        {
            public Configuration()
            {
                HasKey(i => i.EntityId);
                ToTable("catalog.ProductParamEntity");

                //=================================================================
                HasRequired(p => p.ParamEnity).WithMany().HasForeignKey(p => p.ParamId).WillCascadeOnDelete(true);
                Property(p => p.Value).IsRequired().HasMaxLength(128);
            }
        }

    }
}