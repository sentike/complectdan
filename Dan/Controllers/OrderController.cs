﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Dan.Infrastructures.Controllers;
using Dan.Infrastructures.Repository;
using Dan.View.Cart;
using Dan.View.Order;

namespace Dan.Controllers
{
    public class OrderController : DefaultController
    {
        // GET: Order
        public ActionResult Index()
        {
            //======================================================================
            UpdateCartWish();

            //======================================================================
            var entity = OrderRepository.TakeEntities(CustomerKey, true).ToArray();
            var view = entity.AsView();

            //======================================================================
            return View(view);
        }

        // GET: Order/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }

            var order = TradecartRepository.Context.OrderEntities.SingleOrDefault(p => p.EntityId == id);
            if (order == null)
            {
                return RedirectToAction("Index");
            }

            var products = order.ItemEntities.AsView();
            var view = new TradecartOrderView
            {
                //Address = order.CustomerAddressEntity.AsShortView(),

                Products = products,
                Price = products.Sum(p => p.Amount * p.Cost),
                Weight = products.Sum(p => p.Amount * p.Product.Dimensions.Weight),
            };
            return View(view);
        }
    }
}
