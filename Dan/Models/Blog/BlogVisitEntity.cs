﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace Dan.Models.Blog
{
    public class BlogVisitEntity
    {
        public Guid VisitId { get; set; }

        //=======================================
        public long ArticleId { get; set; }
        public virtual BlogArticleEntity ArticleEntity { get; set; }

        //=======================================
        public string Referer { get; set; }
        public string IpAddress { get; set; }
        public string UserAgent { get; set; }
        public DateTime VisitDate { get; set; }

        //=======================================
        public override string ToString()
        {
            return $"{VisitId} | {ArticleId} - {ArticleEntity?.Name} | {VisitDate}";
        }

        //=======================================
        public class Configuration : EntityTypeConfiguration<BlogVisitEntity>
        {
            public Configuration()
            {
                Property(p => p.Referer).IsOptional().HasMaxLength(256);
                Property(p => p.UserAgent).IsOptional().HasMaxLength(128);
                Property(p => p.IpAddress).IsOptional().HasMaxLength(16);
                ToTable("blog.BlogVisitEntity");
                HasKey(p => p.VisitId);
            }
        }
    }
}