﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using AutoMapper;
using Dan.Areas.Admin.View.Image;
using Dan.Models.Image;
using Dan.Models.Interface;
using Dan.View.Image;

namespace Dan.Areas.Admin.View.Image
{
    public class AdminCatalogImageLargeView
        : AdminCatalogImageShortView
    {

    }


    public static partial class ViewHelper
    {
        public static List<AdminCatalogImageLargeView> AsLargeView(this List<ImageEntity> entity)
        {
            return entity.Select(e => e.AsLargeView()).ToList();
        }

        public static AdminCatalogImageLargeView AsLargeView(this ImageEntity entity)
        {
            Mapper.Initialize
            (
                c => c.CreateMap<ImageEntity, AdminCatalogImageLargeView>()
                .ForMember(p => p.AltAttribute, p => p.MapFrom(m => m.AltAttribute))
                .ForMember(p => p.TitleAttribute, p => p.MapFrom(m => m.TitleAttribute))
                .ForMember(p => p.CategoryName, p => p.MapFrom(m => m.CategoryEntity.Name))
            );
            return Mapper.Map<AdminCatalogImageLargeView>(entity);
        }

    }
}