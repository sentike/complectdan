﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Dan.Models;
using Dan.Models.Common;

namespace Dan.Infrastructures.Repository
{
    public class CustomerKey
    {
        public CustomerKey(Guid sessionId, Guid? userId)
        {
            SessionId = sessionId;
            UserId = userId;
        }

        public Guid SessionId { get; set; }
        public Guid? UserId { get; set; }
    }
}