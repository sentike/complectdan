﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace Dan.Models.Catalog
{
    public class ProductImageEntity
    {
        public long ImageId { get; set; }
        public long ProductId { get; set; }
        public virtual ProductEntity ProductEntity { get; set; }

        //===============================================================
        public class Configuration : EntityTypeConfiguration<ProductImageEntity>
        {
            public Configuration()
            {
                ToTable("catalog.ProductImageEntity");
                HasKey(p => p.ImageId).Property(p => p.ImageId).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            }
        }
    }
}