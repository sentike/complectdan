﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Dan.Models.Types
{
    [ComplexType]
    public class ShippingType
    {
        [Display(Name = "Доставка")]
        public bool IsShipEnabled { get; set; } = true;

        [Display(Name = "Бесплатная доставка")]
        public bool IsFreeShipping { get; set; } = false;

        [Display(Name = "Не подлежит возврату")]
        public bool NotReturnable { get; set; } = false;
    }
}