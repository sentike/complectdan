﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;
using Dan.Models.Catalog;

namespace Dan.Models.Order
{
    public class OrderItemEntity
    {
        public Guid EntityId { get; set; }
        public Guid OrderId { get; set; }
        public virtual OrderEntity OrderEntity { get; set; }

        public long ProductId { get; set; }
        public virtual ProductEntity ProductEntity { get; set; }

        public short Amount { get; set; }
        public decimal Price { get; set; }
        public decimal OriginalProductCost { get; set; }

        public class Configuration : EntityTypeConfiguration<OrderItemEntity>
        {
            public Configuration()
            {
                HasKey(i => i.EntityId);
                ToTable("wishlist.OrderItemEntity");

                HasRequired(p => p.ProductEntity).WithMany().HasForeignKey(p => p.ProductId).WillCascadeOnDelete(true);
            }
        }
    }
}