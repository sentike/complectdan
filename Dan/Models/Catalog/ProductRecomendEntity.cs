﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace Dan.Models.Catalog
{
    public class ProductRecomendEntity
    {
        public long EntityId { get; set; }
        public long ProductId { get; set; }
        public virtual ProductEntity ProductEntity { get; set; }

        public long RecomendId { get; set; }
        public virtual ProductEntity RecomendEntity { get; set; }

        public class Configuration : EntityTypeConfiguration<ProductRecomendEntity>
        {
            public Configuration()
            {
                HasKey(i => i.EntityId);
                ToTable("catalog.ProductRecomendEntity");
                HasRequired(p => p.RecomendEntity).WithMany().HasForeignKey(p => p.RecomendId).WillCascadeOnDelete(true);
            }
        }

    }
}