﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Dan.Models.Statistic
{
    public class ProductOrderViewWish
    {
        public byte Month { get; set; }
        public long Views { get; set; }
        public long Orders { get; set; }
        public long Wishes { get; set; }
    }
}