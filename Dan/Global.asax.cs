﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using AutoMapper;
using Dan.Infrastructures.Repository;
using Dan.Infrastructures.Session;

namespace Dan
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            Mapper.Initialize(c => c.CreateMissingTypeMaps = true);
        }

        protected void Session_Start(Object sender, EventArgs e)
        {
            //====================================================================================
            try
            {
                using (var r = new TradecartRepository())
                {
                    Guid? userId = null;
                    if (User.Identity.IsAuthenticated)
                    {
                        userId = User.Identity.GetUserGuid();
                    }

                    Session["CartAmount"] = r.CountEntities(new Guid(Session.SessionID), userId, false);
                    Session["CartCost"] = r.Sum(new Guid(Session.SessionID), userId);
                }
            }
            catch
            {
                Session["CartAmount"] = 0;
                Session["CartCost"] = 0;
            }


            try
            {
                using (var r = new OrderRepository())
                {
                    Guid? userId = null;
                    if (User.Identity.IsAuthenticated)
                    {
                        userId = User.Identity.GetUserGuid();
                    }

                    Session["CartWish"] = r.TakeCount(new CustomerKey(new Guid(Session.SessionID), userId));
                }
            }
            catch
            {
                Session["CartWish"] = 0;
            }



        }
    }
}
