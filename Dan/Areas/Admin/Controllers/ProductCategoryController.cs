﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Dan.Areas.Admin.Infrastuctures;
using Dan.Areas.Admin.View;
using Dan.Areas.Admin.View.Category;
using Dan.Areas.Admin.View.Home;
using Dan.Infrastructures.Repository;
using Dan.Models.Catalog;

namespace Dan.Areas.Admin.Controllers
{
    public class ProductCategoryController : AbstractController
    {
        // GET: Admin/Category
        public ActionResult Index()
        {
            using (var r = new CategoryRepository())
            {
                var entities = r.TakeEntities(null, false).ToList();
                var view = entities.AsShortAdminView();
                return View(view);
            }
        }

        // GET: Admin/Category/Details/5
        public ActionResult Details(long? id)
        {
            using (var r = new CategoryRepository())
            {
                if (id == null || id <= 0)
                {
                    return RedirectToAction("Index");
                }

                var entity = r.TakeEntity(id.Value);
                if (entity == null)
                {
                    return RedirectToAction("Index");
                }

                var view = entity.AsLargeAdminView();
                //===========================================
                try
                {
                    view.LatestOrders = r.Context.OrderEntities
                        .Where(p => p.ItemEntities.Any(q => q.ProductEntity.CategoryId == id.Value))
                        .OrderByDescending(p => p.CreatedDate).Take(30)
                        .ToList()
                        .AsShortAdminView();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }

                //===========================================
                return View(view);
            }
        }

        // GET: Admin/Category/Create
        public ActionResult Create(long? id)
        {
            using (var r1 = new CategoryRepository())
            {
                using (var r2 = new ImageRepository(r1.Context))
                {
                    using (var r3 = new ImageCategoryRepository(r1.Context))
                    {
                        var view = new AdminCategoryCreateView
                        {
                            ParentCategoryId = id ?? 0,
                            CategorySelectList = r1.TakeEntityList(id, null).WithDefaultElem("Выберите категорию"),
                            ImageSelectList = r2.TakeEntityList(),
                            ImageCategorySelectList = r3.TakeEntityList(null)
                        };
                        return View(view);
                    }
                }
            }
        }

        // POST: Admin/Category/Create
        [HttpPost]
        public ActionResult Create(AdminCategoryCreateView view)
        {
            CategoryEntity entity = null;
            using (var r1 = new CategoryRepository())
            {
                using (var r2 = new ImageRepository(r1.Context))
                {
                    using (var r3 = new ImageCategoryRepository(r1.Context))
                    {
                        if (ModelState.IsValid == false)
                        {
                            view.CategorySelectList = r1.TakeEntityList(view.ParentCategoryId, null);
                            view.ImageCategorySelectList = r3.TakeEntityList(null);
                            view.ImageSelectList = r2.TakeEntityList();
                            return View(view);
                        }

                        if (view.ImageId > 0 && r2.IsExistEntity(view.ImageId) == false)
                        {
                            ModelState.AddModelError(nameof(view.ImageId), "Не удалось найти изображение");
                            view.CategorySelectList = r1.TakeEntityList(view.ParentCategoryId, null);
                            view.ImageCategorySelectList = r3.TakeEntityList(null);
                            view.ImageSelectList = r2.TakeEntityList();
                            return View(view);
                        }

                        if (view.ParentCategoryId > 0 && r1.IsExistEntity(view.ParentCategoryId) == false)
                        {
                            ModelState.AddModelError(nameof(view.ImageId), "Не удалось найти родительскую категорию");
                            view.CategorySelectList = r1.TakeEntityList(view.ParentCategoryId, null);
                            view.ImageCategorySelectList = r3.TakeEntityList(null);
                            view.ImageSelectList = r2.TakeEntityList();
                            return View(view);
                        }

                        try
                        {
                            if (view.ImageId == 0)
                            {
                                var status = UploadImage(r3.Context, view.Image);
                                if (status == UploadImageResponse.Successfully)
                                {
                                    view.ImageId = view.Image.EntityId;
                                }
                                else
                                {
                                    view.ImageSelectList = r2.TakeEntityList().WithDefaultElem("Выберите изображение или загрузите новое");
                                    view.ImageCategorySelectList = r3.TakeEntityList(view.Image.CategoryId);
                                    view.CategorySelectList = r1.TakeEntityList(view.ParentCategoryId);
                                    return View(view);
                                }
                            }

                            entity = r1.Context.CategoryEntities.Add(view.AsEntity());
                            r1.SaveChanges();
                            return RedirectToAction("Index");
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e);
                            throw;
                        }

                    }
                }
            }
        }

        // GET: Admin/Category/Edit/5
        public ActionResult Edit(long? id)
        {
            using (var r1 = new CategoryRepository())
            {
                using (var r2 = new ImageRepository(r1.Context))
                {
                    using (var r3 = new ImageCategoryRepository(r1.Context))
                    {
                        CategoryEntity entity = r1.TakeEntity(id.Value);
                        AdminCategoryEditView view = entity.AsEditView();

                        view.CategorySelectList = r1.TakeEntityList(id, null).WithDefaultElem("Выберите категорию");
                        view.ImageSelectList = r2.TakeEntityList();
                        view.ImageCategorySelectList = r3.TakeEntityList(null);
                       
                        return View(view);
                    }
                }
            }
        }

        // POST: Admin/Category/Edit/5
        [HttpPost]
        public ActionResult Edit(long id, AdminCategoryEditView view)
        {
            using (var r1 = new CategoryRepository())
            {
                var entity = r1.TakeEntity(id);

                //=====================================================
                if (view.ParentCategoryId <= 0)
                {
                    entity.ParentCategoryId = null;
                }
                else
                {
                    entity.ParentCategoryId = view.ParentCategoryId;
                }
                entity.Name = view.Name;
                entity.FullDescription = view.FullDescription;
                entity.ShortDescription = view.ShortDescription;
                entity.Published = view.Published;
                entity.Deleted = view.Deleted;
                entity.ImageId = view.ImageId;
                entity.Seo = view.Seo;

                //=====================================================
                r1.SaveChanges();

                //=====================================================
                return RedirectToAction("Details", new {id});
            }
        }

        public const string HtmlNewLine = "<br>";

        // GET: Admin/Category/Delete/5
        public ActionResult Delete(long? id)
        {
            ViewBag.Title = "Удаление категории";

            using (var r = new CategoryRepository())
            {
                var entity = r.TakeEntity(id.Value);

                var sb = new StringBuilder(1024);
                    

                var categoryEntity = entity.TakeChildCaregoryEntities();
                var productEntity = entity.TakeChildProductEntities();

                var cats = categoryEntity.Take(10).Select(p => p.Name).ToArray();
                var prods = productEntity.Take(10).Select(p => p.Name).ToArray();

                sb.AppendLine($"===================================");
                sb.AppendLine($"Связанных категорий: {categoryEntity.LongCount()}");
                sb.AppendLine($"{string.Join(",", cats)}");

                sb.AppendLine($"===================================");
                sb.AppendLine($"Связанных товаров: {productEntity.LongCount()}");
                sb.AppendLine($"{string.Join(",", prods)}");

                sb.AppendLine($"===================================");
                sb.AppendLine($"{entity.FullDescription}");

                var view = new AdminUniversalDeleteView
                {
                    Name = $"{entity.ParentCategoryEntity?.Name} > {entity.Name}",
                    Description = sb.ToString(),
                    EntityId = entity.EntityId,
                    PublicKey = Guid.NewGuid(),
                };
                return View(view);
            }
        }

        public override DeleteProcessResponse OnDeleteProcess(AdminUniversalDeleteView view)
        {
            using (var r = new CategoryRepository())
            {
                var entity = r.TakeEntity(view.EntityId);
                if (entity == null)
                {
                    return DeleteProcessResponse.NotFound;
                }
                else if (entity.Deleted)
                {
                    return DeleteProcessResponse.AlreadyDeleted;
                }
                entity.Deleted = true;
                r.SaveChanges();
            }
            return DeleteProcessResponse.Successfully;
        }
    }
}
