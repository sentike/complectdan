﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;
using Dan.Models.Catalog;
using Dan.Models.Interface;
using Dan.Models.User;

namespace Dan.Models.Tradecart
{
    public class WishlistEntity : IWishItemInterface
    {
        public Guid EntityId { get; set; }
        public long ProductId { get; set; }
        public virtual ProductEntity ProductEntity { get; set; }

        public short Amount { get; set; }

        //============================================
        public Guid SessionId { get; set; }
        public Guid? UserId { get; set; }
        public virtual ApplicationUser UserEntity { get; set; }

        //============================================
        public DateTime CreatedDate { get; set; }
        public DateTime? DeleteDate { get; set; }

        //============================================
        public string Caption { get; set; }

        //============================================
        public class Configuration : EntityTypeConfiguration<WishlistEntity>
        {
            public Configuration()
            {
                HasKey(i => i.EntityId);
                ToTable("wishlist.WishlistEntity");
                Property(p => p.Caption).IsOptional().HasMaxLength(256);
                HasRequired(p => p.ProductEntity).WithMany().HasForeignKey(p => p.ProductId).WillCascadeOnDelete(true);
                HasOptional(p => p.UserEntity).WithMany(p => p.WishlistEntities).HasForeignKey(p => p.UserId).WillCascadeOnDelete(true);
            }
        }
    }
}