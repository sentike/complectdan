﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using AutoMapper;
using Dan.Models.Image;

namespace Dan.Areas.Admin.View.Image
{
    public class AdminImageCategoryShortView
    {
        [Display(Name = "#")]
        public long EntityId { get; set; }

        [Required, StringLength(64, MinimumLength = 4)]
        [Display(Name = "Наименование")]
        public string Name { get; set; }

        [Display(Name = "Изображений")]
        public long Count { get; set; }

        public override string ToString()
        {
            return $"[{EntityId}] : {Name} | Count: {Count}";
        }
    }

    public static partial class ViewHelper
    {
        public static List<AdminImageCategoryShortView> AsShortAdminView(this List<ImageCategoryEntity> entity)
        {
            return entity.Select(e => e.AsShortAdminView()).ToList();
        }

        public static AdminImageCategoryShortView AsShortAdminView(this ImageCategoryEntity entity)
        {
            Mapper.Initialize
            (
                c => c.CreateMap<ImageCategoryEntity, AdminImageCategoryShortView>()
                .ForMember(p => p.Count, p => p.MapFrom(m => m.ImageEntities.Count))
            );
            return Mapper.Map<AdminImageCategoryShortView>(entity);
        }

    }
}