﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using AutoMapper;
using Dan.Models.Order;
using Dan.Models.Tradecart;
using Dan.View.Category;
using Dan.View.Product;

namespace Dan.View.Cart
{
    public class ProductCartView
    {
        public Guid EntityId { get; set; }

        public TradeCartProductView Product { get; set; }
        public CatalogCategoryShortView Category { get; set; }

        [Display(Name = "Количество")]
        public short Amount { get; set; }

        [Display(Name = "Стоимость")]
        public decimal Cost { get; set; }

        [Display(Name = "Итого")]
        public decimal TotalCost => Cost * Amount;

        [Display(Name = "Общий вес")]
        public decimal TotalWeight => Product.Dimensions.Weight * Amount;
    }

    public static partial class ViewHelper
    {
        //====================================================================================================================
        public static List<OrderItemEntity> AsOrder(this ICollection<TradecartEntity> entity)
        {
            return entity.Select(e => e.AsOrder()).ToList();
        }

        public static OrderItemEntity AsOrder(this TradecartEntity entity)
        {
            Mapper.Initialize
            (
                c => c.CreateMap<TradecartEntity, OrderItemEntity>()
                    .ForSourceMember(p => p.EntityId, p => p.Ignore())
                    .ForMember(p => p.EntityId, m => m.UseValue(Guid.NewGuid()))
            );
            return Mapper.Map<OrderItemEntity>(entity);
        }

        //====================================================================================================================
        public static List<ProductCartView> AsView(this ICollection<TradecartEntity> entity)
        {
            return entity.Select(e => e.AsView()).ToList();
        }

        public static ProductCartView AsView(this TradecartEntity entity)
        {
            Mapper.Initialize
            (
                c => c.CreateMap<TradecartEntity, ProductCartView>()
                    .ForMember(p => p.Product, m => m.MapFrom(r => r.ProductEntity.AsTradeCartView()))
                    .ForMember(p => p.Category, m => m.MapFrom(r => r.ProductEntity.CategoryEntity.AsShortView()))
                    .ForMember(p => p.Cost, m => m.MapFrom(r => r.Price))
            );
            return Mapper.Map<ProductCartView>(entity);
        }

        //====================================================================================================================
        public static List<ProductCartView> AsView(this ICollection<OrderItemEntity> entity)
        {
            return entity.Select(e => e.AsView()).ToList();
        }

        public static ProductCartView AsView(this OrderItemEntity entity)
        {
            Mapper.Initialize
            (
                c => c.CreateMap<OrderItemEntity, ProductCartView>()
                    .ForMember(p => p.Product, m => m.MapFrom(r => r.ProductEntity.AsTradeCartView()))
                    .ForMember(p => p.Category, m => m.MapFrom(r => r.ProductEntity.CategoryEntity.AsShortView()))
                    .ForMember(p => p.Cost, m => m.MapFrom(r => r.Price))
            );
            return Mapper.Map<ProductCartView>(entity);
        }
    }
}