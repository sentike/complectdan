﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Dan.Models.Interface;

namespace Dan.Areas.Admin.View.Manufacture
{
    public class AdminManafactureLargeView
        : AdminManafactureShortView
    {
        public SeoEntityType Seo { get; set; }
    }
}