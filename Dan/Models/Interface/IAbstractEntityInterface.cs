﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dan.Models.Interface
{
    public interface IAbstractEntityInterface
    {
        long EntityId { get; set; }

        /// <summary>
        /// Gets or sets the name
        /// </summary>
        string Name { get; set; }
        
        /// <summary>
        /// Gets or sets the short description
        /// </summary>
        string ShortDescription { get; set; }
        
        /// <summary>
        /// Gets or sets the full description
        /// </summary>
        string FullDescription { get; set; }
    }
}
