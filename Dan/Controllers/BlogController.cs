﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Dan.Infrastructures.Repository;

namespace Dan.Controllers
{
    public class BlogController : Controller
    {
        // GET: Blog
        public ActionResult Index()
        {
            using (var r = new BlogArticleRepositor())
            {
                var entities = r.TakeEntities(null, false);
                var view = entities;
                return View(view);
            }
        }
    }
}