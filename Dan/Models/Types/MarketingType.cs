﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Dan.Models.Types
{
    [ComplexType]
    public class MarketingType
    {
        [Display(Name = "Отображать как новинку")]
        public bool MarkAsNew { get; set; }

        [Display(Name = "Дата начала")]
        public DateTime? MarkAsNewStartDateTimeUtc { get; set; }

        [Display(Name = "Дата окончания")]
        public DateTime? MarkAsNewEndDateTimeUtc { get; set; }
    }
}