﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Dan.Areas.Admin.View.Image;
using Dan.Infrastructures.Repository;
using Dan.Models.Image;

namespace Dan.Areas.Admin.Controllers
{
    public class ProductImageCategoryController : Controller
    {
        // GET: Admin/ImageCategory
        public ActionResult Index()
        {
            using (var r1 = new ImageCategoryRepository())
            {
                var categories = r1.TakeEntities(null, false).ToList();
                var view = categories.AsShortAdminView();
                return View(view);
            }
        }

        // GET: Admin/ImageCategory/Details/5
        public ActionResult Details(long? id)
        {
            using (var r1 = new ImageCategoryRepository())
            {
                if (id == null || id < 0)
                {
                    return RedirectToAction("Index");
                }

                var entity = r1.TakeEntity(id.Value);
                if (entity == null)
                {
                    return RedirectToAction("Index");
                }

                var view = entity.AsLargeAdminView();
                return View(view);
            }
        }

        // GET: Admin/ImageCategory/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/ImageCategory/Create
        [HttpPost]
        public ActionResult Create(AdminImageCategoryCreateView view)
        {
            try
            {
                if (ModelState.IsValid == false)
                {
                    return View(view);
                }

                using (var r1 = new ImageCategoryRepository())
                {
                    var entity = r1.Context.ImageCategoryEntities.Add(new ImageCategoryEntity()
                    {
                        Name = view.Name
                    });

                    r1.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            catch
            {
                return View(view);
            }
        }

        // GET: Admin/ImageCategory/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Admin/ImageCategory/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Admin/ImageCategory/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Admin/ImageCategory/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
