﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Dan.Infrastructures.Session;
using Dan.Models;
using Dan.Models.Common;
using Dan.Models.Order;
using Dan.Models.Tradecart;
using Dan.View.Cart;
using Microsoft.AspNet.Identity;

namespace Dan.Infrastructures.Repository
{

    public class OrderRepository : AbstractRepository<OrderEntity, Guid, CustomerKey>
    {
        public OrderRepository() { }
        public OrderRepository(ApplicationDbContext context): base(context) { }

        public override DbSet<OrderEntity> Model => Context.OrderEntities;

        public override OrderEntity TakeEntity(Guid key)
        {
            return Model.SingleOrDefault(p => p.EntityId == key);
        }

        public long TakeCount(CustomerKey key)
        {
            return TakeEntities(key, true).Count(p => p.OrderStatus != CustomerOrderStatus.Canceled && p.OrderStatus != CustomerOrderStatus.Delivered);
        }

        public override IQueryable<OrderEntity> TakeEntities(CustomerKey key, bool include)
        {
            return Model.Where(p => p.SessionId == key.SessionId || p.UserId == key.UserId)
                .OrderByDescending(p => p.CreatedDate);
        }

        public OrderEntity CreateOrder(
            ICollection<TradecartEntity> products,
            CustomerKey customer,
            CustomerAddressEntity address,
            DeliveryMethod delivery,
            string agent,
            string ip)
        {
            //==========================================
            var order = new OrderEntity
            {
                UserId = customer.UserId,
                SessionId = customer.SessionId,
                EntityId = Guid.NewGuid(),
                CustomerAddressEntity = address,
                CreatedDate = DateTime.Now,
                IpAddress = ip,
                OrderStatus = CustomerOrderStatus.Processing,
                ShippingMethod = delivery,
                UserAgent = agent,
                ItemEntities = products.AsOrder()
            };

            //==========================================
            return Model.Add(order);
        }

        public const string HtmlNewLine = "<br>";
        public const string SiteName = "\"Интернет-магазин спортивных товаров\"";
        public const string SiteAddress = "http://localhost:4443/";
        public const string SiteContact = "346400, Ростовская обл, г.Новочеркасск, ул.Первомайская, д.164. ИНН: 6150067968";


        public static void SendConfirmCreateMessage(OrderEntity entity)
        {
            var sb = new StringBuilder(8192);

            //------------------------------------------------------------------------------------------
            sb.AppendLine($"{HtmlNewLine}===========================================");
            sb.AppendLine($"{HtmlNewLine}Информация о заказе:");
            sb.AppendLine($"{HtmlNewLine}Номер заказа: {entity.EntityId}");
            sb.AppendLine($"{HtmlNewLine}Дата заказа: {entity.CreatedDate}");
            sb.AppendLine($"{HtmlNewLine}Сумма заказа: {entity.Cost:F} руб");
            sb.AppendLine($"{HtmlNewLine}<b>Менеджер подтвердил ваш заказ!</b>");
            sb.AppendLine($"{HtmlNewLine}===========================================");
            {
                var itter = 1;
                foreach (var item in entity.ItemEntities)
                {
                    sb.AppendLine($"{HtmlNewLine}{itter++}. {item.ProductEntity.Name} - {item.Price:F} руб, {item.Amount} шт, сумма: {item.Price * item.Amount:F} руб");
                }
            }
            sb.AppendLine($"{HtmlNewLine}===========================================");
            sb.AppendLine($"{HtmlNewLine}Контактная информация:");
            sb.AppendLine($"{HtmlNewLine}Получатель: {entity.CustomerAddressEntity.UserName}");
            sb.AppendLine($"{HtmlNewLine}Номер телефона: {entity.CustomerAddressEntity.PhoneNumber}");

            sb.AppendLine($"{HtmlNewLine}===========================================");
            sb.AppendLine($"{HtmlNewLine}{SiteName} ({SiteAddress}). Адрес: {SiteContact}");

            //------------------------------------------------------------------------------------------
            try
            {
                Task.Run(() => new EmailService().SendAsync(new IdentityMessage
                {
                    Destination = entity.CustomerAddressEntity.Email,
                    Body = sb.ToString(),
                    Subject = "Информация о заказе"
                })).Wait();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        public static void SendConfirmPaymentMessage(OrderEntity entity)
        {
            var sb = new StringBuilder(8192);

            //------------------------------------------------------------------------------------------
            sb.AppendLine($"{HtmlNewLine}===========================================");
            sb.AppendLine($"{HtmlNewLine}Информация о заказе:");
            sb.AppendLine($"{HtmlNewLine}Номер заказа: {entity.EntityId}");
            sb.AppendLine($"{HtmlNewLine}Дата заказа: {entity.CreatedDate}");
            sb.AppendLine($"{HtmlNewLine}Сумма заказа: {entity.Cost:F} руб");
            sb.AppendLine($"{HtmlNewLine}<b>Менеджер подтвердил оплату заказа!</b>");
            if (entity.ShippingMethod == DeliveryMethod.Shipping)
            {
                sb.AppendLine($"{HtmlNewLine}<b>В близжайшее время ваш заказ будет собран и отправлен</b>");
            }
            else
            {
                sb.AppendLine($"{HtmlNewLine}<b>Ваш заказ готов к выдаче. Для получения заказа предъявите паспорт.</b>");
            }

            sb.AppendLine($"{HtmlNewLine}===========================================");
            {
                var itter = 1;
                foreach (var item in entity.ItemEntities)
                {
                    sb.AppendLine($"{HtmlNewLine}{itter++}. {item.ProductEntity.Name} - {item.Price:F} руб, {item.Amount} шт, сумма: {item.Price * item.Amount:F} руб");
                }
            }

            //------------------------------------------------------------------------------------------
            sb.AppendLine($"{HtmlNewLine}===========================================");
            sb.AppendLine($"{HtmlNewLine}Контактная информация:");
            sb.AppendLine($"{HtmlNewLine}Получатель: {entity.CustomerAddressEntity.UserName}");
            if (entity.ShippingMethod == DeliveryMethod.Shipping)
            {
                sb.AppendLine($"{HtmlNewLine}Адрес доставки: {entity.CustomerAddressEntity.SummaryAddress}");
            }
            sb.AppendLine($"{HtmlNewLine}Номер телефона: {entity.CustomerAddressEntity.PhoneNumber}");

            sb.AppendLine($"{HtmlNewLine}===========================================");
            sb.AppendLine($"{HtmlNewLine}{SiteName} ({SiteAddress}). Адрес: {SiteContact}");

            //------------------------------------------------------------------------------------------
            try
            {
                Task.Run(() => new EmailService().SendAsync(new IdentityMessage
                {
                    Destination = entity.CustomerAddressEntity.Email,
                    Body = sb.ToString(),
                    Subject = "Информация о заказе"
                })).Wait();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        public static void SendConfirmDeliveryMessage(OrderEntity entity)
        {
            var sb = new StringBuilder(8192);

            //------------------------------------------------------------------------------------------
            sb.AppendLine($"{HtmlNewLine}===========================================");
            sb.AppendLine($"{HtmlNewLine}Информация о заказе:");
            sb.AppendLine($"{HtmlNewLine}Номер заказа: {entity.EntityId}");
            sb.AppendLine($"{HtmlNewLine}Дата заказа: {entity.CreatedDate}");
            sb.AppendLine($"{HtmlNewLine}Сумма заказа: {entity.Cost:F} руб");
            if (entity.ShippingMethod == DeliveryMethod.Shipping)
            {
                sb.AppendLine($"{HtmlNewLine}Номер отправления: <b>{entity.DeliveryToken}</b>");
                sb.AppendLine($"{HtmlNewLine}<b>Ваш заказ был собран и отправлен!</b>");
            }


            sb.AppendLine($"{HtmlNewLine}===========================================");
            {
                var itter = 1;
                foreach (var item in entity.ItemEntities)
                {
                    sb.AppendLine($"{HtmlNewLine}{itter++}. {item.ProductEntity.Name} - {item.Price:F} руб, {item.Amount} шт, сумма: {item.Price * item.Amount:F} руб");
                }
            }

            //------------------------------------------------------------------------------------------
            sb.AppendLine($"{HtmlNewLine}===========================================");
            sb.AppendLine($"{HtmlNewLine}Контактная информация:");
            sb.AppendLine($"{HtmlNewLine}Получатель: {entity.CustomerAddressEntity.UserName}");
            if (entity.ShippingMethod == DeliveryMethod.Shipping)
            {
                sb.AppendLine($"{HtmlNewLine}Адрес доставки: {entity.CustomerAddressEntity.SummaryAddress}");
            }
            sb.AppendLine($"{HtmlNewLine}Номер телефона: {entity.CustomerAddressEntity.PhoneNumber}");

            sb.AppendLine($"{HtmlNewLine}===========================================");
            sb.AppendLine($"{HtmlNewLine}{SiteName} ({SiteAddress}). Адрес: {SiteContact}");

            //------------------------------------------------------------------------------------------
            try
            {
                Task.Run(() => new EmailService().SendAsync(new IdentityMessage
                {
                    Destination = entity.CustomerAddressEntity.Email,
                    Body = sb.ToString(),
                    Subject = "Информация о заказе"
                })).Wait();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        public static void SendCancelOrderMessage(OrderEntity entity)
        {
            var sb = new StringBuilder(8192);

            //------------------------------------------------------------------------------------------
            sb.AppendLine($"{HtmlNewLine}===========================================");
            sb.AppendLine($"{HtmlNewLine}Информация о заказе:");
            sb.AppendLine($"{HtmlNewLine}Номер заказа: {entity.EntityId}");
            sb.AppendLine($"{HtmlNewLine}Дата заказа: {entity.CreatedDate}");
            sb.AppendLine($"{HtmlNewLine}Сумма заказа: {entity.Cost:F} руб");
            sb.AppendLine($"{HtmlNewLine}<b>Менджер отменил ваш заказ!</b>");


            sb.AppendLine($"{HtmlNewLine}===========================================");
            sb.AppendLine($"{HtmlNewLine}Коментарий: {entity.AdminComment}");

            sb.AppendLine($"{HtmlNewLine}===========================================");
            sb.AppendLine($"{HtmlNewLine}{SiteName} ({SiteAddress}). Адрес: {SiteContact}");

            //------------------------------------------------------------------------------------------
            try
            {
                Task.Run(() => new EmailService().SendAsync(new IdentityMessage
                {
                    Destination = entity.CustomerAddressEntity.Email,
                    Body = sb.ToString(),
                    Subject = "Информация о заказе"
                })).Wait();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        public static void SendCreateMessage(OrderEntity entity)
        {
            var sb = new StringBuilder(8192);

            //------------------------------------------------------------------------------------------
            sb.AppendLine($"{HtmlNewLine}===========================================");
            sb.AppendLine($"{HtmlNewLine}Информация о заказе:");
            sb.AppendLine($"{HtmlNewLine}Номер заказа: {entity.EntityId}");
            sb.AppendLine($"{HtmlNewLine}Дата заказа: {entity.CreatedDate}");
            sb.AppendLine($"{HtmlNewLine}Сумма заказа: {entity.Cost:F} руб");
            sb.AppendLine($"{HtmlNewLine}===========================================");
            {
                var itter = 1;
                foreach (var item in entity.ItemEntities)
                {
                    sb.AppendLine($"{HtmlNewLine}{itter++}. {item.ProductEntity.Name} - {item.Price:F} руб, {item.Amount} шт, сумма: {item.Price * item.Amount:F} руб");
                }
            }
            sb.AppendLine($"{HtmlNewLine}===========================================");
            sb.AppendLine($"{HtmlNewLine}Контактная информация:");
            sb.AppendLine($"{HtmlNewLine}Получатель: {entity.CustomerAddressEntity.UserName}");
            sb.AppendLine($"{HtmlNewLine}Номер телефона: {entity.CustomerAddressEntity.PhoneNumber}");

            sb.AppendLine($"{HtmlNewLine}===========================================");
            sb.AppendLine($"{HtmlNewLine}{SiteName} ({SiteAddress}). Адрес: {SiteContact}");

            //------------------------------------------------------------------------------------------
            try
            {
                Task.Run(() => new EmailService().SendAsync(new IdentityMessage
                {
                    Destination = entity.CustomerAddressEntity.Email,
                    Body = sb.ToString(),
                    Subject = "Информация о заказе"
                })).Wait();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

    }
}