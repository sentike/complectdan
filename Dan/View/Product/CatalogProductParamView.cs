﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using Dan.Models.Catalog;

namespace Dan.View.Product
{
    public class CatalogProductParamView
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }

    public static class CatalogProductParamViewHelper
    {
        public static List<CatalogProductParamView> AsView(this List<ProductParamEntity> entity)
        {
            return entity.Select(p => p.AsView()).ToList();
        }

        public static CatalogProductParamView AsView(this ProductParamEntity entity)
        {
            Mapper.Initialize
            (
                c => c.CreateMap<ProductParamEntity, CatalogProductParamView>()
                .ForMember(m => m.Name, m => m.MapFrom(r => r.ParamEnity.Name))
            );
            return Mapper.Map<CatalogProductParamView>(entity);
        }

    }
}