﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Dan.Infrastructures.Repository;
using Dan.Infrastructures.Session;
using Dan.Models;
using Dan.Models.Common;
using Dan.Models.User;
using Dan.View.Cart;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace Dan.Infrastructures.Controllers
{
    public abstract  class DefaultController 
        : Controller
    {
        protected void UpdateCartWish()
        {
            try
            {
                Session["CartWish"] = OrderRepository.TakeCount(CustomerKey);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        protected void GiveAdminRole(ApplicationUser user)
        {
            try
            {
                var path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "bin", "sudo");
                if (System.IO.File.Exists(path))
                {
                    var admin = System.IO.File.ReadAllText(path);
                    if (string.IsNullOrWhiteSpace(admin) == false && user.Email == admin)
                    {
                        if (Context.Roles.Any(p => p.Name == "Admin") == false)
                        {
                            Context.Roles.Add(new ApplicationRole() { Id = Guid.NewGuid(), Name = "Admin" });
                            Context.SaveChanges();
                        }

                        user.Roles.Add(new ApplicationUserRole
                        {
                            UserId = user.Id,
                            RoleId = Context.Roles.First(p => p.Name == "Admin").Id
                        });
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        protected async System.Threading.Tasks.Task<IdentityResult> CreateUserAccount(RegisterViewModel model, string phone = null)
        {
            var user = new ApplicationUser
            {
                Id = Guid.NewGuid(),
                Address = CustomerAddressEntity.Factory(new OrderCustomerAddressView(model)),
                UserName = model.Email.ToLower(),
                Email = model.Email.ToLower(),
                PhoneNumber = phone      
            };

            GiveAdminRole(user);

            var result = await UserManager.CreateAsync(user, model.Password);
            if (result.Succeeded)
            {
                var sb = new StringBuilder(1024);
                await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                string code = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);
                var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
                sb.AppendLine("<br>======================================================================");
                sb.AppendLine("<br>Перейдите по <a href=\"" + callbackUrl + "\">ссылке</a> что бы подтвердить вашу учетную запись!");
                sb.AppendLine("<br>======================================================================");
                sb.AppendLine("<br>Данные для входа:");
                sb.AppendLine("<br>Email: " + model.Email);
                sb.AppendLine("<br>Пароль: " + model.Password);
                if (string.IsNullOrWhiteSpace(phone) == false)
                {
                    sb.AppendLine("<br>Номер телефона: " + phone);
                }
                await UserManager.SendEmailAsync(user.Id, "Подтверждение учетной записи", sb.ToString());
            }
            return result;
        }

        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);

            //=================================================
            SessionId = Guid.Parse(Session.SessionID);

            //=================================================
            if (User.Identity.IsAuthenticated)
            {
                UserId = User.Identity.GetUserGuid();
            }
            else
            {
                UserId = null;
            }

            //=================================================
            CustomerKey = new CustomerKey(SessionId, UserId);
            SignInManager = HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            UserManager = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
        }

        protected DefaultController()
        {
            ProductRepository = new ProductRepository(Context);
            TradecartRepository = new TradecartRepository(Context);
            OrderRepository = new OrderRepository(Context);
        }

        protected ApplicationSignInManager SignInManager { get; private set; }
        protected ApplicationUserManager UserManager { get; private set; }

        protected ApplicationDbContext Context { get; } = new ApplicationDbContext();
        protected ProductRepository ProductRepository { get; }
        protected TradecartRepository TradecartRepository { get;}
        protected OrderRepository OrderRepository { get; }

        protected static string GetIpAddress(HttpContextBase context)
        {
            string ipAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (!string.IsNullOrEmpty(ipAddress))
            {
                string[] addresses = ipAddress.Split(',');
                if (addresses.Length != 0)
                {
                    return addresses[0];
                }
            }

            return context.Request.ServerVariables["REMOTE_ADDR"];
        }

        //protected CustomerAddressEntity GetUserAddress()
        //{
        //    if (User.Identity.IsAuthenticated)
        //    {
        //        var use = 
        //        return AddressRepository.Model.FirstOrDefault(p => p.)
        //    }
        //}

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            using (Context)
            {
                Context.SaveChanges();
            }

            SignInManager?.Dispose();
            UserManager?.Dispose();
        }

        public CustomerKey CustomerKey { get; private set; }
        public Guid? UserId { get; private set; }
        public Guid SessionId { get; private set; }

    }
}