﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;
using Dan.Models.Image;
using Dan.Models.Interface;

namespace Dan.Models.Catalog
{
    public class ManufactureEntity 
        : IAbstractEntityInterface
        , IEntityWithImageInterface
    {
        public long EntityId { get; set; }
        public string Name { get; set; }
        public string ShortDescription { get; set; }
        public string FullDescription { get; set; }

        public SeoEntityType Seo { get; set; }


        public long ImageId { get; set; }
        public virtual ImageEntity ImagEntity { get; set; }
        public virtual List<ProductEntity> ProductEntities { get; set; } = new List<ProductEntity>();

        public class Configuration : EntityTypeConfiguration<ManufactureEntity>
        {
            public Configuration()
            {
                HasKey(i => i.EntityId);
                ToTable("catalog.ManufactureEntity");

                //=================================================================
                HasMany(p => p.ProductEntities).WithRequired(p => p.ManufactureEntity).HasForeignKey(p => p.ManufactureId).WillCascadeOnDelete(true);

                //=================================================================
                Property(p => p.Name).IsRequired().HasMaxLength(128);
                Property(p => p.ShortDescription).IsRequired().HasMaxLength(4096);
                Property(p => p.FullDescription).IsRequired().HasMaxLength(4096 * 4);
            }
        }
    }
}