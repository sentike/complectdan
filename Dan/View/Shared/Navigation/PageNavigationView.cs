﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Dan.View.Shared.Navigation
{
    public class PageNavigationView<TSource> : IPageNavigationView
        where TSource : class
    {
        protected const int ItemPerPageDefault = 20;

        public IReadOnlyCollection<TSource> List { get; protected set; }

        public int PageBegin => Math.Max(PageNo - 10, 1);
        public int PageEnd => Math.Min(PageNo + 10, CountPage);

        public int PageNo { get; protected set; }

        public int CountPage { get; protected set; }

        public int ItemPerPage { get; protected set; }

        protected PageNavigationView()
        {
            try
            {
                ControllerName = HttpContext.Current.Request.RequestContext.RouteData.Values["controller"].ToString();
                ControllerAction = HttpContext.Current.Request.RequestContext.RouteData.Values["action"].ToString();
            }
            catch
            {
                // ignored
            }
        }

        public PageNavigationView(IQueryable<TSource> queryableSet, int page, int itemPerPage = 0)
            : this()
        {
            if (queryableSet.Any() == false)
            {
                List = new List<TSource>(0);
            }
            else
            {
                if (itemPerPage == 0)
                {
                    itemPerPage = ItemPerPageDefault;
                }
                ItemPerPage = itemPerPage;

                PageNo = page;
                var count = queryableSet.Count();

                CountPage = (int)decimal.Remainder(count, itemPerPage) == 0 ? count / itemPerPage : count / itemPerPage + 1;
                List = queryableSet.Skip((PageNo - 1) * itemPerPage).Take(itemPerPage).ToList();
            }
        }

        public string ControllerName { get; set; }
        public string ControllerAction { get; set; }
    }

    public class PageNavigationView<TSource, TResult> : PageNavigationView<TResult>
        where TSource : class
        where TResult : class
    {
        public PageNavigationView(IQueryable<TSource> queryableSet, Func<TSource, TResult> selector, int page, int itemPerPage = 0)
        {
            if (selector == null)
            {
                throw new ArgumentNullException(nameof(selector),
                    $"Необходимо указать способ переобразования объекта из Entity({typeof(TSource)}) в View({typeof(TResult)})!");
            }
            if (queryableSet.Any() == false)
            {
                List = new List<TResult>(0);
            }
            else
            {
                if (itemPerPage <= 0)
                {
                    itemPerPage = ItemPerPageDefault;
                }
                ItemPerPage = itemPerPage;

                PageNo = page;
                var count = queryableSet.Count();

                CountPage = (int)decimal.Remainder(count, itemPerPage) == 0 ? count / itemPerPage : count / itemPerPage + 1;
                List = queryableSet.Skip((PageNo - 1) * itemPerPage).Take(itemPerPage).ToList().Select(selector).ToList();
            }
        }
    }
}