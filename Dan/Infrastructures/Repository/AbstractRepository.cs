﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Dan.Models;

namespace Dan.Infrastructures.Repository
{
    public abstract class AbstractRepository : IDisposable
    {
        internal AbstractRepository()
        {
            AllowDispose = true;
            Context = new ApplicationDbContext();
        }

        internal AbstractRepository(ApplicationDbContext context)
        {
            AllowDispose = false;
            Context = context;
        }


        protected bool AllowDispose { get; }
        public ApplicationDbContext Context { get; }
        public int SaveChanges() => Context.SaveChanges();

        public void Dispose()
        {
            if (AllowDispose)
            {
                Context?.Dispose();
            }
        }
    }

    public abstract class AbstractRepository<TEntity,TSingleKey,TMultipleKey> : AbstractRepository
        where TEntity : class
    {
        public abstract DbSet<TEntity> Model { get; }
        public abstract TEntity TakeEntity(TSingleKey key);
        public abstract IQueryable<TEntity> TakeEntities(TMultipleKey key, bool include);
        public virtual bool IsExistEntity(TSingleKey key) => TakeEntity(key) != null;

        internal AbstractRepository() : base()
        {
        }

        internal AbstractRepository(ApplicationDbContext context) : base(context)
        {
        }
    }

    public abstract class AbstractRepository
        <
            TEntity, 
            TSingleKey, 
            TMultipleKey,
            TOrderMethodKey
        > : AbstractRepository
        where TEntity : class 
    {
        public abstract DbSet<TEntity> Model { get; }
        public abstract TEntity TakeEntity(TSingleKey key);
        public abstract IQueryable<TEntity> TakeEntities(TMultipleKey key, TOrderMethodKey order, bool include);


        internal AbstractRepository() : base()
        {
        }

        internal AbstractRepository(ApplicationDbContext context) : base(context)
        {
        }
    }
}