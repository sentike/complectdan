﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Dan.Models;
using Dan.Models.Catalog;

namespace Dan.Infrastructures.Repository
{
    public class ManufactureRepository : AbstractRepository<ManufactureEntity, long, long?>
    {
        public override DbSet<ManufactureEntity> Model => Context.ManufactureEntities;

        public override ManufactureEntity TakeEntity(long key)
        {
            return Context.ManufactureEntities.SingleOrDefault(p => p.EntityId == key);
        }

        public List<SelectListItem> TakeEntityList(long? key)
        {
            return TakeEntities(null, false).ToList().Select(p => new SelectListItem
            {
                Selected = p.EntityId == key,
                Value = p.EntityId.ToString(),
                Text = p.Name,
            }).ToList();
        }

        public override IQueryable<ManufactureEntity> TakeEntities(long? key = null, bool include = false)
        {
            return Context.ManufactureEntities.OrderBy(p => p.EntityId);
        }

        //=========================================================================
        public ManufactureRepository() { }
        public ManufactureRepository(ApplicationDbContext context): base(context) { }
    }
}