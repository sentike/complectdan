﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace Dan.Models.Image
{
    public class ImageCategoryEntity
    {
        public long EntityId { get; set; }
        public string Name { get; set; }
        public virtual List<ImageEntity> ImageEntities { get; set; } = new List<ImageEntity>(16);

        public override string ToString()
        {
            return $"[{EntityId}] : {Name} | Images: {ImageEntities.Count}";
        }

        public class Configuration : EntityTypeConfiguration<ImageCategoryEntity>
        {
            public Configuration()
            {
                HasKey(i => i.EntityId);
                ToTable("image.ImageCategoryEntity");

                //=================================================================
                HasMany(p => p.ImageEntities).WithRequired(p => p.CategoryEntity).HasForeignKey(p => p.CategoryId);

                //=================================================================
                Property(p => p.Name).IsRequired().HasMaxLength(256);
            }
        }
    }
}