﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using AutoMapper;
using Dan.Models.Image;
using Dan.Models.Interface;
using Dan.View.Image;

namespace Dan.Areas.Admin.View.Image
{
    public class AdminCatalogImageShortView 
        : ICategoryPropertyInterface
    {
        [Display(Name = "#")]
        public long EntityId { get; set; }

        [Display(Name = "Название")]
        public string Name { get; set; }

        [Display(Name = "Категория")]
        public long CategoryId { get; set; }

        [Display(Name = "Категория")]
        public string CategoryName { get; set; }

        [Display(Name = "Формат")]
        public FileImageFormat ImageFormat { get; set; }

        [Display(Name = "Описание")]
        public string AltAttribute { get; set; }

        [Display(Name = "Заголовок")]
        public string TitleAttribute { get; set; }

        [Display(Name = "Дата создания")]
        public DateTime CreatedDate { get; set; }

        public string FileName { get; set; }
        public string FilePath => $"{ImageEntity.RootDirectory}/{FileName}";
    }


    public static partial class ViewHelper
    {
        public static List<AdminCatalogImageShortView> AsShortAdminView(this List<ImageEntity> entity)
        {
            return entity.Select(e => e.AsShortAdminView()).ToList();
        }

        public static AdminCatalogImageShortView AsShortAdminView(this ImageEntity entity)
        {
            Mapper.Initialize
            (
                c => c.CreateMap<ImageEntity, AdminCatalogImageShortView>()
                .ForMember(p => p.FileName, p => p.MapFrom(m => m.SmallFileName))
                .ForMember(p => p.AltAttribute, p => p.MapFrom(m => m.AltAttribute))
                .ForMember(p => p.TitleAttribute, p => p.MapFrom(m => m.TitleAttribute))
                .ForMember(p => p.CategoryName, p => p.MapFrom(m => m.CategoryEntity.Name))
            );
            return Mapper.Map<AdminCatalogImageShortView>(entity);
        }

    }
}