﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Dan.Models;
using Dan.Models.Catalog;
using Dan.Models.Tradecart;

namespace Dan.Infrastructures.Repository
{
    public class TradecartRepository : AbstractRepository<TradecartEntity, Guid, Guid?>
    {
        public override DbSet<TradecartEntity> Model => Context.UserTradecartEntities;

        public override TradecartEntity TakeEntity(Guid key)
        {
            return Context.UserTradecartEntities.SingleOrDefault(p => p.EntityId == key);
        }

        public override IQueryable<TradecartEntity> TakeEntities(Guid? key, bool include)
        {
            throw new NotSupportedException();
        }

        public IQueryable<TradecartEntity> TakeEntities(Guid session, Guid? user)
        {
            return Context.UserTradecartEntities.Where(p => p.SessionId == session || user != null && p.UserId == user);
        }

        public decimal Sum(Guid session, Guid? user)
        {
            var entities = TakeEntities(session, user);
            if (entities.Any())
            {
                return entities.Sum(p => p.Amount * p.ProductEntity.Price.Price);
            }
            return 0;
        }

        public long CountEntities(Guid session, Guid? user, bool total)
        {
            if (total) return TakeEntities(session, user).LongCount();
            return TakeEntities(session, user).Where(p => p.DeleteDate == null).Select(p => p.Amount).DefaultIfEmpty((short)0).Sum(p => p);
        }

        public TradecartEntity AddEntity(ProductEntity product, Guid session, Guid? user, short amount = 1)
        {
            var entity = TakeEntities(session, user).FirstOrDefault(p => p.ProductId == product.EntityId && p.DeleteDate == null);
            if (entity == null)
            {
                entity = Context.UserTradecartEntities.Add(new TradecartEntity(product, session, user, amount));
            }
            else
            {
                entity.Amount++;
            }

            if (entity.Price == 0)
            {
                entity.Price = product.Price.Price;
            }

            Context.SaveChanges();
            return entity;
        }

        //=========================================================================
        public TradecartRepository() { }
        public TradecartRepository(ApplicationDbContext context): base(context) { }
    }
}