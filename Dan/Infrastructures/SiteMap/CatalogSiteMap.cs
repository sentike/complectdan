﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Dan.Models;
using Dan.Models.Catalog;
using MvcSiteMapProvider;

namespace Dan.Infrastructures.SiteMap
{
    public class CatalogSiteMap : DynamicNodeProviderBase
    {
        public static DynamicNode CreateCategory(DynamicNode parrent, CategoryEntity entity)
        {
            return new DynamicNode
            {
                Clickable = true,
                ParentKey = parrent?.Key,
                Key = $"Category_{entity.EntityId}",
                Action = "Category",
                Controller = "Catalog",
                Title = entity.Name,
                RouteValues = new Dictionary<string, object>()
                {
                    {"id", entity.EntityId}
                }
            };
        }


        public override IEnumerable<DynamicNode> GetDynamicNodeCollection(ISiteMapNode map)
        {
            if (map != null)
                map.Clickable = true;

            using (var db = new ApplicationDbContext())
            {
                var nodes = new List<DynamicNode>();
                foreach (var cat in db.CategoryEntities.Where(p => p.ParentCategoryId ==null || p.ParentCategoryId == 0).ToArray())
                {
                    var node = CreateCategory(null, cat);
                    foreach (var child in cat.ChildCategoryEntities)
                    {
                        var node2 = CreateCategory(node, child);
                        foreach (var child2 in child.ChildCategoryEntities)
                        {
                            var node3 = CreateCategory(node2, child2);
                            nodes.Add(node3);
                        }
                        nodes.Add(node2);
                    }

                    nodes.Add(node);
                    //=====================================================
                }
                nodes.AddRange(db.ProductEntities.ToArray()
                .Select(p => new DynamicNode
                {
                    Clickable = true,
                    ParentKey = $"Category_{p.CategoryId}",
                    Key = $"Product_{p.EntityId}",
                    Action = "Product",
                    Controller = "Catalog",
                    Title = p.Name,
                    RouteValues = new Dictionary<string, object>()
                    {
                        {"id", p.EntityId}
                    }
                }));
                return nodes;
            }
        }
    }
}