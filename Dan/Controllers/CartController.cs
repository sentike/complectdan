﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Dan.Areas.Admin.Infrastuctures;
using Dan.Infrastructures.Controllers;
using Dan.Infrastructures.Repository;
using Dan.Infrastructures.Session;
using Dan.Models;
using Dan.Models.Common;
using Dan.Models.Order;
using Dan.Models.Tradecart;
using Dan.View.Cart;

namespace Dan.Controllers
{
    public class CartController : DefaultController
    {
        public static readonly NumberFormatInfo CurrencyFormat =  new NumberFormatInfo
        {
            NumberGroupSeparator = " "
        };

        [HttpGet]
        public ActionResult Index()
        {
            //-------------------------------------------------------------------
            var products = TradecartRepository.TakeEntities(SessionId, UserId).ToList().AsView();

            //-------------------------------------------------------------------
            var amount = TradecartRepository.CountEntities(SessionId, UserId, false);
            var cost = TradecartRepository.Sum(SessionId, UserId);

            Session["CartAmount"] = amount;
            Session["CartCost"] = cost;



            //-------------------------------------------------------------------
            var view = new TradecartOrderView
            {
                Products = products,
                Price = products.Sum(p => p.Amount * p.Cost),
                Weight = products.Sum(p => p.Amount * p.Product.Dimensions.Weight),
            };

            //-------------------------------------------------------------------
            return View(view);
        }

        [HttpGet]
        public ActionResult ConfirmOrder()
        {
            //-------------------------------------------------------------------
            var products = TradecartRepository.TakeEntities(SessionId, UserId).ToList().AsView();

            //-------------------------------------------------------------------
            var view = new TradecartConfirmOrder
            {
                Products = products,
                Price = products.Sum(p => p.Amount * p.Cost),
                Weight = products.Sum(p => p.Amount * p.Product.Dimensions.Weight),
                Address = UserManager.Users.SingleOrDefault(p => p.Id == UserId)?.Address?.AsShortView(),
            };


            //-------------------------------------------------------------------
            return View(view);
        }

        [HttpPost]
        public ActionResult ConfirmOrder(TradecartConfirmOrder view)
        {
            //-------------------------------------------------------------------
            var products = TradecartRepository.TakeEntities(SessionId, UserId).ToList();

            //-------------------------------------------------------------------
            if (view.TermsofService == false)
            {

                ModelState.AddModelError(nameof(view.TermsofService), "Вы не дали согласие на обработку персональных данных");
                view.Products = products.AsView();
                view.Price = view.Products.Sum(p => p.Amount * p.Cost);
                view.Weight = view.Products.Sum(p => p.Amount * p.Product.Dimensions.Weight);
                return View(view);
            }

            //-------------------------------------------------------------------
            var email = view.Address.Email.ToLower().Trim();
            var phone = view.Address.PhoneNumber.Trim();

            //-------------------------------------------------------------------
            var password = Membership.GeneratePassword(8, 1);
            var user = Context.Users.FirstOrDefault(p => p.Email == email || p.PhoneNumber == phone);
            try
            {
                if (user == null)
                {
                    Task.Run(async () =>
                        {
                            await CreateUserAccount(new RegisterViewModel
                            {
                                Email = email,
                                Password = password,
                                ConfirmPassword = password
                            }, phone);
                        })
                        .Wait();
                    user = Context.Users.FirstOrDefault(p => p.Email == email);
                    user.Address = new CustomerAddressEntity
                    {
                        Address = view.Address.Address,
                        City = view.Address.City,
                        CreatedDate = DateTime.UtcNow,
                        Email = view.Address.Email,
                        FirstName = view.Address.FirstName,
                        LastName = view.Address.LastName,
                        PhoneNumber = view.Address.PhoneNumber,
                        MiddleName = view.Address.MiddleName,
                        StateProvince = view.Address.StateProvince,
                        ZipPostalCode = view.Address.ZipPostalCode
                    };
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            //----------------------------------------------------------------------------------
            CustomerKey.UserId = user?.Id;

            //----------------------------------------------------------------------------------
            var address = UserManager.Users.Single(p => p.Id == UserId).Address;

            //----------------------------------------------------------------------------------
            var order = OrderRepository.CreateOrder(products, CustomerKey, address,
                view.Delivery, Request.UserAgent, GetIpAddress(HttpContext));
            OrderRepository.SaveChanges();

            //-------------------------------------------------------------------
            OrderRepository.SendCreateMessage(order);

            //----------------------------------------------------------------------------------
            TradecartRepository.Model.RemoveRange(products);
            TradecartRepository.SaveChanges();

            //----------------------------------------------------------------------------------
            Session["CartAmount"] = 0;
            Session["CartAmount"] = 0;
            Session["CartWish"] = OrderRepository.TakeCount(CustomerKey);

            //----------------------------------------------------------------------------------
            return RedirectToAction("Details", "Order", new {id = order.EntityId});
        }

        [HttpPost]
        public ActionResult Create(long id, short count = 1)
        {
            CreateAjax(id, count);
            return RedirectToAction("Product", "Catalog", new {id});
        }

        [HttpPost]
        public ActionResult CreateAjax(long id, short count = 1)
        {
            //====================================================================
            var entity = TradecartRepository.Context.ProductEntities.SingleOrDefault(p => p.EntityId == id && p.Deleted == false);
            if (entity == null)
            {
                return HttpNotFound();
            }

            //-------------------------------------------------------------------
            TradecartRepository.AddEntity(entity, SessionId, UserId, count);

            //-------------------------------------------------------------------
            var amount = TradecartRepository.CountEntities(SessionId, UserId, false);
            var cost = TradecartRepository.Sum(SessionId, UserId);

            Session["CartAmount"] = amount;
            Session["CartCost"] = cost;

            //-------------------------------------------------------------------
            return Json(new
            {
                cart = new
                {
                    amount,
                    cost
                },
                product = new
                {
                    id = entity.EntityId,
                    name = entity.Name
                }
            });
            
        }
    }
}
