﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;
using Dan.Models.Image;
using Dan.Models.Interface;

namespace Dan.Models.Blog
{
    public class BlogArticleEntity
        : IAbstractEntityInterface
        , IEntityWithImageInterface
    {
        public long EntityId { get; set; }
        public string Name { get; set; }
        public string ShortDescription { get; set; }
        public string FullDescription { get; set; }

        public long CategoryId { get; set; }
        public virtual BlogCategoryEntity CategoryEntity { get; set; }

        public SeoEntityType Seo { get; set; }

        public long ImageId { get; set; }
        public virtual ImageEntity ImagEntity { get; set; }

        public virtual List<BlogVisitEntity> VisitEntities { get; set; } = new List<BlogVisitEntity>();
        public class Configuration : EntityTypeConfiguration<BlogArticleEntity>
        {
            public Configuration()
            {
                HasKey(i => i.EntityId);
                ToTable("blog.BlogArticleEntity");

                HasMany(p => p.VisitEntities).WithRequired(p => p.ArticleEntity).HasForeignKey(p => p.ArticleId).WillCascadeOnDelete(true);
                //=================================================================
                Property(p => p.Name).IsRequired().HasMaxLength(128);
                Property(p => p.ShortDescription).IsRequired().HasMaxLength(4096);
                Property(p => p.FullDescription).IsRequired().HasMaxLength(4096 * 4);

            }
        }
    }
}