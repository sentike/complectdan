﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Dan.Startup))]
namespace Dan
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
