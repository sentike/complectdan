﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using Dan.Models.Catalog;
using Dan.View.Image;

namespace Dan.View.Product
{
    public class TradeCartProductView : CatalogProductLargeView
    {
    }

    public static class TradeCartProductViewHelper
    {
        public static TradeCartProductView AsTradeCartView(this ProductEntity entity)
        {
            Mapper.Initialize
            (
                c => c.CreateMap<ProductEntity, TradeCartProductView>()
                .ForMember(m => m.Description, m => m.MapFrom(r => r.ShortDescription))
                .ForMember(m => m.Image, m => m.MapFrom(r => r.ImagEntity.AsShortView()))
                .ForMember(m => m.Params, m => m.MapFrom(r => r.ParamEntities.AsView()))
            );
            return Mapper.Map<TradeCartProductView>(entity);
        }

    }
}