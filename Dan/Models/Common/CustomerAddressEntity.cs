﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using Dan.View.Cart;

namespace Dan.Models.Common
{
    [ComplexType]
    public class CustomerAddressEntity
    {
        public static CustomerAddressEntity Factory(OrderCustomerAddressView view)
        {
            return new CustomerAddressEntity
            {
                FirstName = view.FirstName,
                ZipPostalCode = view.ZipPostalCode,
                StateProvince = view.StateProvince,
                City = view.City,
                Address = view.Address,
                PhoneNumber = view.PhoneNumber,
                Email = view.Email
            };
        }

        [Display(Name = "Имя")]
        public string FirstName { get; set; }

        [Display(Name = "Фамилия")]
        public string LastName { get; set; }

        [Display(Name = "Отчество")]
        public string MiddleName { get; set; }

        [Display(Name = "Email")]
        public string Email { get; set; }

        [Display(Name = "Почтовый индекс")]
        public string ZipPostalCode { get; set; }

        [Display(Name = "Область")]
        public string StateProvince { get; set; }

        [Display(Name = "Город")]
        public string City { get; set; }

        [Display(Name = "Адресс")]
        public string Address { get; set; }
        
        [Display(Name = "Номер телефона")]
        public string PhoneNumber { get; set; }

        public DateTime CreatedDate { get; set; }

        public string UserName => $"{FirstName} {LastName} {MiddleName}";

        public string UserNameAbbreb => $"{LastName} {FirstName?.FirstOrDefault()} {MiddleName?.FirstOrDefault()}";
        public string SummaryAddress => $"{ZipPostalCode}, {StateProvince}, {City}, {Address}";
    }
}