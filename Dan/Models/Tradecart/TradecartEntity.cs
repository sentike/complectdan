﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;
using Dan.Models.Catalog;
using Dan.Models.Interface;
using Dan.Models.User;

namespace Dan.Models.Tradecart
{
    public class TradecartEntity : IWishItemInterface
    {
        public TradecartEntity() { }

        public TradecartEntity(ProductEntity product, Guid session, Guid? user, short amount = 1)
        {
            CreatedDate = DateTime.Now;
            EntityId = Guid.NewGuid();
            SessionId = session;
            UserId = user;
            ProductId = product.EntityId;
            Price = product.Price.Price;
            OriginalProductCost = product.Price.Price;
            Amount = amount;
        }

        public Guid EntityId { get; set; }
        public long ProductId { get; set; }
        public virtual ProductEntity ProductEntity { get; set; }

        public short Amount { get; set; }
        public decimal Price { get; set; }
        public decimal OriginalProductCost { get; set; }

        //============================================
        public Guid SessionId { get; set; }
        public Guid? UserId { get; set; }
        public virtual ApplicationUser UserEntity { get; set; }

        //============================================
        public DateTime CreatedDate { get; set; }
        public DateTime? DeleteDate { get; set; }

        //============================================

        public class Configuration : EntityTypeConfiguration<TradecartEntity>
        {
            public Configuration()
            {
                HasKey(i => i.EntityId);
                ToTable("wishlist.TradecartEntity");
                HasRequired(p => p.ProductEntity).WithMany().HasForeignKey(p => p.ProductId).WillCascadeOnDelete(true);
                HasOptional(p => p.UserEntity).WithMany(p => p.TradecartEntities).HasForeignKey(p => p.UserId).WillCascadeOnDelete(true);
            }
        }

    }
}