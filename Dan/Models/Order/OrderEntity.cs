﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;
using Dan.Models.Common;
using Dan.Models.User;
using Dan.View.Cart;

namespace Dan.Models.Order
{
    public enum CustomerOrderStatus : byte
    {
        //  Заказ оформлен и ожидает подтверждения
        [Display(Name = "Ожидает подтверждения")]
        Processing,

        [Display(Name = "Ожидает оплаты")]
        Payment,

        //  Заказ подтвержден и ожидает отправки
        [Display(Name = "Ожидает отправки")]
        Preparing,

        //  Заказ отправлен и ожидает доставки
        [Display(Name = "Ожидает доставки")]
        Delivery,

        //  Заказ доставлен
        [Display(Name = "Доставлен")]
        Delivered,

        //  Заказ отменен
        [Display(Name = "Отменен")]
        Canceled,
    }

    public static partial class EnumHelper
    {
        public static string Description(this CustomerOrderStatus status)
        {
            switch (status)
            {
                case CustomerOrderStatus.Processing:
                    return "Ожидает подтверждения";
                case CustomerOrderStatus.Preparing:
                    return "Ожидает отправки";
                case CustomerOrderStatus.Delivery:
                    return "Ожидает доставки";
                case CustomerOrderStatus.Delivered:
                    return "Доставлен";
                case CustomerOrderStatus.Canceled:
                    return "Отменен";
                case CustomerOrderStatus.Payment:
                    return "Ожидает оплаты";
                default:
                    throw new ArgumentOutOfRangeException(nameof(status), status, null);
            }
        }

        public static string Description(this DeliveryMethod method)
        {
            switch (method)
            {
                case DeliveryMethod.Shipping:
                    return "Доставка";
                case DeliveryMethod.Pickup:
                    return "Самовывоз";
                default:
                    throw new ArgumentOutOfRangeException(nameof(method), method, null);
            }
        }
    }

    [Flags]
    public enum ContactConfirmingStatus : byte
    {
        None,
        EmailConfirmed = 1,
        PhoneNumberConfirmed = 2,
    }


    public class OrderEntity
    {
        public Guid EntityId { get; set; }

        //============================================
        public Guid SessionId { get; set; }
        public Guid? UserId { get; set; }
        public virtual ApplicationUser UserEntity { get; set; }

        //============================================
        public short OrderConfirmAttep { get; set; } = 1;
        public CustomerOrderStatus OrderStatus { get; set; }
        public ContactConfirmingStatus ContactConfirmingStatus { get; set; }

        //============================================
        public virtual CustomerAddressEntity CustomerAddressEntity { get; set; }

        //============================================
        public DateTime CreatedDate { get; set; }
        public DateTime? DeleteDate { get; set; }
        public DateTime? ConfirmEmailDate { get; set; }
        public DateTime? ConfirmPhoneDate { get; set; }
        public DateTime? ConfirmOrderDate { get; set; }
        public DateTime? DeliveryConfirmDate { get; set; }
        public DateTime? DeliverySendDate { get; set; }

        //============================================
        public DeliveryMethod ShippingMethod { get; set; }
        public string DeliveryToken { get; set; }
        public string IpAddress { get; set; }
        public string UserAgent { get; set; }

        //============================================
        public string AdminComment { get; set; }
        public string Comment { get; set; }
        
        //============================================
        public virtual List<OrderItemEntity> ItemEntities { get; set; } = new List<OrderItemEntity>();

        public decimal Cost => ItemEntities.Sum(p => p.Price * p.Amount);
       // public decimal Weight => ItemEntities.Sum(p => p.ProductEntity.Dimensions.Weight);

        public class Configuration : EntityTypeConfiguration<OrderEntity>
        {
            public Configuration()
            {
                HasKey(i => i.EntityId);
                ToTable("wishlist.OrderEntity");

                Property(p => p.Comment).IsOptional().HasMaxLength(1024);
                Property(p => p.AdminComment).IsOptional().HasMaxLength(196);
                Property(p => p.UserAgent).IsOptional().HasMaxLength(196);
                Property(p => p.IpAddress).IsOptional().HasMaxLength(16);
                Property(p => p.DeliveryToken).IsOptional().HasMaxLength(32);

                HasMany(p => p.ItemEntities).WithRequired(p => p.OrderEntity).HasForeignKey(p => p.OrderId).WillCascadeOnDelete(true);
            }
        }
    }
}