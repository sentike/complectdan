﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Dan.View.Filter
{
    public class FilterParamValueView
    {
        public long EntityId { get; set; }
        public string Name { get; set; }
    }

    public class FilterParamView
    {
        public long EntityId { get; set; }
        public string Name { get; set; }
        public List<FilterParamValueView> Values { get; set; } = new List<FilterParamValueView>();
    }
}