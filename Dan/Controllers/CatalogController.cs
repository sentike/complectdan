﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using Dan.Infrastructures.Controllers;
using Dan.Infrastructures.Repository;
using Dan.Models;
using Dan.Models.Catalog;
using Dan.Models.Image;
using Dan.View.Category;
using Dan.View.Filter;
using Dan.View.Product;
using Dan.View.Shared.Navigation;

namespace Dan.Controllers
{
    public class CatalogController : DefaultController
    {
        [HttpGet]
        public ActionResult Index()
        {
            using (var r = new CategoryRepository())
            {
                return View(r.TakeEntities(null, true).ToList().AsShortView());
            }
        }

        [HttpGet]
        public ActionResult Product(long? id)
        {
            using (var r1 = new ProductRepository())
            {
                //==============================================
                if (id == null)
                {
                    HttpContext.Response.StatusCode = 404;
                    return RedirectToAction("Index");
                }

                //==============================================
                var product = r1.TakeEntity(id.Value);
                if (product == null || product.Deleted)
                {
                    HttpContext.Response.StatusCode = 404;
                    return RedirectToAction("Index");
                }

                //==============================================
                return View(product.AsLargeView());
            }
        }


        public class FilterKeyValue
        {
            public long Key { get; set; }
            public ICollection<long> Value { get; set; }
        }

        [HttpGet]
        public ActionResult Category(long? id
            , string query = null
            , decimal? weightMin = null
            , decimal? weightMax = null
            , decimal? costMin = null
            , decimal? costMax = null
            , ProductOrderMethod? order = null
            , ICollection<long> filter = null
            , bool seed = false
            , int items = 0
            , int page = 1)
        {
            //if (seed) CategorySeed2(id.Value);
            using (var r1 = new ProductRepository())
            {
                using (var r2 = new CategoryRepository(r1.Context))
                {
                    //==============================================
                    if (id == null)
                    {
                        HttpContext.Response.StatusCode = 404;
                        return RedirectToAction("Index");
                    }

                    //==============================================
                    var category = r2.TakeEntity(id.Value);
                    if (category == null || category.Deleted)
                    {
                        HttpContext.Response.StatusCode = 404;
                        return RedirectToAction("Index");
                    }

                    //==============================================
                    ViewBag.Title = category.Name;

                    //==============================================
                    var products = r1.TakeEntities(id, order, true);
                    if (string.IsNullOrWhiteSpace(query) == false && query.Length >= 4)
                    {
                        products = products.Where(p => p.Name.Contains(query) || p.FullDescription.Contains(query));
                    }

                    if (costMin != null) products = products.Where(p => p.Price.Price >= costMin);
                    if (costMax != null) products = products.Where(p => p.Price.Price <= costMax);


                    //==============================================
                    var navigation = new PageNavigationView<ProductEntity, CatalogProductShortView>(products, x => x.AsShortView(), page, items);
                    
                    //==============================================
                    var view = category.AsLargeView(category, false, navigation);

                    //==============================================
                    view.IsSearchView |= string.IsNullOrWhiteSpace(query) == false && query.Length >= 4;
                    view.IsSearchView |= weightMin != null;
                    view.IsSearchView |= weightMax != null;
                    view.IsSearchView |= costMin != null;
                    view.IsSearchView |= costMax != null;
                    view.IsSearchView |= filter != null;

                    //==============================================
                    //if (filter != null)
                    //{
                    //    products = products.Where(p => p.ParamEntities.Any(pp => filter.Contains(pp.EntityId)));
                    //}

                    //==============================================
                    var @params = products.SelectMany(p => p.ParamEntities).GroupBy(p => p.ParamEnity, p => p).ToArray();
                    view.Filters = @params.Select(p => new FilterParamView
                    {
                        EntityId = p.Key.EntityId,
                        Name = p.Key.Name,
                        Values = p.Select(v => new FilterParamValueView
                        {
                            Name = v.Value,
                            EntityId = v.EntityId
                        }).ToList()
                    }).ToArray().Take(10).ToArray();

                    //==============================================
                    if (products.Any())
                    {
                        view.WeightSeed = new CatalogSliderFilter(products.Min(p => p.Dimensions.Weight), products.Max(p => p.Dimensions.Weight));
                        view.WeightFilter = new CatalogSliderFilter(weightMin, weightMax);

                        view.CostSeed = new CatalogSliderFilter(products.Min(p => p.Price.Price), products.Max(p => p.Price.Price));
                        view.CostFilter = new CatalogSliderFilter(costMin, costMax);
                    }
                    
                    //==============================================
                    return View("CategoryIndex", view);
                }
            }
        }



    }
}