﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;
using Dan.Models.Interface;

namespace Dan.Models.Blog
{
    public class BlogCategoryEntity
        : IAbstractEntityInterface
    {
        public long EntityId { get; set; }
        public string Name { get; set; }
        public string ShortDescription { get; set; }
        public string FullDescription { get; set; }

        public SeoEntityType Seo { get; set; }
        public virtual List<BlogArticleEntity> ArticleEntities { get; set; } = new List<BlogArticleEntity>();

        public class Configuration : EntityTypeConfiguration<BlogCategoryEntity>
        {
            public Configuration()
            {
                HasKey(i => i.EntityId);
                ToTable("blog.BlogCategoryEntity");

                HasMany(p => p.ArticleEntities).WithRequired(p => p.CategoryEntity).HasForeignKey(p => p.CategoryId).WillCascadeOnDelete(true);

                //=================================================================
                Property(p => p.Name).IsRequired().HasMaxLength(128);
                Property(p => p.ShortDescription).IsRequired().HasMaxLength(4096);
                Property(p => p.FullDescription).IsRequired().HasMaxLength(4096 * 4);
            }
        }

    }
}