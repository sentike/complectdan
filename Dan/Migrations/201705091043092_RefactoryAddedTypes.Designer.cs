// <auto-generated />
namespace Dan.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class RefactoryAddedTypes : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(RefactoryAddedTypes));
        
        string IMigrationMetadata.Id
        {
            get { return "201705091043092_RefactoryAddedTypes"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
