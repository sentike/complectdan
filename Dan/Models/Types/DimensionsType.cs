﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Dan.Models.Types
{
    [ComplexType]
    public class DimensionsType
    {
        /// <summary>
        /// Вес, кг
        /// </summary>
        [Display(Name = "Вес, кг")]
        public decimal Weight { get; set; }

        /// <summary>
        /// Длина, мм
        /// </summary>
        [Display(Name = "Длина, мм")]
        public decimal Length { get; set; }

        /// <summary>
        /// Ширина, мм
        /// </summary>
        [Display(Name = "Ширина, мм")]
        public decimal Width { get; set; }

        /// <summary>
        /// Высота, мм
        /// </summary>
        [Display(Name = "Высота, мм")]
        public decimal Height { get; set; }

        [Display(Name = "Габариты, см")]
        public string Dimensions => $"{Length:F1} x {Width:F1} x {Height:F1}";
    }
}