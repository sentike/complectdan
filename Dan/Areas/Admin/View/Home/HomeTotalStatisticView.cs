﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Dan.Areas.Admin.View.Home
{
    public class HomeTotalStatisticView
    {
        //=======================================
        public long TotalUsers { get; set; }
        public long MaleUsers { get; set; }
        public long FemaleUsers { get; set; }

        //=======================================
        public long TotalOrders { get; set; }
        public long OredersInCurrentMonth { get; set; }
        public long OredersInLastMonth { get; set; }

        //=======================================
        public List<DateGraphicView> OrdersGraphicView { get; set; } = new List<DateGraphicView>(64);
        public List<DateGraphicView> UsersGraphicView { get; set; } = new List<DateGraphicView>(64);

        //=======================================
        public List<HomeOrderPreview> LatestOrders { get; set; } = new List<HomeOrderPreview>(16);
        public List<HomeOrderPreview> ProcessOrders { get; set; } = new List<HomeOrderPreview>(16);
    
        //=======================================
        public List<HomeSellHit> SellCostHits { get; set; } = new List<HomeSellHit>(16);
        public List<HomeSellHit> SellAmountHits { get; set; } = new List<HomeSellHit>(16);
    }
}