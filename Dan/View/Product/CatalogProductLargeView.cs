﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using Dan.Models.Catalog;
using Dan.Models.Interface;
using Dan.View.Image;
using Dan.Models.Types;

namespace Dan.View.Product
{
    public class CatalogProductLargeView 
        : CatalogProductShortView
    {
        [AllowHtml]
        [StringLength(4096)]
        [Display(Name = "Описание")]
        public string Description { get; set; }
        public List<CatalogProductParamView> Params { get; set; } = new List<CatalogProductParamView>();


        public ShippingType Shipping { get; set; }
        public DimensionsType Dimensions { get; set; }
        public SeoEntityType Seo { get; set; }
    }

    public static class CatalogProductLargeViewHelper
    {
        public static CatalogProductLargeView AsLargeView(this ProductEntity entity)
        {
            Mapper.Initialize
            (
                c => c.CreateMap<ProductEntity, CatalogProductLargeView>()
                .ForMember(m => m.Description, m => m.MapFrom(r => r.FullDescription))
                .ForMember(m => m.Params, m => m.MapFrom(r => r.ParamEntities.AsView()))
                .ForMember(p => p.Image, p => p.MapFrom(m => m.ImagEntity.AsShortView()))
            );
            return Mapper.Map<CatalogProductLargeView>(entity);
        }

    }
}