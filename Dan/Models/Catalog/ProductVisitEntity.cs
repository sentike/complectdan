﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace Dan.Models.Catalog
{
    public class ProductVisitEntity
    {
        public Guid VisitId { get; set; }

        //=======================================
        public long ProductId { get; set; }
        public virtual ProductEntity ProductEntity { get; set; }

        //=======================================
        public string Referer { get; set; }
        public string IpAddress { get; set; }
        public string UserAgent { get; set; }
        public DateTime VisitDate { get; set; }

        //=======================================
        public override string ToString()
        {
            return $"{VisitId} | {ProductId} - {ProductEntity?.Name} | {VisitDate}";
        }

        //=======================================
        public class Configuration : EntityTypeConfiguration<ProductVisitEntity>
        {
            public Configuration()
            {
                Property(p => p.Referer).IsOptional().HasMaxLength(256);
                Property(p => p.UserAgent).IsOptional().HasMaxLength(128);
                Property(p => p.IpAddress).IsOptional().HasMaxLength(16);
                ToTable("catalog.ProductVisitEntity");
                HasKey(p => p.VisitId);
            }
        }
    }
}