﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Dan.Areas.Admin.Infrastuctures;
using Dan.Areas.Admin.View;
using Dan.Areas.Admin.View.Home;
using Dan.Areas.Admin.View.Product;
using Dan.Infrastructures.Repository;

namespace Dan.Areas.Admin.Controllers
{
    public class ProductController : AbstractController
    {
        // GET: Admin/Product
        public ActionResult Index(long? id, int? page)
        {
            using (var r = new ProductRepository())
            {
                return View(r.TakeEntities(id, ProductOrderMethod.PriceUp, false).ToList().AsShortAdminView());
            }
        }

        // GET: Admin/Product/Details/5
        public ActionResult Details(long? id)
        {
            //===========================================
            if (id == null)
            {
                return RedirectToAction("Index", new {action = 1});
            }

            //===========================================
            using (var r = new ProductRepository())
            {
                var entity = r.TakeEntity(id.Value);
                
                //===========================================
                if (entity == null)
                {
                    return RedirectToAction("Index");
                }

                //===========================================
                var view = entity.AsLargeAdminView();

                //===========================================
                try
                {
                    view.LatestOrders = r.Context.OrderEntities
                        .Where(p => p.ItemEntities.Any(q => q.ProductId == id.Value))
                        .OrderByDescending(p => p.CreatedDate).Take(30)
                        .ToList()
                        .AsShortAdminView();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }

                //===========================================
                return View(view);
            }
        }

        // GET: Admin/Product/Create
        public ActionResult Create(long? id)
        {
            using (var r1 = new CategoryRepository())
            {
                using (var r2 = new ImageRepository(r1.Context))
                {
                    using (var r3 = new ImageCategoryRepository(r1.Context))
                    {
                        using (var r4 = new ManufactureRepository(r1.Context))
                        {
                            var view = new AdminProductCreateView
                            {
                                ImageSelectList = r2.TakeEntityList().WithDefaultElem("Выберите изображение или загрузите новое"),
                                ImageCategorySelectList = r3.TakeEntityList(null),
                                CategorySelectList = r1.TakeEntityList(id),
                                ManafacturerSelectList = r4.TakeEntityList(null),
                            };
                            return View(view);
                        }
                    }
                }
            }
        }

        // POST: Admin/Product/Create
        [HttpPost]
        public ActionResult Create(AdminProductCreateView view)
        {
            using (var r1 = new CategoryRepository())
            {
                using (var r2 = new ImageRepository(r1.Context))
                {
                    using (var r3 = new ImageCategoryRepository(r1.Context))
                    {
                        using (var r4 = new ManufactureRepository(r1.Context))
                        {
                            if (ModelState.IsValid == false)
                            {
                                view.ImageSelectList = r2.TakeEntityList().WithDefaultElem("Выберите изображение или загрузите новое");
                                view.ImageCategorySelectList = r3.TakeEntityList(view.Image.CategoryId);
                                view.CategorySelectList = r1.TakeEntityList(view.CategoryId);
                                view.ManafacturerSelectList = r4.TakeEntityList(view.ManufactureId);
                                return View(view);
                            }

                            if (view.CategoryId <= 0 || r1.IsExistEntity(view.CategoryId) == false)
                            {
                                ModelState.AddModelError(nameof(view.CategoryId), "Категория не найдена");
                                view.ImageSelectList = r2.TakeEntityList().WithDefaultElem("Выберите изображение или загрузите новое");
                                view.ImageCategorySelectList = r3.TakeEntityList(view.Image.CategoryId);
                                view.CategorySelectList = r1.TakeEntityList(view.CategoryId);
                                view.ManafacturerSelectList = r4.TakeEntityList(view.ManufactureId);
                                return View(view);
                            }


                            if (view.ManufactureId <= 0 || r4.IsExistEntity(view.ManufactureId) == false)
                            {
                                ModelState.AddModelError(nameof(view.ManufactureId), "Производитель не найден");
                                view.ImageSelectList = r2.TakeEntityList().WithDefaultElem("Выберите изображение или загрузите новое");
                                view.ImageCategorySelectList = r3.TakeEntityList(view.Image.CategoryId);
                                view.CategorySelectList = r1.TakeEntityList(view.CategoryId);
                                view.ManafacturerSelectList = r4.TakeEntityList(view.ManufactureId);
                                return View(view);
                            }

                            if (view.ImageId < 0 || (view.ImageId > 0 && r2.IsExistEntity(view.ImageId) == false))
                            {
                                ModelState.AddModelError(nameof(view.ImageId), "Изображение не найдено");
                                view.ImageSelectList = r2.TakeEntityList().WithDefaultElem("Выберите изображение или загрузите новое");
                                view.ImageCategorySelectList = r3.TakeEntityList(view.Image.CategoryId);
                                view.CategorySelectList = r1.TakeEntityList(view.CategoryId);
                                view.ManafacturerSelectList = r4.TakeEntityList(view.ManufactureId);
                                return View(view);
                            }

                            try
                            {
                                using (var r5 = new ProductRepository(r1.Context))
                                {
                                    if (view.ImageId == 0)
                                    {
                                        var status = UploadImage(r5.Context, view.Image);
                                        if (status == UploadImageResponse.Successfully)
                                        {
                                            view.ImageId = view.Image.EntityId;
                                        }
                                        else
                                        {
                                            view.ImageSelectList = r2.TakeEntityList().WithDefaultElem("Выберите изображение или загрузите новое");
                                            view.ImageCategorySelectList = r3.TakeEntityList(view.Image.CategoryId);
                                            view.CategorySelectList = r1.TakeEntityList(view.CategoryId);
                                            view.ManafacturerSelectList = r3.TakeEntityList(view.ManufactureId);
                                            return View(view);
                                        }
                                    }
                                    r5.Context.ProductEntities.Add(view.AsEntity());
                                    r5.Context.SaveChanges();
                                }
                            }
                            catch (Exception e)
                            {
                                ModelState.AddModelError(string.Empty, $"Exception: {e}");
                                view.ImageSelectList = r2.TakeEntityList().WithDefaultElem("Выберите изображение или загрузите новое");
                                view.ImageCategorySelectList = r3.TakeEntityList(view.Image.CategoryId);
                                view.CategorySelectList = r1.TakeEntityList(view.CategoryId);
                                view.ManafacturerSelectList = r3.TakeEntityList(view.ManufactureId);
                                return View(view);
                            }
                        }
                    }
                }
            }

            return RedirectToAction("Index");
        }

        // GET: Admin/Product/Edit/5
        public ActionResult Edit(long? id)
        {
            using (var r1 = new CategoryRepository())
            {
                using (var r2 = new ImageRepository(r1.Context))
                {
                    using (var r3 = new ImageCategoryRepository(r1.Context))
                    {
                        using (var r4 = new ManufactureRepository(r1.Context))
                        {
                            using (var r5 = new ProductRepository(r1.Context))
                            {
                                //==============================================
                                var entity = r5.TakeEntity(id.Value);
                                if (entity == null)
                                {
                                    
                                }

                                //==============================================
                                AdminProductEditView view = entity.AsEditView();
                                view.ImageSelectList = r2.TakeEntityList();
                                view.ImageCategorySelectList = r3.TakeEntityList(null);
                                view.CategorySelectList = r1.TakeEntityList(id);
                                view.ManafacturerSelectList = r4.TakeEntityList(null);

                                //==============================================
                                return View(view);
                            }
                        }
                    }
                }
            }
        }

        // POST: Admin/Product/Edit/5
        [HttpPost]
        public ActionResult Edit(long id, AdminProductEditView view)
        {
            using (var r1 = new CategoryRepository())
            {
                using (var r2 = new ImageRepository(r1.Context))
                {
                    using (var r3 = new ImageCategoryRepository(r1.Context))
                    {
                        using (var r4 = new ManufactureRepository(r1.Context))
                        {
                            using (var r5 = new ProductRepository(r1.Context))
                            {
                                var entity = r5.TakeEntity(id);

                                //=====================================================
                                entity.Name = view.Name;
                                entity.FullDescription = view.FullDescription;
                                entity.ShortDescription = view.ShortDescription;
                                entity.Shipping = view.Shipping;
                                entity.Price = view.Price;
                                entity.Dimensions = view.Dimensions;
                                entity.CategoryId = view.CategoryId;
                                entity.ImageId = view.ImageId;
                                entity.ManufactureId = view.ManufactureId;
                                entity.AdminComment = view.AdminComment;
                                entity.Published = view.Published;
                                entity.Deleted = view.Deleted;
                                entity.Marketing = view.Marketing;
                                entity.Sku = view.Sku;
                                entity.Seo = view.Seo;
                                
                                //=====================================================
                                r5.SaveChanges();

                                //=====================================================
                                return RedirectToAction("Details", new {id});
                            }
                        }
                    }
                }
            }
        }

        // GET: Admin/Product/Delete/5
        public ActionResult Delete(long? id)
        {
            ViewBag.Title = "Удаление товара";
            using (var r = new ProductRepository())
            {
                var entity = r.TakeEntity(id.Value);
                var view = new AdminUniversalDeleteView
                {
                    Name = $"{entity.Name}, {entity.Price.Price} руб",
                    Description = entity.FullDescription,
                    EntityId = entity.EntityId,
                    PublicKey = Guid.NewGuid(),
                };
                return View(view);
            }
        }

        // POST: Admin/Product/Delete/5
        public override DeleteProcessResponse OnDeleteProcess(AdminUniversalDeleteView view)
        {
            using (var r = new ProductRepository())
            {
                var entity = r.TakeEntity(view.EntityId);
                if (entity == null)
                {
                    return DeleteProcessResponse.NotFound;
                }
                else if (entity.Deleted)
                {
                    return DeleteProcessResponse.AlreadyDeleted;
                }
                entity.Deleted = true;
                r.SaveChanges();
            }
            return DeleteProcessResponse.Successfully;
        }
    }
}
