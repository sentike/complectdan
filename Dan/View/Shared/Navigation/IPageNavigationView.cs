namespace Dan.View.Shared.Navigation
{
    public interface IPageNavigationView
    {
        int PageBegin { get; }
        int PageEnd { get; }
        int PageNo { get; }
        int CountPage { get; }
        int ItemPerPage { get; }
        string ControllerName { get; set; }
        string ControllerAction { get; set; }
    }
}