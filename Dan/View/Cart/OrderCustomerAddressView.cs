﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using Dan.Models;
using Dan.Models.Common;

namespace Dan.View.Cart
{
    public class OrderCustomerAddressView
    {
        public OrderCustomerAddressView()
        {
            
        }

        public OrderCustomerAddressView(RegisterViewModel model)
        {
            FirstName = model.FirstName;
            LastName = model.LastName;
            MiddleName = model.MiddleName;

            Email = model.Email;
            City = model.City;
            ZipPostalCode = model.ZipPostalCode;
            StateProvince = model.StateProvince;
            Address = model.Address;

        }

        public Guid EntityId { get; set; }

        /// <summary>
        /// Gets or sets the first name
        /// </summary>
        [Display(Name = "Имя")]
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets the last name
        /// </summary>
        [Display(Name = "Фамилия")]
        public string LastName { get; set; }

        /// <summary>
        /// Gets or sets the last name
        /// </summary>
        [Display(Name = "Отчество")]
        public string MiddleName { get; set; }

        /// <summary>
        /// Gets or sets the email
        /// </summary>
        [Display(Name = "Адрес эл. почты")]
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets the company
        /// </summary>
        [Display(Name = "Название компании")]
        public string Company { get; set; }

        /// <summary>
        /// Gets or sets the country identifier
        /// </summary>
        public int? CountryId { get; set; }

        /// <summary>
        /// Gets or sets the state/province identifier
        /// </summary>
        public int? StateProvinceId { get; set; }

        /// <summary>
        /// Gets or sets the city
        /// </summary>
        [Display(Name = "Город")]
        public string City { get; set; }

        /// <summary>
        /// Gets or sets the address 1
        /// </summary>
        [Display(Name = "Адрес")]
        public string Address { get; set; }

        /// <summary>
        /// Gets or sets the zip/postal code
        /// </summary>
        [Display(Name = "Индекс")]
        public string ZipPostalCode { get; set; }

        /// <summary>
        /// Gets or sets the phone number
        /// </summary>
        [Display(Name = "Номер телефона")]
        public string PhoneNumber { get; set; }

        /// <summary>
        /// Gets or sets the fax number
        /// </summary>
        [Display(Name = "Номер факса")]
        public string FaxNumber { get; set; }

        /// <summary>
        /// Gets or sets the country
        /// </summary>
        [Display(Name = "Страна")]
        public string Country { get; set; }

        /// <summary>
        /// Gets or sets the state/province
        /// </summary>
        [Display(Name = "Область")]
        public string StateProvince { get; set; }

        [Display(Name = "Ф.И.О")]
        public string UserName => $"{LastName} {FirstName} {MiddleName}";

        [Display(Name = "Адрес доставки")]
        public string SummaryAddress => $"{ZipPostalCode}, {StateProvince}, {City}, {Address}";

        public string GetSummaryInformation()
        {
            var sb = new StringBuilder(256);
            sb.AppendLine($"<br>Получатель: {FirstName} {LastName}");
            sb.AppendLine($"<br>Адресс: {SummaryAddress}");
            return sb.ToString();
        }
    }


    public static partial class ViewHelper
    {
        public static List<OrderCustomerAddressView> AsShortView(this List<CustomerAddressEntity> entity)
        {
            return entity.Select(e => e.AsShortView()).ToList();
        }

        public static OrderCustomerAddressView AsShortView(this CustomerAddressEntity entity)
        {
            Mapper.Initialize
            (
                c => c.CreateMap<CustomerAddressEntity, OrderCustomerAddressView>()
            );
            return Mapper.Map<OrderCustomerAddressView>(entity);
        }

    }
}