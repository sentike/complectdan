﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using AutoMapper;
using Dan.Models.Order;
using Dan.View.Cart;

namespace Dan.View.Order
{
    public class CustomerOrderPreview
    {
        [Display(Name = "Заказ")]
        public Guid EntityId { get; set; }

        [Display(Name = "Статус заказа")]
        public CustomerOrderStatus OrderStatus { get; set; }
        public ContactConfirmingStatus ContactConfirmingStatus { get; set; }

        [Display(Name = "Контактная информация")]
        public OrderCustomerAddressView CustomerAddressView { get; set; }

        [Display(Name = "Дата создания")]
        public DateTime CreatedDate { get; set; }

        [Display(Name = "Дата отправки")]
        public DateTime? DeliverySendDate { get; set; }

        [Display(Name = "Способ доставки")]
        public DeliveryMethod ShippingMethod { get; set; }

        [Display(Name = "Номер отправления")]
        public string DeliveryToken { get; set; }

        [Display(Name = "Вес, кг")]
        public double Weight { get; set; }

        [Display(Name = "Сумма, руб")]
        public double Cost { get; set; }
    }

    public static partial class ViewHelper
    {
        public static List<CustomerOrderPreview> AsView(this ICollection<OrderEntity> entity)
        {
            return entity.Select(p => p.AsView()).ToList();
        }

        public static CustomerOrderPreview AsView(this OrderEntity entity)
        {
            Mapper.Initialize
            (
                c => c.CreateMap<OrderEntity, CustomerOrderPreview>()
                    //.ForMember(p => p.Product, m => m.MapFrom(r => r.ProductEntity.AsTradeCartView()))
                    //.ForMember(p => p.Category, m => m.MapFrom(r => r.ProductEntity.CategoryEntity.AsShortView()))
                    .ForMember(p => p.Cost, m => m.MapFrom(r => r.ItemEntities.Sum(p => p.Price * p.Amount)))
                    .ForMember(p => p.Weight, m => m.MapFrom(r => r.ItemEntities.Sum(p => p.ProductEntity.Dimensions.Weight * p.Amount)))
            );
            return Mapper.Map<CustomerOrderPreview>(entity);
        }
    }
}