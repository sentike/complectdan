﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Dan.Areas.Admin.View;
using Dan.Areas.Admin.View.Image;
using Dan.Models;
using Dan.Infrastructures.Repository;
using Dan.Models.Image;

namespace Dan.Areas.Admin.Infrastuctures
{
    [Authorize(Roles = "Admin")]
    public abstract class AbstractController : Controller
    {
        protected ApplicationDbContext Context { get; } = new ApplicationDbContext();
        protected ProductRepository ProductRepository { get; }
        protected TradecartRepository TradecartRepository { get; }
        protected OrderRepository OrderRepository { get; }

        protected AbstractController()
        {
            ProductRepository = new ProductRepository(Context);
            TradecartRepository = new TradecartRepository(Context);
            OrderRepository = new OrderRepository(Context);
        }

        public enum DeleteProcessResponse
        {
            NotFound,
            AlreadyDeleted,
            Successfully,
        }

        [NonAction]
        public abstract DeleteProcessResponse OnDeleteProcess(AdminUniversalDeleteView view);

        [HttpPost]
        public ActionResult Delete(long id, AdminUniversalDeleteView view)
        {
            if (view.PrivateKey == Guid.Empty)
            {
                ModelState.AddModelError(nameof(view.PrivateKey), "Вы не указали код подтверждения!");
            }
            else if (view.PublicKey == Guid.Empty)
            {
                ModelState.AddModelError(nameof(view.PublicKey), "Ошибка при генерации ключа подтверждения!");
            }
            else if (view.PrivateKey != view.PublicKey)
            {
                ModelState.AddModelError(nameof(view.PrivateKey), "Вы указали не верный код подтверждения!");
            }
            else if (view.EntityId != id)
            {
                ModelState.AddModelError(nameof(view.EntityId), "Вы удаляете неизвестный объект!");
            }
            else
            {
                var response = OnDeleteProcess(view);
                if (response == DeleteProcessResponse.NotFound)
                {
                    ModelState.AddModelError(nameof(view.EntityId), "Экземпляр объекта не найден!");
                }
                else if (response == DeleteProcessResponse.AlreadyDeleted)
                {
                    ModelState.AddModelError(nameof(view.EntityId), "Объект уже удален! Вы можете его восстановить перейдя на страницу редактирования");
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            return View(view);
        }


        public enum UploadImageResponse
        {
            InvalidModel,
            InvalidCategory,
            InvalidFile,
            Exception,
            Successfully,
        }

        public UploadImageResponse UploadImage(ApplicationDbContext context, AdminCatalogImageCreateView view)
        {
            using (var r1 = new ImageCategoryRepository(context))
            {
                //===================================================================
                if (ModelState.IsValid == false)
                {
                    view.CategorySelectList = r1.TakeEntityList(view.CategoryId);
                    return UploadImageResponse.InvalidModel;
                }

                //===================================================================
                if (view.CategoryId <= 0)
                {
                    ModelState.AddModelError(nameof(view.CategoryId), "Категория не указана");
                    view.CategorySelectList = r1.TakeEntityList(view.CategoryId);
                    return UploadImageResponse.InvalidCategory;
                }

                if (r1.IsExistEntity(view.CategoryId) == false)
                {
                    ModelState.AddModelError(nameof(view.CategoryId), "Категория не найдена");
                    view.CategorySelectList = r1.TakeEntityList(view.CategoryId);
                    return UploadImageResponse.InvalidCategory;
                }

                if (view.ImageFile.HasFile() == false)
                {
                    ModelState.AddModelError(nameof(view.ImageFile), "Изображение не загружено");
                    view.CategorySelectList = r1.TakeEntityList(view.CategoryId);
                    return UploadImageResponse.InvalidFile;
                }


                //===================================================================
                using (var r2 = new ImageRepository(context))
                {
                    ImageEntity entity = r2.Context.ImageEntities.Add(view.AsEntity());
                    r2.SaveChanges();

                    view.EntityId = entity.EntityId;
                    view.ImageFile.SaveAs(Server.MapPath($"/Content/public/{entity.EntityId}_original.{view.FileType}"));
                    view.ImageFile.SaveImageAs(Server.MapPath($"/Content/public/{entity.EntityId}_large.{view.FileType}"), new FileEditImagePropertyView { SaveImageFormat = view.FileType });
                    view.ImageFile.SaveImageAs(Server.MapPath($"/Content/public/{entity.EntityId}_small.{view.FileType}"), new FileEditImagePropertyView(260, 160) { SaveImageFormat = view.FileType });
                }
                return UploadImageResponse.Successfully;
            }
        }

        public UploadImageResponse UploadImage(AdminCatalogImageCreateView view)
        {
            using (var db = new ApplicationDbContext())
            {
                return UploadImage(db, view);
            }
        }
    }
}