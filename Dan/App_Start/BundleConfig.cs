﻿using System.Web;
using System.Web.Optimization;

namespace Dan
{
    public class BundleConfig
    {
        //Дополнительные сведения об объединении см. по адресу: http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/Admin/DataBase/Scripts").Include(
     "~/Content/Admin/vendors/datatables.net/js/jquery.dataTables.min.js",
     "~/Content/Admin/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js",
     "~/Content/Admin/vendors/datatables.net-buttons/js/dataTables.buttons.min.js",
     "~/Content/Admin/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js",
     "~/Content/Admin/vendors/datatables.net-buttons/js/buttons.flash.min.js",
     "~/Content/Admin/vendors/datatables.net-buttons/js/buttons.html5.min.js",
     "~/Content/Admin/vendors/datatables.net-buttons/js/buttons.print.min.js",
     "~/Content/Admin/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js",
     "~/Content/Admin/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js",
     "~/Content/Admin/vendors/datatables.net-responsive/js/dataTables.responsive.min.js",
     "~/Content/Admin/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js",
     "~/Content/Admin/vendors/datatables.net-scroller/js/datatables.scroller.min.js",
     "~/Content/Admin/vendors/jszip/dist/jszip.min.js",
     "~/Content/Admin/vendors/pdfmake/build/pdfmake.min.js",
     "~/Content/Admin/vendors/pdfmake/build/vfs_fonts.js"
 ));

            bundles.Add(new StyleBundle("~/bundles/Admin/DataBase/Styles").Include(
            "~/Content/Admin/vendors/datatables.net-bs/css/dataTables.bootstrap.css",
            "~/Content/Admin/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.css",
            "~/Content/Admin/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.css",
            "~/Content/Admin/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.css",
            "~/Content/Admin/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.css"));

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js",
                        "~/Scripts/jquery.unobtrusive-ajax.js"));


            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Используйте версию Modernizr для разработчиков, чтобы учиться работать. Когда вы будете готовы перейти к работе,
            // используйте средство сборки на сайте http://modernizr.com, чтобы выбрать только нужные тесты.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/font-awesome.css",
                      "~/Content/site.css"));
        }
    }
}
