﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;
using Dan.Models.Image;
using Dan.Models.Interface;

namespace Dan.Models.Catalog
{
    [Flags]
    public enum CatalogDisplayMethod
    {
        None = 0,
        Product = 1,
        LargeCategory = 2,
        ShortCategory = 4
    }

    public class CategoryEntity
        : IAbstractEntityInterface
        , IEntityWithImageInterface
    {
        public long EntityId { get; set; }

        #region Description
        /// <summary>
        /// Gets or sets the name
        /// </summary>
        public string Name { get; set; }
        public string GetDisplayName()
        {
            if(ParentCategoryEntity == null)
            {
                return $"[{EntityId}] ================ {Name} ==============";
            }
            return $"[{EntityId}] {ParentCategoryEntity.Name} > {Name}";
        }

        /// <summary>
        /// Gets or sets the short description
        /// </summary>
        public string ShortDescription { get; set; }
        /// <summary>
        /// Gets or sets the full description
        /// </summary>
        public string FullDescription { get; set; }
        #endregion


        /// <summary>
        /// Gets or sets a value of used category template identifier
        /// </summary>
        public int CategoryTemplateId { get; set; }

        public CatalogDisplayMethod DisplayMethod => GetDisplayMethod();

        public CatalogDisplayMethod GetDisplayMethod()
        {
            CatalogDisplayMethod method = CatalogDisplayMethod.None;
            if (ProductEntities.Any())
            {
                method |= CatalogDisplayMethod.Product;
            }

            if(ChildCategoryEntities.Any())
            {
                method |= CatalogDisplayMethod.LargeCategory;
            }

            return method;
        }

        public SeoEntityType Seo { get; set; }


        /// <summary>
        /// Gets or sets the parent category identifier
        /// </summary>
        public long? ParentCategoryId { get; set; }
        public virtual CategoryEntity ParentCategoryEntity { get; set; }


        /// <summary>
        /// Gets or sets the picture identifier
        /// </summary>
        public long ImageId { get; set; }
        public virtual ImageEntity ImagEntity { get; set; }


        public int PageSize { get; set; }
        public bool AllowCustomersToSelectPageSize { get; set; }
        public bool ShowOnHomePage { get; set; }
        public bool IncludeInTopMenu { get; set; }
        public bool SubjectToAcl { get; set; }
        public bool LimitedToStores { get; set; }
        public bool Published { get; set; }
        public bool Deleted { get; set; }
        public int DisplayOrder { get; set; }
        public DateTime CreatedOnUtc { get; set; }
        public DateTime UpdatedOnUtc { get; set; }

        public virtual List<ProductEntity> ProductEntities { get; set; } = new List<ProductEntity>(32);
        public virtual List<CategoryEntity> ChildCategoryEntities { get; set; } = new List<CategoryEntity>(8);
        public long[] TakeChildCategoryIds() => new []
        {
            EntityId
        }.Concat(ChildCategoryEntities.SelectMany(p => p.TakeChildCategoryIds())).ToArray();

        public List<CategoryEntity> TakeChildCaregoryEntities()
        {
            return new List<CategoryEntity>(ChildCategoryEntities)
                .Concat(ChildCategoryEntities.SelectMany(p => p.TakeChildCaregoryEntities()))
                .ToList();
        }

        public List<ProductEntity> TakeChildProductEntities()
        {
            return new List<ProductEntity>(ProductEntities)
                .Concat(ChildCategoryEntities.SelectMany(p => p.TakeChildProductEntities()))
                .ToList();
        }

        public long TakeChildCategoryCounts()
        {
            return TakeChildCategoryIds().LongLength;
        }

        public long TakeChildProductCounts()
        {
            return TakeChildProductEntities().Count;
        }

        public class Configuration : EntityTypeConfiguration<CategoryEntity>
        {
            public Configuration()
            {
                HasKey(i => i.EntityId);
                ToTable("catalog.CategoryEntity");

                Ignore(p => p.DisplayMethod);

                HasMany(p => p.ProductEntities).WithRequired(p => p.CategoryEntity).HasForeignKey(p => p.CategoryId);
                HasOptional(p => p.ParentCategoryEntity).WithMany(p => p.ChildCategoryEntities).HasForeignKey(p => p.ParentCategoryId);

                //=================================================================
                Property(p => p.Name).IsRequired().HasMaxLength(64);
                Property(p => p.ShortDescription).IsOptional().HasMaxLength(2048);
                Property(p => p.FullDescription).IsOptional().HasMaxLength(4096);

            }
        }
    }
}