﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using Dan.Models.Catalog;
using Dan.Models.Image;

namespace Dan.View.Category
{
    public class CatalogCategoryShortView 
    {
        public long EntityId { get; set; }
        public string Name { get; set; }
        public string Image { get; set; }
        public List<CatalogCategoryShortView> ChildCategories { get; set; } = new List<CatalogCategoryShortView>();
    }

    public static class CatalogCategoryShortViewHelper
    {
        public static List<CatalogCategoryShortView> AsShortView(this List<CategoryEntity> entity)
        {
            return entity.Select(e => e.AsShortView()).ToList();
        }

        public static CatalogCategoryShortView AsShortView(this CategoryEntity entity)
        {
            Mapper.Initialize
            (
                c => c.CreateMap<CategoryEntity, CatalogCategoryShortView>()
                .ForMember(p => p.ChildCategories, m => m.MapFrom(r => r.ChildCategoryEntities.AsShortView()))
                .ForMember(p => p.Image, m => m.MapFrom(r => $"{ImageEntity.RootDirectory}/{r.ImagEntity.FileName}"))

            );
            return Mapper.Map<CatalogCategoryShortView>(entity);
        }

    }
}