﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Dan.Models;
using Dan.Models.Catalog;
using Dan.Models.Statistic;

namespace Dan.Infrastructures.Repository
{

    public enum ProductOrderMethod
    {
        Name,
        PriceUp,
        PriceDown,
        RatingUp,
        RatingDown,
    }

    public class ProductRepository : AbstractRepository<ProductEntity, long, long?, ProductOrderMethod?>
    {
        public override DbSet<ProductEntity> Model => Context.ProductEntities;

        public override ProductEntity TakeEntity(long key)
        {
            return Context.ProductEntities.SingleOrDefault(p => p.EntityId == key);
        }

        public static IOrderedQueryable<ProductEntity> OrderBy(IQueryable<ProductEntity> entity, ProductOrderMethod? order)
        {
            if (order == ProductOrderMethod.Name) return entity.OrderBy(p => p.Name);
            if (order == ProductOrderMethod.PriceUp) return entity.OrderBy(p => p.Price.Price);
            if (order == ProductOrderMethod.PriceDown) return entity.OrderByDescending(p => p.Price.Price);
            return entity.OrderBy(p => p.Price.Price);
        }

        public override IQueryable<ProductEntity> TakeEntities(long? key, ProductOrderMethod? order, bool include)
        {
            //=================================================================================================
            if (key == null)
            {
                return OrderBy(Context.ProductEntities, order);
            }

            //=================================================================================================
            if (include)
            {
                var category = Context.CategoryEntities.Single(c => c.EntityId == key.Value);
                if (category.DisplayMethod.HasFlag(CatalogDisplayMethod.Product))
                {
                    var categories = category.TakeChildCategoryIds();
                    return OrderBy(Context.ProductEntities.Where(p => categories.Contains(p.CategoryId)).OrderBy(p => p.Price.Price), order);
                }
                return Enumerable.Empty<ProductEntity>().AsQueryable();
            }

            //=================================================================================================
            return OrderBy(Context.ProductEntities.Where(p => p.CategoryId == key.Value), order);
        }

        //public ICollection<ProductOrderViewWish> TakeOrderViewStatistic(long key)
        //{
        //    //return Context.ProductEntities.GroupBy(p => p.)
        //}

        //=========================================================================
        public ProductRepository() { }
        public ProductRepository(ApplicationDbContext context): base(context) { }
    }
}