﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Dan.Models.Order;

namespace Dan.Areas.Admin.View.Home
{
    public class HomeSellHit
    {
        public HomeSellHit(OrderItemEntity entity)
        {
            EntityId = entity.ProductEntity.EntityId;
            CategoryId = entity.ProductEntity.CategoryId;
            EntityName = entity.ProductEntity.Name;
            CategoryName = entity.ProductEntity.CategoryEntity.Name;
            Price = entity.ProductEntity.Price.Price;
        }

        public long EntityId { get; set; }
        public long CategoryId { get; set; }

        [Display(Name = "Товар")]
        public string EntityName { get; set; }

        [Display(Name = "Категория")]
        public string CategoryName { get; set; }

        [Display(Name = "Стоимость")]
        public decimal Price { get; set; }

        [Display(Name = "Количество")]
        public decimal Hit { get; set; }
    }
}