﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using Dan.Models.Order;
using Dan.View.Cart;

namespace Dan.Areas.Admin.View.Order
{
    public class AdminOrderConfirmView : AdminOrderDetailView
    {
    }

    public static partial class ViewHelper
    {
        public static List<AdminOrderConfirmView> AsEditView(this ICollection<OrderEntity> entity)
        {
            return entity.Select(p => p.AsEditView()).ToList();
        }

        public static AdminOrderConfirmView AsEditView(this OrderEntity entity)
        {
            Mapper.Initialize
            (
                c => c.CreateMap<OrderEntity, AdminOrderConfirmView>()
                    .ForMember(p => p.Products, m => m.MapFrom(r => r.ItemEntities.AsView()))
                    //.ForMember(p => p.Category, m => m.MapFrom(r => r.ProductEntity.CategoryEntity.AsShortView()))
                    .ForMember(p => p.CustomerAddressView, m => m.MapFrom(r => r.CustomerAddressEntity.AsShortView()))
                    .ForMember(p => p.Cost, m => m.MapFrom(r => r.ItemEntities.Sum(p => p.Price * p.Amount)))
                    .ForMember(p => p.Weight, m => m.MapFrom(r => r.ItemEntities.Sum(p => p.ProductEntity.Dimensions.Weight)))
            );
            return Mapper.Map<AdminOrderConfirmView>(entity);
        }
    }
}