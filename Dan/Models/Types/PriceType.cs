﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Dan.Models.Types
{
    [ComplexType]
    public class PriceType
    {
        [Required, Range(1.0, 100000.0), Display(Name = "Стоимость")]
        public decimal Price { get; set; }

        [Display(Name = "Себестоимость")]
        public decimal? OriginalPrice { get; set; } = 0;

        [Display(Name = "Старая стоимость")]
        public decimal OldPrice { get; set; } = 0;

        [Display(Name = "Стоимость по звонку")]
        public bool CallForPrice { get; set; } = false;

        [Display(Name = "Стоимость по акции")]
        public decimal? SpecialPrice { get; set; }

        [Display(Name = "Дата начала акции")]
        public DateTime? SpecialPriceStartDateTimeUtc { get; set; }

        [Display(Name = "Дата окончания акции")]
        public DateTime? SpecialPriceEndDateTimeUtc { get; set; }
    }
}