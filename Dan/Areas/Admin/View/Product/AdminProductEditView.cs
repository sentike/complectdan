﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Dan.Models.Types;
using Dan.View.Image;

namespace Dan.Areas.Admin.View.Product
{
    public class AdminProductEditView : AdminProductCreateView
    {
        [Display(Name = "Коменнтарий")]
        public string AdminComment { get; set; }

        [Display(Name = "Товар виден в каталоге")]
        public bool Published { get; set; }

        [Display(Name = "Товар удален из каталога")]
        public bool Deleted { get; set; }

        public MarketingType Marketing { get; set; }
        public new CatalogImageView Image { get; set; }
    }
}