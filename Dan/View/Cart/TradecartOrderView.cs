﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Dan.Models.Common;

namespace Dan.View.Cart
{
    public enum DeliveryMethod : byte
    {
        Pickup,
        Shipping,
    }

    public class TradecartOrderView
    {
        public List<ProductCartView> Products { get; set; } = new List<ProductCartView>();
        public decimal Price { get; set; }
        public decimal Weight { get; set; }

    }

    public class TradeCartSelectAddress
    {
        public OrderCustomerAddressView Address { get; set; }
        public List<SelectListItem> AddressSelectList { get; set; }
        public Guid AddressId { get; set; }
    }

    public class TradecartConfirmOrder
    {
        public List<ProductCartView> Products { get; set; } = new List<ProductCartView>();
        public DeliveryMethod Delivery { get; set; }
        public OrderCustomerAddressView Address { get; set; }
        public bool TermsofService { get; set; }

        public decimal Price { get; set; }
        public decimal Weight { get; set; }
    }

}