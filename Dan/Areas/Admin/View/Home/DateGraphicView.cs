﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Dan.Areas.Admin.View.Home
{
    public class DateGraphicView
    {
        public DateGraphicView() { }

        public DateGraphicView(int day, double value)
        {
            Date = new DateTime(DateTime.Now.Year, 1, 1).AddDays(day - 1);
            Value = value;
        }

        public DateTime Date { get; set; }
        public double Value { get; set; }
    }
}