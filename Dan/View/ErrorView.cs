﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Dan.View
{
    public class ErrorView
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public Exception Exception { get; set; }
    }
}