namespace Dan.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FirstInit : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "catalog.CategoryEntity",
                c => new
                    {
                        EntityId = c.Long(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 64),
                        ShortDescription = c.String(maxLength: 2048),
                        FullDescription = c.String(maxLength: 4096),
                        CategoryTemplateId = c.Int(nullable: false),
                        MetaKeywords = c.String(maxLength: 256),
                        MetaDescription = c.String(maxLength: 256),
                        MetaTitle = c.String(maxLength: 256),
                        ParentCategoryId = c.Long(),
                        ImageId = c.Long(nullable: false),
                        PageSize = c.Int(nullable: false),
                        AllowCustomersToSelectPageSize = c.Boolean(nullable: false),
                        ShowOnHomePage = c.Boolean(nullable: false),
                        IncludeInTopMenu = c.Boolean(nullable: false),
                        SubjectToAcl = c.Boolean(nullable: false),
                        LimitedToStores = c.Boolean(nullable: false),
                        Published = c.Boolean(nullable: false),
                        Deleted = c.Boolean(nullable: false),
                        DisplayOrder = c.Int(nullable: false),
                        CreatedOnUtc = c.DateTime(nullable: false),
                        UpdatedOnUtc = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.EntityId)
                .ForeignKey("image.ImageEntity", t => t.ImageId, cascadeDelete: true)
                .ForeignKey("catalog.CategoryEntity", t => t.ParentCategoryId)
                .Index(t => t.ParentCategoryId)
                .Index(t => t.ImageId);
            
            CreateTable(
                "image.ImageEntity",
                c => new
                    {
                        EntityId = c.Long(nullable: false, identity: true),
                        CategoryId = c.Long(nullable: false),
                        ImageFormat = c.Int(nullable: false),
                        Name = c.String(maxLength: 256),
                        ShortDescription = c.String(maxLength: 256),
                        FullDescription = c.String(maxLength: 256),
                        IsNew = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.EntityId)
                .ForeignKey("image.ImageCategoryEntity", t => t.CategoryId, cascadeDelete: true)
                .Index(t => t.CategoryId);
            
            CreateTable(
                "blog.BlogArticleEntity",
                c => new
                    {
                        EntityId = c.Long(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 128),
                        ShortDescription = c.String(nullable: false, maxLength: 4096),
                        FullDescription = c.String(nullable: false, maxLength: 16384),
                        CategoryId = c.Long(nullable: false),
                        MetaKeywords = c.String(maxLength: 256),
                        MetaDescription = c.String(maxLength: 256),
                        MetaTitle = c.String(maxLength: 256),
                        ImageId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.EntityId)
                .ForeignKey("blog.BlogCategoryEntity", t => t.CategoryId, cascadeDelete: true)
                .ForeignKey("image.ImageEntity", t => t.ImageId, cascadeDelete: true)
                .Index(t => t.CategoryId)
                .Index(t => t.ImageId);
            
            CreateTable(
                "blog.BlogCategoryEntity",
                c => new
                    {
                        EntityId = c.Long(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 128),
                        ShortDescription = c.String(nullable: false, maxLength: 4096),
                        FullDescription = c.String(nullable: false, maxLength: 16384),
                        MetaKeywords = c.String(maxLength: 256),
                        MetaDescription = c.String(maxLength: 256),
                        MetaTitle = c.String(maxLength: 256),
                    })
                .PrimaryKey(t => t.EntityId);
            
            CreateTable(
                "blog.BlogVisitEntity",
                c => new
                    {
                        VisitId = c.Guid(nullable: false),
                        ArticleId = c.Long(nullable: false),
                        Referer = c.String(maxLength: 256),
                        IpAddress = c.String(maxLength: 16),
                        UserAgent = c.String(maxLength: 128),
                        VisitDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.VisitId)
                .ForeignKey("blog.BlogArticleEntity", t => t.ArticleId, cascadeDelete: true)
                .Index(t => t.ArticleId);
            
            CreateTable(
                "image.ImageCategoryEntity",
                c => new
                    {
                        EntityId = c.Long(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.EntityId);
            
            CreateTable(
                "catalog.ManufactureEntity",
                c => new
                    {
                        EntityId = c.Long(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 128),
                        ShortDescription = c.String(nullable: false, maxLength: 4096),
                        FullDescription = c.String(nullable: false, maxLength: 16384),
                        MetaKeywords = c.String(maxLength: 256),
                        MetaDescription = c.String(maxLength: 256),
                        MetaTitle = c.String(maxLength: 256),
                        ImageId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.EntityId)
                .ForeignKey("image.ImageEntity", t => t.ImageId, cascadeDelete: true)
                .Index(t => t.ImageId);
            
            CreateTable(
                "catalog.ProductEntity",
                c => new
                    {
                        EntityId = c.Long(nullable: false, identity: true),
                        ManufactureId = c.Long(nullable: false),
                        CategoryId = c.Long(nullable: false),
                        Name = c.String(nullable: false, maxLength: 128),
                        ShortDescription = c.String(nullable: false, maxLength: 4096),
                        FullDescription = c.String(nullable: false, maxLength: 16384),
                        AdminComment = c.String(maxLength: 196),
                        MetaKeywords = c.String(maxLength: 256),
                        MetaDescription = c.String(maxLength: 256),
                        MetaTitle = c.String(maxLength: 256),
                        IsShipEnabled = c.Boolean(nullable: false),
                        IsFreeShipping = c.Boolean(nullable: false),
                        NotReturnable = c.Boolean(nullable: false),
                        CallForPrice = c.Boolean(nullable: false),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        OldPrice = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ProductCost = c.Decimal(nullable: false, precision: 18, scale: 2),
                        SpecialPrice = c.Decimal(precision: 18, scale: 2),
                        SpecialPriceStartDateTimeUtc = c.DateTime(),
                        SpecialPriceEndDateTimeUtc = c.DateTime(),
                        Weight = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Length = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Width = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Height = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Published = c.Boolean(nullable: false),
                        Deleted = c.Boolean(nullable: false),
                        MarkAsNew = c.Boolean(nullable: false),
                        MarkAsNewStartDateTimeUtc = c.DateTime(),
                        MarkAsNewEndDateTimeUtc = c.DateTime(),
                        CreatedOnUtc = c.DateTime(nullable: false),
                        UpdatedOnUtc = c.DateTime(nullable: false),
                        HasUserAgreement = c.Boolean(nullable: false),
                        UserAgreementText = c.String(maxLength: 4096),
                        Sku = c.String(maxLength: 128),
                        ManufacturerPartNumber = c.String(maxLength: 128),
                        Gtin = c.String(nullable: false, maxLength: 128),
                        ImageId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.EntityId)
                .ForeignKey("catalog.ManufactureEntity", t => t.ManufactureId, cascadeDelete: true)
                .ForeignKey("image.ImageEntity", t => t.ImageId, cascadeDelete: true)
                .ForeignKey("catalog.CategoryEntity", t => t.CategoryId, cascadeDelete: true)
                .Index(t => t.ManufactureId)
                .Index(t => t.CategoryId)
                .Index(t => t.ImageId);
            
            CreateTable(
                "catalog.ProductImageEntity",
                c => new
                    {
                        ImageId = c.Long(nullable: false, identity: true),
                        ProductId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.ImageId)
                .ForeignKey("catalog.ProductEntity", t => t.ProductId, cascadeDelete: true)
                .Index(t => t.ProductId);
            
            CreateTable(
                "catalog.ProductParamEntity",
                c => new
                    {
                        EntityId = c.Long(nullable: false, identity: true),
                        ProductId = c.Long(nullable: false),
                        ParamId = c.Long(nullable: false),
                        Value = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.EntityId)
                .ForeignKey("catalog.TemplateParamEnity", t => t.ParamId, cascadeDelete: true)
                .ForeignKey("catalog.ProductEntity", t => t.ProductId, cascadeDelete: true)
                .Index(t => t.ProductId)
                .Index(t => t.ParamId);
            
            CreateTable(
                "catalog.TemplateParamEnity",
                c => new
                    {
                        EntityId = c.Long(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 64),
                    })
                .PrimaryKey(t => t.EntityId);
            
            CreateTable(
                "catalog.ProductRecomendEntity",
                c => new
                    {
                        EntityId = c.Long(nullable: false, identity: true),
                        ProductId = c.Long(nullable: false),
                        RecomendId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.EntityId)
                .ForeignKey("catalog.ProductEntity", t => t.RecomendId, cascadeDelete: true)
                .ForeignKey("catalog.ProductEntity", t => t.ProductId, cascadeDelete: true)
                .Index(t => t.ProductId)
                .Index(t => t.RecomendId);
            
            CreateTable(
                "catalog.ProductVisitEntity",
                c => new
                    {
                        VisitId = c.Guid(nullable: false),
                        ProductId = c.Long(nullable: false),
                        Referer = c.String(maxLength: 256),
                        IpAddress = c.String(maxLength: 16),
                        UserAgent = c.String(maxLength: 128),
                        VisitDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.VisitId)
                .ForeignKey("catalog.ProductEntity", t => t.ProductId, cascadeDelete: true)
                .Index(t => t.ProductId);
            
            CreateTable(
                "account.Roles",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "account.UserRoles",
                c => new
                    {
                        UserId = c.Guid(nullable: false),
                        RoleId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("account.Roles", t => t.RoleId, cascadeDelete: true)
                .ForeignKey("account.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "account.Users",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        SecretAnswer = c.String(maxLength: 16),
                        EmailReserveValue = c.String(maxLength: 64),
                        EmailSafeValue = c.String(maxLength: 64),
                        EmailChangeDate = c.DateTime(nullable: false),
                        PhoneChangeDate = c.DateTime(nullable: false),
                        FirstName = c.String(maxLength: 16),
                        MiddleName = c.String(maxLength: 16),
                        LastName = c.String(maxLength: 16),
                        Gender = c.Short(nullable: false),
                        RegistrationDate = c.DateTime(nullable: false),
                        LastActivityDate = c.DateTime(nullable: false),
                        BirthDate = c.DateTime(),
                        DefaultAddressId = c.Guid(),
                        Email = c.String(maxLength: 64),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(maxLength: 20),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 64),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("account.CustomerAddressEntity", t => t.DefaultAddressId)
                .Index(t => t.RegistrationDate)
                .Index(t => t.LastActivityDate)
                .Index(t => t.DefaultAddressId)
                .Index(t => t.Email, unique: true)
                .Index(t => t.PhoneNumber, unique: true)
                .Index(t => t.UserName, unique: true);
            
            CreateTable(
                "account.CustomerAddressEntity",
                c => new
                    {
                        EntityId = c.Guid(nullable: false),
                        SessionId = c.Guid(nullable: false),
                        UserId = c.Guid(),
                        AddressName = c.String(),
                        FirstName = c.String(),
                        LastName = c.String(),
                        Email = c.String(),
                        Company = c.String(),
                        Country = c.String(),
                        ZipPostalCode = c.String(),
                        StateProvince = c.String(),
                        City = c.String(),
                        Address = c.String(),
                        PhoneNumber = c.String(),
                        FaxNumber = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.EntityId)
                .ForeignKey("account.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "account.UserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.Guid(nullable: false),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("account.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "account.UserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("account.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "wishlist.OrderEntity",
                c => new
                    {
                        EntityId = c.Guid(nullable: false),
                        SessionId = c.Guid(nullable: false),
                        UserId = c.Guid(),
                        OrderConfirmAttep = c.Short(nullable: false),
                        OrderStatus = c.Short(nullable: false),
                        ContactConfirmingStatus = c.Short(nullable: false),
                        CustomerAddressId = c.Guid(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        DeleteDate = c.DateTime(),
                        ConfirmEmailDate = c.DateTime(),
                        ConfirmPhoneDate = c.DateTime(),
                        ConfirmOrderDate = c.DateTime(),
                        DeliveryConfirmDate = c.DateTime(),
                        DeliverySendDate = c.DateTime(),
                        ShippingMethod = c.Short(nullable: false),
                        DeliveryToken = c.String(maxLength: 32),
                        IpAddress = c.String(maxLength: 16),
                        UserAgent = c.String(maxLength: 196),
                        AdminComment = c.String(maxLength: 196),
                        Comment = c.String(maxLength: 1024),
                        CustomerAddressEntity_EntityId = c.Guid(),
                    })
                .PrimaryKey(t => t.EntityId)
                .ForeignKey("account.CustomerAddressEntity", t => t.CustomerAddressEntity_EntityId)
                .ForeignKey("account.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.CustomerAddressEntity_EntityId);
            
            CreateTable(
                "wishlist.OrderItemEntity",
                c => new
                    {
                        EntityId = c.Guid(nullable: false),
                        OrderId = c.Guid(nullable: false),
                        ProductId = c.Long(nullable: false),
                        Amount = c.Short(nullable: false),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        OriginalProductCost = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.EntityId)
                .ForeignKey("catalog.ProductEntity", t => t.ProductId, cascadeDelete: true)
                .ForeignKey("wishlist.OrderEntity", t => t.OrderId, cascadeDelete: true)
                .Index(t => t.OrderId)
                .Index(t => t.ProductId);
            
            CreateTable(
                "wishlist.TradecartEntity",
                c => new
                    {
                        EntityId = c.Guid(nullable: false),
                        ProductId = c.Long(nullable: false),
                        Amount = c.Short(nullable: false),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        OriginalProductCost = c.Decimal(nullable: false, precision: 18, scale: 2),
                        SessionId = c.Guid(nullable: false),
                        UserId = c.Guid(),
                        CreatedDate = c.DateTime(nullable: false),
                        DeleteDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.EntityId)
                .ForeignKey("catalog.ProductEntity", t => t.ProductId, cascadeDelete: true)
                .ForeignKey("account.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.ProductId)
                .Index(t => t.UserId);
            
            CreateTable(
                "wishlist.WishlistEntity",
                c => new
                    {
                        EntityId = c.Guid(nullable: false),
                        ProductId = c.Long(nullable: false),
                        Amount = c.Short(nullable: false),
                        SessionId = c.Guid(nullable: false),
                        UserId = c.Guid(),
                        CreatedDate = c.DateTime(nullable: false),
                        DeleteDate = c.DateTime(),
                        Caption = c.String(maxLength: 256),
                    })
                .PrimaryKey(t => t.EntityId)
                .ForeignKey("catalog.ProductEntity", t => t.ProductId, cascadeDelete: true)
                .ForeignKey("account.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.ProductId)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("wishlist.WishlistEntity", "UserId", "account.Users");
            DropForeignKey("wishlist.WishlistEntity", "ProductId", "catalog.ProductEntity");
            DropForeignKey("wishlist.TradecartEntity", "UserId", "account.Users");
            DropForeignKey("wishlist.TradecartEntity", "ProductId", "catalog.ProductEntity");
            DropForeignKey("account.UserRoles", "UserId", "account.Users");
            DropForeignKey("wishlist.OrderEntity", "UserId", "account.Users");
            DropForeignKey("wishlist.OrderItemEntity", "OrderId", "wishlist.OrderEntity");
            DropForeignKey("wishlist.OrderItemEntity", "ProductId", "catalog.ProductEntity");
            DropForeignKey("wishlist.OrderEntity", "CustomerAddressEntity_EntityId", "account.CustomerAddressEntity");
            DropForeignKey("account.UserLogins", "UserId", "account.Users");
            DropForeignKey("account.Users", "DefaultAddressId", "account.CustomerAddressEntity");
            DropForeignKey("account.UserClaims", "UserId", "account.Users");
            DropForeignKey("account.CustomerAddressEntity", "UserId", "account.Users");
            DropForeignKey("account.UserRoles", "RoleId", "account.Roles");
            DropForeignKey("catalog.ProductEntity", "CategoryId", "catalog.CategoryEntity");
            DropForeignKey("catalog.CategoryEntity", "ParentCategoryId", "catalog.CategoryEntity");
            DropForeignKey("catalog.ProductEntity", "ImageId", "image.ImageEntity");
            DropForeignKey("catalog.ManufactureEntity", "ImageId", "image.ImageEntity");
            DropForeignKey("catalog.ProductEntity", "ManufactureId", "catalog.ManufactureEntity");
            DropForeignKey("catalog.ProductVisitEntity", "ProductId", "catalog.ProductEntity");
            DropForeignKey("catalog.ProductRecomendEntity", "ProductId", "catalog.ProductEntity");
            DropForeignKey("catalog.ProductRecomendEntity", "RecomendId", "catalog.ProductEntity");
            DropForeignKey("catalog.ProductParamEntity", "ProductId", "catalog.ProductEntity");
            DropForeignKey("catalog.ProductParamEntity", "ParamId", "catalog.TemplateParamEnity");
            DropForeignKey("catalog.ProductImageEntity", "ProductId", "catalog.ProductEntity");
            DropForeignKey("image.ImageEntity", "CategoryId", "image.ImageCategoryEntity");
            DropForeignKey("catalog.CategoryEntity", "ImageId", "image.ImageEntity");
            DropForeignKey("blog.BlogArticleEntity", "ImageId", "image.ImageEntity");
            DropForeignKey("blog.BlogVisitEntity", "ArticleId", "blog.BlogArticleEntity");
            DropForeignKey("blog.BlogArticleEntity", "CategoryId", "blog.BlogCategoryEntity");
            DropIndex("wishlist.WishlistEntity", new[] { "UserId" });
            DropIndex("wishlist.WishlistEntity", new[] { "ProductId" });
            DropIndex("wishlist.TradecartEntity", new[] { "UserId" });
            DropIndex("wishlist.TradecartEntity", new[] { "ProductId" });
            DropIndex("wishlist.OrderItemEntity", new[] { "ProductId" });
            DropIndex("wishlist.OrderItemEntity", new[] { "OrderId" });
            DropIndex("wishlist.OrderEntity", new[] { "CustomerAddressEntity_EntityId" });
            DropIndex("wishlist.OrderEntity", new[] { "UserId" });
            DropIndex("account.UserLogins", new[] { "UserId" });
            DropIndex("account.UserClaims", new[] { "UserId" });
            DropIndex("account.CustomerAddressEntity", new[] { "UserId" });
            DropIndex("account.Users", new[] { "UserName" });
            DropIndex("account.Users", new[] { "PhoneNumber" });
            DropIndex("account.Users", new[] { "Email" });
            DropIndex("account.Users", new[] { "DefaultAddressId" });
            DropIndex("account.Users", new[] { "LastActivityDate" });
            DropIndex("account.Users", new[] { "RegistrationDate" });
            DropIndex("account.UserRoles", new[] { "RoleId" });
            DropIndex("account.UserRoles", new[] { "UserId" });
            DropIndex("account.Roles", "RoleNameIndex");
            DropIndex("catalog.ProductVisitEntity", new[] { "ProductId" });
            DropIndex("catalog.ProductRecomendEntity", new[] { "RecomendId" });
            DropIndex("catalog.ProductRecomendEntity", new[] { "ProductId" });
            DropIndex("catalog.ProductParamEntity", new[] { "ParamId" });
            DropIndex("catalog.ProductParamEntity", new[] { "ProductId" });
            DropIndex("catalog.ProductImageEntity", new[] { "ProductId" });
            DropIndex("catalog.ProductEntity", new[] { "ImageId" });
            DropIndex("catalog.ProductEntity", new[] { "CategoryId" });
            DropIndex("catalog.ProductEntity", new[] { "ManufactureId" });
            DropIndex("catalog.ManufactureEntity", new[] { "ImageId" });
            DropIndex("blog.BlogVisitEntity", new[] { "ArticleId" });
            DropIndex("blog.BlogArticleEntity", new[] { "ImageId" });
            DropIndex("blog.BlogArticleEntity", new[] { "CategoryId" });
            DropIndex("image.ImageEntity", new[] { "CategoryId" });
            DropIndex("catalog.CategoryEntity", new[] { "ImageId" });
            DropIndex("catalog.CategoryEntity", new[] { "ParentCategoryId" });
            DropTable("wishlist.WishlistEntity");
            DropTable("wishlist.TradecartEntity");
            DropTable("wishlist.OrderItemEntity");
            DropTable("wishlist.OrderEntity");
            DropTable("account.UserLogins");
            DropTable("account.UserClaims");
            DropTable("account.CustomerAddressEntity");
            DropTable("account.Users");
            DropTable("account.UserRoles");
            DropTable("account.Roles");
            DropTable("catalog.ProductVisitEntity");
            DropTable("catalog.ProductRecomendEntity");
            DropTable("catalog.TemplateParamEnity");
            DropTable("catalog.ProductParamEntity");
            DropTable("catalog.ProductImageEntity");
            DropTable("catalog.ProductEntity");
            DropTable("catalog.ManufactureEntity");
            DropTable("image.ImageCategoryEntity");
            DropTable("blog.BlogVisitEntity");
            DropTable("blog.BlogCategoryEntity");
            DropTable("blog.BlogArticleEntity");
            DropTable("image.ImageEntity");
            DropTable("catalog.CategoryEntity");
        }
    }
}
