﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Dan.Models.Blog;

namespace Dan.Infrastructures.Repository
{
    public class BlogArticleRepositor : AbstractRepository<BlogArticleEntity, long, long?>
    {
        public override BlogArticleEntity TakeEntity(long key)
        {
            return Context.BlogArticleEntities.SingleOrDefault(p => p.EntityId == key);
        }

        public override IQueryable<BlogArticleEntity> TakeEntities(long? key, bool include)
        {
            return Context.BlogArticleEntities.OrderByDescending(p => p.EntityId);
        }

        public override DbSet<BlogArticleEntity> Model => Context.BlogArticleEntities;
    }
}