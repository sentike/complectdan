﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using Dan.Areas.Admin.View.Image;
using Dan.Models.Catalog;
using Dan.Models.Interface;
using Dan.Models.Types;
using Dan.View.Image;

namespace Dan.Areas.Admin.View.Product
{
    public class AdminProductCreateView 
    {
        //===============================================================
        #region Основная информация
        [Display(Name = "#")]
        public long EntityId { get; set; }

        [Required, Display(Name = "Категория")]
        public long CategoryId { get; set; }

        [Display(Name = "Артикул"), StringLength(256)]
        public string Sku { get; set; }

        [Required, AllowHtml,  StringLength(256)]
        [Display(Name = "Наименование")]
        public string Name { get; set; }

        [AllowHtml]
        [Required, StringLength(4096)]
        [Display(Name = "Описание товара")]
        public string ShortDescription { get; set; }

        [AllowHtml]
        [Required, StringLength(8192)]
        [Display(Name = "Полное описание товара")]
        public string FullDescription { get; set; }

        [Display(Name = "Производитель")]
        public long ManufactureId { get; set; }

        #endregion

        //===============================================================

        public ShippingType Shipping { get; set; }
        public DimensionsType Dimensions { get; set; }
        public SeoEntityType Seo { get; set; }
        public PriceType Price { get; set; }

        //===============================================================
        #region Изображение 
        [Display(Name = "Изображение")]
        public long ImageId { get; set; }
        public AdminCatalogImageCreateView Image { get; set; }
        #endregion


        //===============================================================
        #region Списки
        public List<SelectListItem> CategorySelectList { get; set; } = new List<SelectListItem>();
        public List<SelectListItem> ImageSelectList { get; set; } = new List<SelectListItem>();
        public List<SelectListItem> ImageCategorySelectList { get; set; } = new List<SelectListItem>();
        public List<SelectListItem> ManafacturerSelectList { get; set; } = new List<SelectListItem>();
        #endregion
    }

    public static partial class ViewHelper
    {
        public static AdminProductEditView AsEditView(this ProductEntity view)
        {
            Mapper.Initialize
            (
                c => c.CreateMap<ProductEntity, AdminProductEditView>()
                .ForMember(p => p.Image, p => p.MapFrom(m => m.ImagEntity.AsShortView()))
            );
            return Mapper.Map<AdminProductEditView>(view);
        }

        public static ProductEntity AsEntity(this AdminProductCreateView view)
        {
            Mapper.Initialize
            (
                c => c.CreateMap<AdminProductCreateView, ProductEntity>()
                .ForMember(p => p.CreatedOnUtc, p => p.UseValue(DateTime.Now))
            );
            return Mapper.Map<ProductEntity>(view);
        }

    }


}