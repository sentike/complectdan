﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Dan.Areas.Admin.Infrastuctures;
using Dan.Areas.Admin.View;
using Dan.Areas.Admin.View.Image;
using Dan.Infrastructures.Repository;
using Dan.Models.Image;

namespace Dan.Areas.Admin.Controllers
{
    public class ProductImageController : AbstractController
    {
        public override DeleteProcessResponse OnDeleteProcess(AdminUniversalDeleteView view)
        {
            throw new NotImplementedException();
        }

        // GET: Admin/Image
        public ActionResult Index()
        {
            using (var r = new ImageRepository())
            {
                return View(r.TakeEntities(null, false).ToList().AsShortAdminView());
            }
        }

        // GET: Admin/Image/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }

            using (var r = new ImageRepository())
            {
                var entity = r.TakeEntity(id.Value);
                if (entity == null)
                {
                    return RedirectToAction("Index");
                }
                return View(entity.AsLargeView());
            }
        }

        // GET: Admin/Image/Create
        public ActionResult Create(long? id)
        {
            using (var r1 = new ImageCategoryRepository())
            {
                var categories = r1.TakeEntityList(id);
                var view = new AdminCatalogImageCreateView
                {
                    CategoryId = id ?? 0,
                    CategorySelectList = categories
                };
                return View(view);
            }
        }

        // POST: Admin/Image/Create
        [HttpPost]
        public ActionResult Create(AdminCatalogImageCreateView view)
        {
            if (UploadImage(view) == UploadImageResponse.Successfully)
            {
                return RedirectToAction("Index");
            }
            return View(view);

            //using (var r1 = new ImageCategoryRepository())
            //{
            //    //===================================================================
            //    if (ModelState.IsValid == false)
            //    {
            //        view.CategorySelectList = r1.TakeEntityList(view.CategoryId);
            //        return View(view);
            //    }
            //
            //    //===================================================================
            //    if (view.CategoryId <= 0)
            //    {
            //        ModelState.AddModelError(nameof(view.CategoryId), "Категория не указана");
            //        view.CategorySelectList = r1.TakeEntityList(view.CategoryId);
            //        return View(view);
            //    }
            //
            //    if (r1.IsExistEntity(view.CategoryId) == false)
            //    {
            //        ModelState.AddModelError(nameof(view.CategoryId), "Категория не найдена");
            //        view.CategorySelectList = r1.TakeEntityList(view.CategoryId);
            //        return View(view);
            //    }
            //
            //    if (view.ImageFile.HasFile() == false)
            //    {
            //        ModelState.AddModelError(nameof(view.ImageFile), "Изображение не загружено");
            //        view.CategorySelectList = r1.TakeEntityList(view.CategoryId);
            //        return View(view);
            //    }
            //
            //
            //    //===================================================================
            //    using (var r2 = new ImageRepository(r1.Context))
            //    {
            //        ImageEntity entity = r2.Context.ImageEntities.Add(view.AsEntity());
            //        r2.SaveChanges();
            //
            //        view.ImageFile.SaveAs(Server.MapPath($"/Content/public/{entity.EntityId}_original.{view.FileType}"));
            //        view.ImageFile.SaveImageAs(Server.MapPath($"/Content/public/{entity.EntityId}_large.{view.FileType}"), new FileEditImagePropertyView { SaveImageFormat = view.FileType });
            //        view.ImageFile.SaveImageAs(Server.MapPath($"/Content/public/{entity.EntityId}_small.{view.FileType}"), new FileEditImagePropertyView(260, 160) { SaveImageFormat = view.FileType });
            //    }
            //
            //    return RedirectToAction("Index");
            //}
        }

        // GET: Admin/Image/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Admin/Image/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Admin/Image/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Admin/Image/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
