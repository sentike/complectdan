﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Dan.Models.Interface
{
    public class SeoEntityType
    {
        [Display(Name = "Ключевые слова")]
        [StringLength(256)]
        public string MetaKeywords { get; set; }

        [Display(Name = "Описание")]
        [StringLength(256)]
        public string MetaDescription { get; set; }


        [Display(Name = "Заголовок")]
        [StringLength(256)]
        public string MetaTitle { get; set; }
    }
}