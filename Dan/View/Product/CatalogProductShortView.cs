﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using AutoMapper;
using Dan.Areas.Admin.View.Image;
using Dan.Models.Catalog;
using Dan.View.Image;
using Dan.Models.Types;

namespace Dan.View.Product
{
    public class CatalogProductShortView
    {
        [Display(Name = "#")]
        public long EntityId { get; set; }


        [Required, StringLength(256)]
        [Display(Name = "Наименование")]
        public string Name { get; set; }

        [Display(Name = "Изображение")]
        public CatalogImageView Image { get; set; }
        public PriceType Price { get; set; }

    }

    public static class CatalogProductShortViewHelper
    {
        public static List<CatalogProductShortView> AsShortView(this List<ProductEntity> entity)
        {
            return entity.Select(e => e.AsShortView()).ToList();
        }

        public static CatalogProductShortView AsShortView(this ProductEntity entity)
        {
            Mapper.Initialize
            (
                c => c.CreateMap<ProductEntity, CatalogProductShortView>()
                .ForMember(p => p.Image, p => p.MapFrom(m => m.ImagEntity.AsShortView()))
            );
            return Mapper.Map<CatalogProductShortView>(entity);
        }

    }
}