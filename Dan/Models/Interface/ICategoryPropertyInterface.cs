﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dan.Models.Interface
{
    interface ICategoryPropertyInterface
    {
        long CategoryId { get; set; }
        string CategoryName { get; set; }
    }
}
