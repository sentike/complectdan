﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using Dan.Areas.Admin.View.Home;
using Dan.Areas.Admin.View.Product;
using Dan.Models.Catalog;
using Dan.Models.Interface;

namespace Dan.Areas.Admin.View.Category
{
    public class AdminCategoryLargeView 
        : AdminCategoryShortView
    {
        public List<HomeOrderPreview> LatestOrders { get; set; } = new List<HomeOrderPreview>(16);
        public List<AdminProductShortView> Products { get; set; } = new List<AdminProductShortView>();
        public SeoEntityType Seo { get; set; }
    }

    public static partial class ViewHelper
    {
        public static List<AdminCategoryLargeView> AsLargeAdminView(this List<CategoryEntity> entity)
        {
            return entity.Select(e => e.AsLargeAdminView()).ToList();
        }

        public static AdminCategoryLargeView AsLargeAdminView(this CategoryEntity entity)
        {
            Mapper.Initialize
            (
                c => c.CreateMap<CategoryEntity, AdminCategoryLargeView>()
                .ForMember(p => p.Description, p => p.MapFrom(m => m.ShortDescription))
                .ForMember(p => p.ParentCategoryId, p => p.MapFrom(m => m.ParentCategoryId))
                .ForMember(p => p.ParentCategoryName, p => p.MapFrom(m => m.ParentCategoryEntity.Name))
                .ForMember(p => p.ChildCategories, p => p.MapFrom(m => m.ChildCategoryEntities.AsShortAdminView()))
                .ForMember(p => p.CountOfProducts, p => p.MapFrom(m => m.ProductEntities.Count))
            );
            return Mapper.Map<AdminCategoryLargeView>(entity);
        }

    }
}