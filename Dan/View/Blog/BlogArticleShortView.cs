﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Dan.View.Blog
{
    public class BlogArticleShortView
    {
        public long EntityId { get; set; }
        public string Name { get; set; }
    }
}