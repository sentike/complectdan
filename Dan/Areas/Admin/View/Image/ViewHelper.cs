﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Dan.Areas.Admin.View.Category
{
    public static partial class ViewHelper
    {
        public static int AsUnixTime(this DateTime time) => (int)(time.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
    }
}