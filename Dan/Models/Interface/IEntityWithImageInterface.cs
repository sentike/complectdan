﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dan.Models.Image;

namespace Dan.Models.Interface
{
    public interface IEntityWithImageInterface
    {
        long ImageId { get; set; }
        ImageEntity ImagEntity { get; set; }
    }
}
