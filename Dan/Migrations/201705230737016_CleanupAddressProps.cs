namespace Dan.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CleanupAddressProps : DbMigration
    {
        public override void Up()
        {
            //===================================================
            DropColumn("account.CustomerAddressEntity", "AddressName");
            DropColumn("account.CustomerAddressEntity", "Company");
            DropColumn("account.CustomerAddressEntity", "Country");
            DropColumn("account.CustomerAddressEntity", "FaxNumber");

            AddColumn("account.CustomerAddressEntity", "MiddleName", c => c.String());
            //===================================================
            DropForeignKey("wishlist.OrderEntity", "CustomerAddressEntity_EntityId", "account.CustomerAddressEntity");
            DropIndex("wishlist.OrderEntity", new[] { "CustomerAddressEntity_EntityId" });
            DropColumn("wishlist.OrderEntity", "CustomerAddressEntity_EntityId");

            CreateIndex("wishlist.OrderEntity", "CustomerAddressId");
            AddForeignKey("wishlist.OrderEntity", "CustomerAddressId", "account.CustomerAddressEntity", "EntityId", cascadeDelete: true);

        }
        
        public override void Down()
        {
            AddColumn("account.CustomerAddressEntity", "FaxNumber", c => c.String());
            AddColumn("account.CustomerAddressEntity", "Country", c => c.String());
            AddColumn("account.CustomerAddressEntity", "Company", c => c.String());
            AddColumn("account.CustomerAddressEntity", "AddressName", c => c.String());
            DropForeignKey("wishlist.OrderEntity", "CustomerAddressId", "account.CustomerAddressEntity");
            DropIndex("wishlist.OrderEntity", new[] { "CustomerAddressId" });
            AlterColumn("wishlist.OrderEntity", "CustomerAddressId", c => c.Guid());
            DropColumn("account.CustomerAddressEntity", "MiddleName");
            RenameColumn(table: "wishlist.OrderEntity", name: "CustomerAddressId", newName: "CustomerAddressEntity_EntityId");
            AddColumn("wishlist.OrderEntity", "CustomerAddressId", c => c.Guid(nullable: false));
            CreateIndex("wishlist.OrderEntity", "CustomerAddressEntity_EntityId");
            AddForeignKey("wishlist.OrderEntity", "CustomerAddressEntity_EntityId", "account.CustomerAddressEntity", "EntityId");
        }
    }
}
