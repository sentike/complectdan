﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using AutoMapper;
using Dan.Models.Order;
using Dan.View.Cart;

namespace Dan.Areas.Admin.View.Order
{
    public class AdminOrderPreview
    {
        [Display(Name = "Заказ")]
        public Guid EntityId { get; set; }

        [Display(Name = "Статус заказа")]
        public CustomerOrderStatus OrderStatus { get; set; }
        public ContactConfirmingStatus ContactConfirmingStatus { get; set; }

        [Display(Name = "Контактная информация")]
        public OrderCustomerAddressView CustomerAddressView { get; set; }

        [Display(Name = "Дата создания")]
        public DateTime CreatedDate { get; set; }

        [Display(Name = "Способ доставки")]
        public DeliveryMethod ShippingMethod { get; set; }

        [Display(Name = "Номер отправления")]
        public string DeliveryToken { get; set; }

        [Display(Name = "Вес, кг")]
        public double Weight { get; set; }

        [Display(Name = "Сумма, руб")]
        public double Cost { get; set; }

        [Display(Name = "Ip")]
        public string IpAddress { get; set; }


        public string AdminComment { get; set; }
    }

    public static partial class ViewHelper
    {
        public static string StatusColor(this CustomerOrderStatus orderStatus)
        {
            if (orderStatus == CustomerOrderStatus.Canceled)
            {
                return "danger";
            }
            else if (orderStatus == CustomerOrderStatus.Delivery)
            {
                return "active";
            }
            else if (orderStatus == CustomerOrderStatus.Payment)
            {
                return "warning";
            }
            else if (orderStatus == CustomerOrderStatus.Preparing)
            {
                return "success";
            }

            return "info";       
        }

        public static List<AdminOrderPreview> AsView(this ICollection<OrderEntity> entity)
        {
            return entity.Select(p => p.AsView()).ToList();
        }

        public static AdminOrderPreview AsView(this OrderEntity entity)
        {
            Mapper.Initialize
            (
                c => c.CreateMap<OrderEntity, AdminOrderPreview>()
                                        //.ForMember(p => p.Product, m => m.MapFrom(r => r.ProductEntity.AsTradeCartView()))
                                        //.ForMember(p => p.Category, m => m.MapFrom(r => r.ProductEntity.CategoryEntity.AsShortView()))
                    .ForMember(p => p.CustomerAddressView, m => m.MapFrom(r => r.CustomerAddressEntity.AsShortView()))
                    .ForMember(p => p.Cost, m => m.MapFrom(r => r.ItemEntities.Sum(p => p.Price * p.Amount)))
                    .ForMember(p => p.Weight, m => m.MapFrom(r => r.ItemEntities.Sum(p => p.ProductEntity.Dimensions.Weight)))
            );
            return Mapper.Map<AdminOrderPreview>(entity);
        }
    }
}