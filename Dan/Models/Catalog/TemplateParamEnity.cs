﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace Dan.Models.Catalog
{
    public class TemplateParamEnity
    {
        public long EntityId { get; set; }
        public string Name { get; set; }
        //public bool DisplayAsFilter { get; set; }

        public class Configuration : EntityTypeConfiguration<TemplateParamEnity>
        {
            public Configuration()
            {
                HasKey(i => i.EntityId);
                ToTable("catalog.TemplateParamEnity");

                //=================================================================
                Property(p => p.Name).IsRequired().HasMaxLength(64);
            }
        }
    }
}