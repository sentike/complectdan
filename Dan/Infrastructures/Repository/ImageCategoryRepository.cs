﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Dan.Models;
using Dan.Models.Image;

namespace Dan.Infrastructures.Repository
{
    public class ImageCategoryRepository : AbstractRepository<ImageCategoryEntity, long, long?>
    {
        public override ImageCategoryEntity TakeEntity(long key)
        {
            return Context.ImageCategoryEntities.SingleOrDefault(p => p.EntityId == key);
        }

        public List<SelectListItem> TakeEntityList(long? key)
        {
            return TakeEntities(null, false).ToList(). Select(p => new SelectListItem
            {
                Selected = p.EntityId == key,
                Value = p.EntityId.ToString(),
                Text = $"[{p.EntityId}] {p.Name}"
            }).ToList();
        }

        public override IQueryable<ImageCategoryEntity> TakeEntities(long? key, bool include)
        {
            return Context.ImageCategoryEntities.OrderBy(p => p.EntityId);
        }

        //=========================================================================
        public ImageCategoryRepository() { }
        public ImageCategoryRepository(ApplicationDbContext context): base(context) { }
        public override DbSet<ImageCategoryEntity> Model => Context.ImageCategoryEntities;

    }
}