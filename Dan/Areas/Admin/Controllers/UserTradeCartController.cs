﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Dan.Areas.Admin.Controllers
{
    public class UserTradeCartController : Controller
    {
        // GET: Admin/Trade
        public ActionResult Index(Guid? id, int page = 1)
        {
            return View();
        }

        // GET: Admin/Trade/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Admin/Trade/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/Trade/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Admin/Trade/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Admin/Trade/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Admin/Trade/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Admin/Trade/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
