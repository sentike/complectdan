﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;

namespace Dan.Infrastructures.Session
{
    public class GuidSessionManager : SessionIDManager
    {
        public override string CreateSessionID(HttpContext context)
        {
            return Guid.NewGuid().ToString();
        }

        public override bool Validate(string id)
        {
            Guid t;
            var r = Guid.TryParse(id, out t) && t != Guid.Empty && t.ToString() == id;
            return r;
        }
    }
}