﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Dan.Areas.Admin.View.Image;
using Dan.Models.Image;

namespace Dan.Areas.Admin.Infrastuctures
{
    public static class PostedFileHelper
    {
        public static List<SelectListItem> WithDefaultElem(this List<SelectListItem> list, string desc, string value = "0")
        {
            return new[]
                {
                    new SelectListItem()
                    {
                        Selected = true,
                        Text = desc,
                        Value = value
                    }
                }.Concat(list)
                .ToList();
        }

        public static bool HasFile(this HttpPostedFileBase file)
        {
            return file != null && file.ContentLength > 0;
        }

        public static ImageFormat AsImageFormat(this FileImageFormat format)
        {
            if (format == FileImageFormat.Png) return ImageFormat.Png;
            if (format == FileImageFormat.Jpg) return ImageFormat.Jpeg;
            if (format == FileImageFormat.Bmp) return ImageFormat.Bmp;
            return ImageFormat.Jpeg;
        }

        //==================================================================================
        public static void SaveImageAs(this HttpPostedFileBase file, string path, FileEditImagePropertyView property) => SaveImageAs(Image.FromStream(file.InputStream), path, property);

        public static void SaveImageAs(string source, string path, FileEditImagePropertyView property)
        {
            if (File.Exists(source) == false) throw new FileNotFoundException("Не удалось найти оригинальный файл изображения", source);
            SaveImageAs(Image.FromFile(source), path, property);
        }

        public static void SaveImageAs(this Image image, string path, FileEditImagePropertyView property)
        {
            var eps = new EncoderParameters(1)
            {
                Param = new[]
                {
                    new EncoderParameter(Encoder.Quality, Math.Max(25, 100 - property.Compression))
                }
            };

            var resized = image.ResizeImage(property);
            var format = property.SaveImageFormat.AsImageFormat();
            var encoder = format.GetEncoderInfo();
            resized.Save(path, encoder, eps);
        }


        /// <summary>
        /// Resize the image to the specified width and height.
        /// </summary>
        /// <param name="image">The image to resize.</param>
        /// <returns>The resized image.</returns>
        public static Bitmap ResizeImage(this Image image, FileEditImagePropertyView property)
        {
            if (property.ResolutionX <= 0 || property.ResolutionY <= 0)
            {
                property.ResolutionX = image.Width;
                property.ResolutionY = image.Height;
            }

            var destRect = new Rectangle(0, 0, property.ResolutionX, property.ResolutionY);
            var destImage = new Bitmap(property.ResolutionX, property.ResolutionY);

            destImage.SetResolution(image.HorizontalResolution, image.VerticalResolution);

            using (var graphics = Graphics.FromImage(destImage))
            {
                graphics.CompositingMode = CompositingMode.SourceCopy;
                graphics.CompositingQuality = property.CompositingQuality;
                graphics.InterpolationMode = property.InterpolationMode;
                graphics.SmoothingMode = property.SmoothingMode;
                graphics.PixelOffsetMode = property.PixelOffsetMode;

                using (var wrapMode = new ImageAttributes())
                {
                    wrapMode.SetWrapMode(WrapMode.TileFlipXY);
                    graphics.DrawImage(image, destRect, 0, 0, image.Width, image.Height, GraphicsUnit.Pixel, wrapMode);
                }
            }

            return destImage;
        }



        public static ImageCodecInfo GetEncoderInfo(this ImageFormat format)
        {
            ImageCodecInfo[] codecs = ImageCodecInfo.GetImageDecoders();
            return codecs.FirstOrDefault(codec => codec.FormatID == format.Guid);
        }
    }
}