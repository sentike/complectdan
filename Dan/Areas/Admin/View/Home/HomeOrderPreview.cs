﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using AutoMapper;
using Dan.Models.Image;
using Dan.Models.Order;

namespace Dan.Areas.Admin.View.Home
{
    public class HomeOrderPreview
    {
        [Display(Name = "#")]
        public Guid EntityId { get; set; }

        [Display(Name = "Статус")]
        public CustomerOrderStatus OrderStatus { get; set; }

        [Display(Name = "Клиент")]
        public string Custumer { get; set; }

        [Display(Name = "Город")]
        public string City { get; set; }

        [Display(Name = "Дата")]
        public DateTime CreateDate { get; set; }

        [Display(Name = "Сумма, руб")]
        public double Cost { get; set; }
    }

    public static partial class ViewHelper
    {
        public static List<HomeOrderPreview> AsShortAdminView(this List<OrderEntity> entity)
        {
            return entity.Select(e => e.AsShortAdminView()).ToList();
        }

        public static HomeOrderPreview AsShortAdminView(this OrderEntity entity)
        {
            Mapper.Initialize
            (
                c => c.CreateMap<OrderEntity, HomeOrderPreview>()
                    .ForMember(p => p.Custumer, p => p.MapFrom(m => m.CustomerAddressEntity.UserName))
                    .ForMember(p => p.City, p => p.MapFrom(m => m.CustomerAddressEntity.City))
                    .ForMember(p => p.CreateDate, p => p.MapFrom(m => m.CreatedDate))
                    .ForMember(p => p.Cost, p => p.MapFrom(m => m.Cost))
            );
            return Mapper.Map<HomeOrderPreview>(entity);
        }

    }

}