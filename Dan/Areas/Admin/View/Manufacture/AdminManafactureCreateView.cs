﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using Dan.Areas.Admin.View.Image;
using Dan.Models.Catalog;
using Dan.Models.Interface;
using Dan.View.Product;

namespace Dan.Areas.Admin.View.Manufacture
{
    public class AdminManafactureCreateView 
        : AdminManafactureLargeView
    {
        [AllowHtml]
        [Required, StringLength(8192)]
        [Display(Name = "Полное описание товара")]
        public string FullDescription { get; set; }

        [Display(Name = "Изображение")]
        public new AdminCatalogImageCreateView Image { get; set; }
        public List<SelectListItem> ImageCategorySelectList { get; set; } = new List<SelectListItem>();
        public List<SelectListItem> ImageSelectList { get; set; } = new List<SelectListItem>();
        public long ImageId { get; set; }
    }



    public static partial class ViewHelper
    {
        public static ManufactureEntity AsEntity(this AdminManafactureCreateView view)
        {
            Mapper.Initialize
            (
                c => c.CreateMap<AdminManafactureCreateView, ManufactureEntity>()
                .ForMember(p => p.ShortDescription, p => p.MapFrom(m => m.Description))
            );
            return Mapper.Map<ManufactureEntity>(view);
        }
    }
}