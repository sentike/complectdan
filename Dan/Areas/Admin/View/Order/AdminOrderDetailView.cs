﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using AutoMapper;
using Dan.Models.Order;
using Dan.View.Cart;
using Dan.View.Order;

namespace Dan.Areas.Admin.View.Order
{
    public class AdminOrderDetailView : CustomerOrderDetailView
    {
        [Display(Name = "Дата отмены")]
        public DateTime? DeleteDate { get; set; }

        [Display(Name = "Дата подтверждения Email")]
        public DateTime? ConfirmEmailDate { get; set; }
        public DateTime? ConfirmPhoneDate { get; set; }

        [Display(Name = "Дата подтверждения заказа")]
        public DateTime? ConfirmOrderDate { get; set; }

        [Display(Name = "Дата получения")]
        public DateTime? DeliveryConfirmDate { get; set; }

        [Display(Name = "Ip-адрес клиента")]
        public string IpAddress { get; set; }

        [Display(Name = "Браузер клиента")]
        public string UserAgent { get; set; }

        [Display(Name = "Коментарий администратора")]
        public string AdminComment { get; set; }

        [Display(Name = "Коментарий клиента")]
        public string Comment { get; set; }
    }

    public static partial class ViewHelper
    {
        public static List<AdminOrderPreview> AsDetailView(this ICollection<OrderEntity> entity)
        {
            return entity.Select(p => p.AsView()).ToList();
        }

        public static AdminOrderDetailView AsDetailView(this OrderEntity entity)
        {
            Mapper.Initialize
            (
                c => c.CreateMap<OrderEntity, AdminOrderDetailView>()
                    .ForMember(p => p.Products, m => m.MapFrom(r => r.ItemEntities.AsView()))
                    //.ForMember(p => p.Category, m => m.MapFrom(r => r.ProductEntity.CategoryEntity.AsShortView()))
                    .ForMember(p => p.CustomerAddressView, m => m.MapFrom(r => r.CustomerAddressEntity.AsShortView()))
                    .ForMember(p => p.Cost, m => m.MapFrom(r => r.ItemEntities.Sum(p => p.Price * p.Amount)))
                    .ForMember(p => p.Weight, m => m.MapFrom(r => r.ItemEntities.Sum(p => p.ProductEntity.Dimensions.Weight)))
            );
            return Mapper.Map<AdminOrderDetailView>(entity);
        }
    }
}