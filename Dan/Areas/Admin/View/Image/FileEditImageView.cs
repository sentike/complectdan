﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Web;
using AutoMapper;
using Dan.Areas.Admin.Infrastuctures;
using Dan.Models.Image;

namespace Dan.Areas.Admin.View.Image
{
    public class FileEditImagePropertyView
    {
        [Range(100, 4800)]
        [Display(Name = "Высота")]
        public int ResolutionX { get; set; }

        [Range(100, 4800)]
        [Display(Name = "Ширина")]
        public int ResolutionY { get; set; }

        [Range(0, 80)]
        [Display(Name = "Степень сжатия")]
        public byte Compression { get; set; } = 50;

        //===========================================================

        public FileImageFormat SaveImageFormat { get; set; }
        public CompositingQuality CompositingQuality { get; set; } = CompositingQuality.HighQuality;
        public InterpolationMode InterpolationMode { get; set; } = InterpolationMode.HighQualityBicubic;
        public SmoothingMode SmoothingMode { get; set; } = SmoothingMode.HighQuality;
        public PixelOffsetMode PixelOffsetMode { get; set; } = PixelOffsetMode.HighQuality;

        public FileEditImagePropertyView() { }
        public FileEditImagePropertyView(string image)
        {
            //try
            //{
            //    var img = Image.FromFile(FileManagerRepository.Server.MapPath(image));
            //    ResolutionX = img.Width;
            //    ResolutionY = img.Height;
            //}
            //catch (Exception e)
            //{
            //    ResolutionX = 1920;
            //    ResolutionY = 1080;
            //}
        }

        public FileEditImagePropertyView(int width, int height)
        {
            ResolutionX = width;
            ResolutionY = height;
        }
    }

    public class FileEditImageView //: FilePreview
    {
        public FileImageFormat SaveImageFormat { get; set; }
        //public string LargeFilePath => $"/Content/public/{LargeFileName}";
        //public string LargeFileName => $"{FileId}_large.{FileType}";

        public FileEditImagePropertyView Short { get; set; }
        public FileEditImagePropertyView Large { get; set; }
    }

    //public static class FileEditImageViewHelper
    //{
    //    public static FileEditImageView AsEditImageView(this FileEntity entity)
    //    {
    //        Mapper.Initialize
    //        (
    //            c => c.CreateMap<ImageEntity, FileEditImageView>()
    //                .ForSourceMember(m => m.OriginalFileName, m => m.Ignore())
    //                .ForSourceMember(m => m.LargeFileName, m => m.Ignore())
    //                .ForSourceMember(m => m.SmallFileName, m => m.Ignore())
    //                .ForSourceMember(m => m.LargeFilePath, m => m.Ignore())
    //                .ForSourceMember(m => m.SmallFilePath, m => m.Ignore())
    //                .ForMember(m => m.Large, m => m.UseValue(new FileEditImagePropertyView(entity.LargeFilePath)))
    //                .ForMember(m => m.Short, m => m.UseValue(new FileEditImagePropertyView(entity.SmallFilePath)))
    //                .ForMember(m => m.SaveImageFormat, m => m.MapFrom(r => r.FileType.AsSaveImageFormat()))
    //        );
    //        return Mapper.Map<FileEditImageView>(entity);
    //    }
    //
    //}
}