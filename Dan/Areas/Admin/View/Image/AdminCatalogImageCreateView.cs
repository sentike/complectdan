﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using Dan.Areas.Admin.Infrastuctures;
using Dan.Models.Image;

namespace Dan.Areas.Admin.View.Image
{
    public class AdminCatalogImageCreateView : AdminCatalogImageLargeView
    {
        [Display(Name = "Выберите файл")]
        public HttpPostedFileBase ImageFile { get; set; }
        public List<SelectListItem> CategorySelectList { get; set; }


        public string FileExtension => Path.GetExtension(ImageFile?.FileName)?.Replace(".", String.Empty);
        public FileImageFormat FileType => (FileImageFormat)Enum.Parse(typeof(FileImageFormat), FileExtension ?? "none", true);

    }

    public static partial class ViewHelper
    {
        public static ImageEntity AsEntity(this AdminCatalogImageCreateView view)
        {
            Mapper.Initialize
            (
                c => c.CreateMap<AdminCatalogImageCreateView, ImageEntity>()
                .ForMember(p => p.ImageFormat, p =>p.MapFrom(m => m.FileType))
            );
            return Mapper.Map<ImageEntity>(view);
        }
    }
}