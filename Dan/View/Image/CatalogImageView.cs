﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using AutoMapper;
using Dan.Models.Image;

namespace Dan.View.Image
{
    public class CatalogImageView
    {
        [Display(Name = "Название")]
        public string Title { get; set; }

        [Display(Name = "Описание")]
        public string Description { get; set; }
        public string FileName { get; set; }
        public string FilePath => $"/Content/public/{FileName}";
    }

    public static partial class ViewHelper
    {
        public static List<CatalogImageView> AsShortView(this List<ImageEntity> entity)
        {
            return entity.Select(e => e.AsShortView()).ToList();
        }

        public static CatalogImageView AsShortView(this ImageEntity entity)
        {
            Mapper.Initialize
            (
                c => c.CreateMap<ImageEntity, CatalogImageView>()
                .ForMember(p => p.Description, p => p.MapFrom(m => m.ShortDescription))
                .ForMember(p => p.FileName, p => p.MapFrom(m => m.FileName))
            );
            return Mapper.Map<CatalogImageView>(entity);
        }

    }
}