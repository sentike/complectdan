﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Dan.Models.Catalog;
using Dan.Models.User;

namespace Dan.Models.Interface
{
    public interface IWishItemInterface
    {
        Guid EntityId { get; set; }
        long ProductId { get; set; }
        ProductEntity ProductEntity { get; set; }

        short Amount { get; set; }

        //============================================
        Guid SessionId { get; set; }
        Guid? UserId { get; set; }
        ApplicationUser UserEntity { get; set; }

        //============================================
        DateTime CreatedDate { get; set; }
        DateTime? DeleteDate { get; set; }

        //============================================
    }
}