﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Dan.Models;
using Dan.Models.Image;

namespace Dan.Infrastructures.Repository
{
    public class ImageRepository : AbstractRepository<ImageEntity, long, long?>
    {
        public override DbSet<ImageEntity> Model => Context.ImageEntities;

        public override ImageEntity TakeEntity(long key)
        {
            return Context.ImageEntities.SingleOrDefault(p => p.EntityId == key);
        }

        public override IQueryable<ImageEntity> TakeEntities(long? key, bool include)
        {
            if (key == null) return Context.ImageEntities.OrderByDescending(p => p.EntityId);
            return Context.ImageEntities.Where(p => p.CategoryId == key).OrderByDescending(p => p.EntityId);
        }

        public List<SelectListItem> TakeEntityList()
        {
            using (var r = new ImageCategoryRepository(Context))
            {
                var groups = r.TakeEntities(null, false).ToDictionary(p => p.EntityId, p => new SelectListGroup {Name = p.Name});
                return TakeEntities(null, false).ToArray().Select(p => new SelectListItem
                {
                    Group = groups[p.CategoryId],
                    Value = p.EntityId.ToString(),
                    Text = $"[{p.EntityId}] {p.Name}"
                }).ToList();
            }
        }

        //=========================================================================
        public ImageRepository() { }
        public ImageRepository(ApplicationDbContext context): base(context) { }
    }
}