﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Dan.Areas.Admin.View.TradeCart
{
    public class AdminTradeCartShortView
    {
        public Guid SessionId { get; set; }

        public Guid? UserId { get; set; }
        public string UserName { get; set; }

        public DateTime CreatedDate { get; set; }

        public decimal Cost { get; set; }
        public long Amounts { get; set; }
    }
}