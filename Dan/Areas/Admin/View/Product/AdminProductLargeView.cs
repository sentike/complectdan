﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using AutoMapper;
using Dan.Areas.Admin.View.Home;
using Dan.Models.Catalog;
using Dan.Models.Interface;
using Dan.View.Product;

namespace Dan.Areas.Admin.View.Product
{
    public class AdminProductLargeView 
        : CatalogProductLargeView
        , ICategoryPropertyInterface
    {
        public List<HomeOrderPreview> LatestOrders { get; set; } = new List<HomeOrderPreview>(16);

        [StringLength(128)]
        [Display(Name = "Артикул")]
        public string Sku { get; set; }

        [Display(Name = "Себестоимость")]
        public decimal? OriginalPrice { get; set; }

        [Display(Name = "Категория")]
        public long CategoryId { get; set; }
        public string CategoryName { get; set; }
    }

    public static partial class ViewHelper
    {
        public static List<AdminProductLargeView> AsLargeAdminView(this List<ProductEntity> entity)
        {
            return entity.Select(e => e.AsLargeAdminView()).ToList();
        }

        public static AdminProductLargeView AsLargeAdminView(this ProductEntity entity)
        {
            Mapper.Initialize
            (
                c => c.CreateMap<ProductEntity, AdminProductLargeView>()
                .ForMember(p => p.CategoryName, p => p.MapFrom(m => m.CategoryEntity.Name))
            );
            return Mapper.Map<AdminProductLargeView>(entity);
        }

    }
}