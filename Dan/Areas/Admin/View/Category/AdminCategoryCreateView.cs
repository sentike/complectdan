﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using Dan.Areas.Admin.View.Image;
using Dan.Models.Catalog;
using Dan.Models.Interface;
using Dan.View.Image;

namespace Dan.Areas.Admin.View.Category
{
    public class AdminCategoryCreateView
    {
        [Display(Name = "Название")]
        public string Name { get; set; }

        [Display(Name = "Изображение")]
        public long ImageId { get; set; }

        [Display(Name = "Родительская категория")]
        public long ParentCategoryId { get; set; }
        public List<SelectListItem> CategorySelectList { get; set; } = new List<SelectListItem>();
        public List<SelectListItem> ImageSelectList { get; set; } = new List<SelectListItem>();
        public List<SelectListItem> ImageCategorySelectList { get; set; } = new List<SelectListItem>();
        public long? AsParentCategoryId()
        {
            if (ParentCategoryId <= 0) return null;
            return ParentCategoryId;
        }


        [Display(Name = "Изображение")]
        public AdminCatalogImageCreateView Image { get; set; }


        [AllowHtml]
        [Required, StringLength(8192)]
        [Display(Name = "Описание")]
        public string ShortDescription { get; set; }


        [AllowHtml]
        [Required, StringLength(8192)]
        [Display(Name = "Полное описание")]
        public string FullDescription { get; set; }


        public SeoEntityType Seo { get; set; }
    }

    public static partial class ViewHelper
    {
        public static CategoryEntity AsEntity(this AdminCategoryCreateView entity)
        {
            Mapper.Initialize
            (
                c => c.CreateMap<AdminCategoryCreateView, CategoryEntity>()
                .ForMember(p => p.ParentCategoryId, p => p.MapFrom(m => m.AsParentCategoryId()))
            );
            return Mapper.Map<CategoryEntity>(entity);
        }

        public static AdminCategoryEditView AsEditView(this CategoryEntity entity)
        {
            Mapper.Initialize
            (
                c => c.CreateMap<CategoryEntity, AdminCategoryEditView>()
                    .ForMember(p => p.Image, p => p.MapFrom(m => m.ImagEntity.AsShortView()))
            );
            return Mapper.Map<AdminCategoryEditView>(entity);
        }

    }
}