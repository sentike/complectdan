﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using AutoMapper;
using Dan.Areas.Admin.View.Product;
using Dan.Models.Catalog;
using Dan.Models.Image;

namespace Dan.Areas.Admin.View.Category
{
    public class AdminCategoryShortView
    {
        [Display(Name = "#")]
        public long EntityId { get; set; }

        [Display(Name = "Наименование")]
        public string Name { get; set; }

        [Display(Name = "Изображение")]
        public string Image { get; set; }

        [Display(Name = "Описание")]
        public string Description { get; set; }

        [Display(Name = "Позиция")]
        public int DisplayOrder { get; set; }

        [Display(Name = "Описание")]
        public bool Published { get; set; }
        public bool Deleted { get; set; }

        [Display(Name = "Создано")]
        public DateTime CreatedOnUtc { get; set; }

        [Display(Name = "Родительская категория")]
        public long ParentCategoryId { get; set; }

        [Display(Name = "Родительская категория")]
        public string ParentCategoryName { get; set; }

        [Display(Name = "Продуктов")]
        public int CountOfProducts { get; set; }
        public List<AdminCategoryShortView> ChildCategories { get; set; } = new List<AdminCategoryShortView>(4);
    }



    public static partial class ViewHelper
    {
        public static List<AdminCategoryShortView> AsShortAdminView(this List<CategoryEntity> entity)
        {
            return entity.Select(e => e.AsShortAdminView()).ToList();
        }

        public static AdminCategoryShortView AsShortAdminView(this CategoryEntity entity)
        {
            Mapper.Initialize
            (
                c => c.CreateMap<CategoryEntity, AdminCategoryShortView>()
                .ForMember(p => p.Image, p => p.MapFrom(m => $"{ImageEntity.RootDirectory}/{m.ImagEntity.SmallFileName}"))
                .ForMember(p => p.Description, p => p.MapFrom(m => m.ShortDescription))
                .ForMember(p => p.ParentCategoryId, p => p.MapFrom(m => m.ParentCategoryId))
                .ForMember(p => p.ParentCategoryName, p => p.MapFrom(m => m.ParentCategoryEntity.Name))
                .ForMember(p => p.ChildCategories, p => p.MapFrom(m => m.ChildCategoryEntities.AsShortAdminView()))
                .ForMember(p => p.CountOfProducts, p => p.MapFrom(m => m.ProductEntities.Count))
            );
            return Mapper.Map<AdminCategoryShortView>(entity);
        }

    }

}