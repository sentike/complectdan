﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Dan.Models;
using Dan.Models.Catalog;

namespace Dan.Infrastructures.Repository
{
    public class CategoryRepository : AbstractRepository<CategoryEntity, long, long?>
    {
        public override CategoryEntity TakeEntity(long key)
        {
            return Context.CategoryEntities.SingleOrDefault(p => p.EntityId == key);
        }

        public override IQueryable<CategoryEntity> TakeEntities(long? key, bool include)
        {
            if (key == null)
            {
                if (include)
                {
                    return Context.CategoryEntities.Where(p => p.ParentCategoryId == null).OrderBy(p => p.DisplayOrder);
                }
                else
                {
                    return Context.CategoryEntities.OrderBy(p => p.DisplayOrder);
                }
            }
            return Context.CategoryEntities.Where(p => p.ParentCategoryId == key.Value).OrderBy(p => p.DisplayOrder);
        }

        private static IEnumerable<SelectListItem> CreateList(long? key, IEnumerable<CategoryEntity> entities, SelectListGroup group)
        {
            return entities.Select(p => new SelectListItem
            {
                Selected = p.EntityId == key,
                Value = p.EntityId.ToString(),
                //Group = group,
                Text = p.GetDisplayName()
            });
        }

        public static SelectListItem AsListItem(CategoryEntity entity, long? key)
        {
            return new SelectListItem
            {
                Selected = entity.EntityId == key,
                Value = entity.EntityId.ToString(),
                //Group = group,
                Text = entity.GetDisplayName()
            };
        }

        public List<SelectListItem> TakeEntityList(long? key, long? key2 = 0)
        {
            var list = new List<SelectListItem>();
            var root = TakeEntities(null, true).ToArray();



            foreach (var c1 in root)
            {
                list.Add(AsListItem(c1, key));

                foreach (var c2 in c1.ChildCategoryEntities)
                {
                    list.Add(AsListItem(c2, key));
                    foreach (var c3 in c2.ChildCategoryEntities)
                    {
                        list.Add(AsListItem(c3, key));
                        foreach (var c4 in c3.ChildCategoryEntities)
                        {
                            list.Add(AsListItem(c4, key));
                            foreach (var c5 in c4.ChildCategoryEntities)
                            {
                                list.Add(AsListItem(c5, key));
                                foreach (var c6 in c5.ChildCategoryEntities)
                                {
                                    list.Add(AsListItem(c6, key));
                                    foreach (var c7 in c6.ChildCategoryEntities)
                                    {
                                        list.Add(AsListItem(c7, key));
                                    }
                                }
                            }
                        }
                    }
                }



                //if (c1.ChildCategoryEntities.Any())
                //{
                //    list.AddRange(CreateList(key, c1.ChildCategoryEntities, group));
                //    foreach (var c2 in c1.ChildCategoryEntities)
                //    {
                //        list.AddRange(CreateList(key, c2.ChildCategoryEntities, group));
                //        foreach (var c3 in c2.ChildCategoryEntities)
                //        {
                //            list.AddRange(CreateList(key, c3.ChildCategoryEntities, group));
                //            foreach (var c4 in c2.ChildCategoryEntities)
                //            {
                //                list.AddRange(CreateList(key, c4.ChildCategoryEntities, group));
                //            }
                //        }
                //    }
                //}
            }


            //===============================================================================
            //foreach (var c1 in root)
            //{
            //    var group = new SelectListGroup()
            //    {
            //        Name = c1.Name
            //    };
            //
            //    list.AddRange(CreateList(key, new []
            //    {
            //        c1
            //    }, null));
            //
            //    if (c1.ChildCategoryEntities.Any())
            //    {
            //        list.AddRange(CreateList(key, c1.ChildCategoryEntities, group));
            //        foreach (var c2 in c1.ChildCategoryEntities)
            //        {
            //            list.AddRange(CreateList(key, c2.ChildCategoryEntities, group));
            //            foreach (var c3 in c2.ChildCategoryEntities)
            //            {
            //                list.AddRange(CreateList(key, c3.ChildCategoryEntities, group));
            //                foreach (var c4 in c2.ChildCategoryEntities)
            //                {
            //                    list.AddRange(CreateList(key, c4.ChildCategoryEntities, group));
            //                }
            //            }
            //        }
            //    }
            //}


            return list;
        }

        //=========================================================================
        public CategoryRepository(){}
        public CategoryRepository(ApplicationDbContext context): base(context) { }
        public override DbSet<CategoryEntity> Model => Context.CategoryEntities;
    }

}