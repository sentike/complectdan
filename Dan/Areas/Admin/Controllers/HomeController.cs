﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Dan.Areas.Admin.Infrastuctures;
using Dan.Areas.Admin.View;
using Dan.Areas.Admin.View.Home;
using Dan.Models.Order;
using Dan.Models.User;
using Microsoft.Ajax.Utilities;

namespace Dan.Areas.Admin.Controllers
{
    public class HomeController : AbstractController
    {
        // GET: Admin/Home
        public ActionResult Index()
        {
            var last = DateTime.Now.AddMonths(-1).Month;
            var curr = DateTime.Now.Month;


            var view = new HomeTotalStatisticView
            {
                //================================================
                TotalUsers = Context.Users.LongCount(),
                TotalOrders = Context.OrderEntities.LongCount(),
                
                //================================================
                MaleUsers = Context.Users.LongCount(p => p.Gender == GenderTypeId.Male),
                FemaleUsers = Context.Users.LongCount(p => p.Gender == GenderTypeId.Famele),

                //================================================
                OredersInLastMonth = Context.OrderEntities.LongCount(p => p.CreatedDate.Month == last),
                OredersInCurrentMonth = Context.OrderEntities.LongCount(p => p.CreatedDate.Month == curr),

                //================================================
                OrdersGraphicView = Context.OrderEntities/*.Where(p => p.CreatedDate.Month == last)*/.OrderByDescending(p => p.CreatedDate).Select(p => p.CreatedDate).ToArray().Select(p => p.DayOfYear).GroupBy(p => p).ToArray().Select(p => new DateGraphicView(p.Key, p.Count())).ToList(),
                UsersGraphicView = Context.Users/*.Where(p => p.RegistrationDate.Month == last)*/.OrderByDescending(p => p.RegistrationDate).Select(p => p.RegistrationDate).ToArray().Select(p => p.DayOfYear).GroupBy(p => p).ToArray().Select(p => new DateGraphicView(p.Key, p.Count())).ToList(),

                //================================================
                LatestOrders = Context.OrderEntities.OrderByDescending(p => p.CreatedDate).Take(10).ToList().AsShortAdminView(),
                ProcessOrders = Context.OrderEntities.Where(p => p.OrderStatus == CustomerOrderStatus.Processing).OrderByDescending(p => p.CreatedDate).Take(10).ToList().AsShortAdminView(),

                //================================================
                SellCostHits = Context.OrderEntities.Where(p => p.CreatedDate.Month >= last).SelectMany(p => p.ItemEntities).DistinctBy(p => p.ProductId).Take(10).ToArray().Select(p => new HomeSellHit(p)).ToList()

            };
            return View(view);
        }

        public override DeleteProcessResponse OnDeleteProcess(AdminUniversalDeleteView view)
        {
            throw new NotImplementedException();
        }
    }
}