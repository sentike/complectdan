namespace Dan.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemovedAddresses : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("account.CustomerAddressEntity", "UserId", "account.Users");
            DropForeignKey("account.Users", "DefaultAddressId", "account.CustomerAddressEntity");
            DropForeignKey("wishlist.OrderEntity", "CustomerAddressId", "account.CustomerAddressEntity");
            DropIndex("account.CustomerAddressEntity", new[] { "UserId" });
            DropIndex("account.Users", new[] { "DefaultAddressId" });
            DropIndex("wishlist.OrderEntity", new[] { "CustomerAddressId" });
            AddColumn("account.Users", "Address_FirstName", c => c.String());
            AddColumn("account.Users", "Address_LastName", c => c.String());
            AddColumn("account.Users", "Address_MiddleName", c => c.String());
            AddColumn("account.Users", "Address_Email", c => c.String());
            AddColumn("account.Users", "Address_ZipPostalCode", c => c.String());
            AddColumn("account.Users", "Address_StateProvince", c => c.String());
            AddColumn("account.Users", "Address_City", c => c.String());
            AddColumn("account.Users", "Address_Address", c => c.String());
            AddColumn("account.Users", "Address_PhoneNumber", c => c.String());
            AddColumn("account.Users", "Address_CreatedDate", c => c.DateTime(nullable: false));
            AddColumn("wishlist.OrderEntity", "CustomerAddressEntity_FirstName", c => c.String());
            AddColumn("wishlist.OrderEntity", "CustomerAddressEntity_LastName", c => c.String());
            AddColumn("wishlist.OrderEntity", "CustomerAddressEntity_MiddleName", c => c.String());
            AddColumn("wishlist.OrderEntity", "CustomerAddressEntity_Email", c => c.String());
            AddColumn("wishlist.OrderEntity", "CustomerAddressEntity_ZipPostalCode", c => c.String());
            AddColumn("wishlist.OrderEntity", "CustomerAddressEntity_StateProvince", c => c.String());
            AddColumn("wishlist.OrderEntity", "CustomerAddressEntity_City", c => c.String());
            AddColumn("wishlist.OrderEntity", "CustomerAddressEntity_Address", c => c.String());
            AddColumn("wishlist.OrderEntity", "CustomerAddressEntity_PhoneNumber", c => c.String());
            AddColumn("wishlist.OrderEntity", "CustomerAddressEntity_CreatedDate", c => c.DateTime(nullable: false));
            DropColumn("account.Users", "DefaultAddressId");
            DropColumn("wishlist.OrderEntity", "CustomerAddressId");
            DropTable("account.CustomerAddressEntity");
        }
        
        public override void Down()
        {
            CreateTable(
                "account.CustomerAddressEntity",
                c => new
                    {
                        EntityId = c.Guid(nullable: false),
                        SessionId = c.Guid(nullable: false),
                        UserId = c.Guid(),
                        FirstName = c.String(),
                        LastName = c.String(),
                        MiddleName = c.String(),
                        Email = c.String(),
                        ZipPostalCode = c.String(),
                        StateProvince = c.String(),
                        City = c.String(),
                        Address = c.String(),
                        PhoneNumber = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.EntityId);
            
            AddColumn("wishlist.OrderEntity", "CustomerAddressId", c => c.Guid(nullable: false));
            AddColumn("account.Users", "DefaultAddressId", c => c.Guid());
            DropColumn("wishlist.OrderEntity", "CustomerAddressEntity_CreatedDate");
            DropColumn("wishlist.OrderEntity", "CustomerAddressEntity_PhoneNumber");
            DropColumn("wishlist.OrderEntity", "CustomerAddressEntity_Address");
            DropColumn("wishlist.OrderEntity", "CustomerAddressEntity_City");
            DropColumn("wishlist.OrderEntity", "CustomerAddressEntity_StateProvince");
            DropColumn("wishlist.OrderEntity", "CustomerAddressEntity_ZipPostalCode");
            DropColumn("wishlist.OrderEntity", "CustomerAddressEntity_Email");
            DropColumn("wishlist.OrderEntity", "CustomerAddressEntity_MiddleName");
            DropColumn("wishlist.OrderEntity", "CustomerAddressEntity_LastName");
            DropColumn("wishlist.OrderEntity", "CustomerAddressEntity_FirstName");
            DropColumn("account.Users", "Address_CreatedDate");
            DropColumn("account.Users", "Address_PhoneNumber");
            DropColumn("account.Users", "Address_Address");
            DropColumn("account.Users", "Address_City");
            DropColumn("account.Users", "Address_StateProvince");
            DropColumn("account.Users", "Address_ZipPostalCode");
            DropColumn("account.Users", "Address_Email");
            DropColumn("account.Users", "Address_MiddleName");
            DropColumn("account.Users", "Address_LastName");
            DropColumn("account.Users", "Address_FirstName");
            CreateIndex("wishlist.OrderEntity", "CustomerAddressId");
            CreateIndex("account.Users", "DefaultAddressId");
            CreateIndex("account.CustomerAddressEntity", "UserId");
            AddForeignKey("wishlist.OrderEntity", "CustomerAddressId", "account.CustomerAddressEntity", "EntityId", cascadeDelete: true);
            AddForeignKey("account.Users", "DefaultAddressId", "account.CustomerAddressEntity", "EntityId");
            AddForeignKey("account.CustomerAddressEntity", "UserId", "account.Users", "Id", cascadeDelete: true);
        }
    }
}
