using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Infrastructure.Annotations;
using Dan.Models.Blog;
using Dan.Models.Catalog;
using Dan.Models.Common;
using Dan.Models.Image;
using Dan.Models.Order;
using Dan.Models.Tradecart;
using Dan.Models.User;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Dan.Models
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser, ApplicationRole, Guid, ApplicationUserLogin, ApplicationUserRole, ApplicationUserClaim>
    {
        public virtual DbSet<BlogArticleEntity> BlogArticleEntities { get; set; }
        public virtual DbSet<OrderEntity> OrderEntities { get; set; }

        //=========================================================================
        

        public virtual DbSet<ProductEntity> ProductEntities { get; set; }
        public virtual DbSet<CategoryEntity> CategoryEntities { get; set; }
        public virtual DbSet<ManufactureEntity> ManufactureEntities { get; set; }

        //=========================================================================
        public virtual DbSet<ProductParamEntity> ProductParamEntities { get; set; }
        public virtual DbSet<TemplateParamEnity> TemplateParamEnities { get; set; }

        //=========================================================================
        public virtual DbSet<ImageEntity> ImageEntities { get; set; }
        public virtual DbSet<ImageCategoryEntity> ImageCategoryEntities { get; set; }

        //=========================================================================
        public virtual DbSet<TradecartEntity> UserTradecartEntities { get; set; }
        public virtual DbSet<WishlistEntity> UserWishlistEntities { get; set; }

        //=========================================================================
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Configurations.Add(new BlogArticleEntity.Configuration());
            modelBuilder.Configurations.Add(new BlogCategoryEntity.Configuration());
            modelBuilder.Configurations.Add(new BlogVisitEntity.Configuration());


            //=========================================================================
            modelBuilder.Configurations.Add(new ProductEntity.Configuration());
            modelBuilder.Configurations.Add(new CategoryEntity.Configuration());
            modelBuilder.Configurations.Add(new ManufactureEntity.Configuration());
            modelBuilder.Configurations.Add(new ProductImageEntity.Configuration());
            modelBuilder.Configurations.Add(new ProductVisitEntity.Configuration());
            modelBuilder.Configurations.Add(new ProductRecomendEntity.Configuration());

            modelBuilder.Configurations.Add(new ProductParamEntity.Configuration());
            modelBuilder.Configurations.Add(new TemplateParamEnity.Configuration());


            //=========================================================================
            modelBuilder.Configurations.Add(new ImageEntity.Configuration());
            modelBuilder.Configurations.Add(new ImageCategoryEntity.Configuration());

            //=========================================================================
            modelBuilder.Configurations.Add(new TradecartEntity.Configuration());
            modelBuilder.Configurations.Add(new WishlistEntity.Configuration());

            modelBuilder.Configurations.Add(new OrderEntity.Configuration());
            modelBuilder.Configurations.Add(new OrderItemEntity.Configuration());


            //==============================================================================================================================================================================
            //                                  [ User ]
            //==============================================================================================================================================================================

            // modelBuilder.Configurations.Add(new ApplicationUser.Configuration());
            modelBuilder.Entity<ApplicationUser>().ToTable("account.Users");
            modelBuilder.Entity<ApplicationUser>().Property(p => p.FirstName).HasMaxLength(16).IsOptional();
            modelBuilder.Entity<ApplicationUser>().Property(p => p.LastName).HasMaxLength(16).IsOptional();
            modelBuilder.Entity<ApplicationUser>().Property(p => p.MiddleName).HasMaxLength(16).IsOptional();
            modelBuilder.Entity<ApplicationUser>().Property(p => p.UserName).HasMaxLength(64);
            modelBuilder.Entity<ApplicationUser>().Property(p => p.Email).HasMaxLength(64).IsOptional();
            modelBuilder.Entity<ApplicationUser>().Property(p => p.EmailSafeValue).HasMaxLength(64).IsOptional();
            modelBuilder.Entity<ApplicationUser>().Property(p => p.EmailReserveValue).HasMaxLength(64).IsOptional();
            modelBuilder.Entity<ApplicationUser>().Property(p => p.SecretAnswer).HasMaxLength(16).IsOptional();
            modelBuilder.Entity<ApplicationUser>().Property(p => p.PhoneNumber).HasMaxLength(20).IsOptional();

            modelBuilder.Entity<ApplicationUser>().Property(p => p.PhoneNumber).HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute() { IsUnique = true }));
            modelBuilder.Entity<ApplicationUser>().Property(p => p.Email).HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute() { IsUnique = true }));
            modelBuilder.Entity<ApplicationUser>().Property(p => p.UserName).HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute() { IsUnique = true }));
            modelBuilder.Entity<ApplicationUser>().Property(p => p.LastActivityDate).HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute()));
            modelBuilder.Entity<ApplicationUser>().Property(p => p.RegistrationDate).HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute()));


            modelBuilder.Entity<ApplicationUser>().HasMany(p => p.OrderEntities).WithOptional(p => p.UserEntity).HasForeignKey(p => p.UserId).WillCascadeOnDelete(true);

            //modelBuilder.Entity<ApplicationUser>().HasOptional(u => u.PlayerEntity).WithRequired(u => u.User);

            modelBuilder.Entity<ApplicationUserRole>().ToTable("account.UserRoles");
            modelBuilder.Entity<ApplicationUserLogin>().ToTable("account.UserLogins");
            modelBuilder.Entity<ApplicationUserClaim>().ToTable("account.UserClaims");
            modelBuilder.Entity<ApplicationRole>().ToTable("account.Roles");
        }

        public ApplicationDbContext()
            : base("DefaultConnection")
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
    }
}